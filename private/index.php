<?php
require_once 'Controlador_index.php';
$Controlador = new \controlador\Controlador_index();
if ($_SESSION['UsuarioActivo'] == 1) {
    $Controlador->index();
} else {
    header('Location: login/login.php');
}