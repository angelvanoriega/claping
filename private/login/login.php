<?php

require_once 'Controlador_login.php';
$Controlador = new \controlador\Controlador_login();
session_start();
if (empty($_SESSION['UsuarioId'])) {
    session_destroy();
} else {
    header('Location: index.php');
}
$Controlador->login();
