<?php

require_once 'Controlador_login.php';
$Controlador = new \controlador\Controlador_login();
$Controlador->controlarSesion();
if ($_SESSION['RolId'] == 1 && $_SESSION['UsuarioActivo'] == 1) {
    $Controlador->obtenerPorId();
}
