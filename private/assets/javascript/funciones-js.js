function obtenerImagen() {
    var file = $("#imagen")[0].files[0];
    if (file) {
        var fileType = file.type;
        var fileSize = file.size;
        if ((fileType === "image/jpeg")) {
            if (fileSize <= 2500000) {
                //información del formulario
                var formData = new FormData($(".formulario")[0]);
                var message = "";
                //hacemos la petición ajax  
                $.ajax({
                    url: 'upload.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                        message = $("<span>Subiendo la imagen, por favor espere...</span>");
                        showMessage(message);
                    },
                    //una vez finalizado correctamente
                    success: function (data) {
                        message = $("<span class='text-success'>La imagen ha subido correctamente.</span>");
                        showMessage(message);
                        document.getElementById("showImage").src = "imagenes_perfil/" + data;
                    },
                    //si ha ocurrido un error
                    error: function () {
                        message = $("<span class='text-danger'>Ha ocurrido un error.</span>");
                        showMessage(message);
                    }
                });
            } else {
                swal("Error!", "El tamaño maximo es 2MB." + fileSize, "error");
            }
        } else {
            swal("Error!", "Solo se permiten archivos de tipo imagen JPG.", "error");
        }
    }
}
function obtenerMultimedia() {
    var file = $("#multimedia_file")[0].files[0];
    if (file) {
        var fileType = file.type;
        var fileSize = file.size;
        if ((fileType === "image/jpeg") || (fileType === "video/mp4")) {
            if (fileSize <= 2500000) {
                //información del formulario
                var formData = new FormData($(".formularioM")[0]);
                var message = "";
                //hacemos la petición ajax  
                $.ajax({
                    url: 'upload_multimedia.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                        message = $("<span>Subiendo archivo, por favor espere...</span>");
                        showMessageMultimedia(message);
                    },
                    //una vez finalizado correctamente
                    success: function (data) {
                        message = $("<span class='text-success'>El archivo ha subido correctamente.</span>");
                        showMessageMultimedia(message);
                        document.getElementById("showMultimedia").src = "multimedia/" + data;
                    },
                    //si ha ocurrido un error
                    error: function () {
                        message = $("<span class='text-danger'>Ha ocurrido un error.</span>");
                        showMessageMultimedia(message);
                    }
                });
            } else {
                swal("Error!", "El tamaño maximo es 2MB." + fileSize, "error");
            }
        } else {
            swal("Error!", "Solo se permiten archivos de tipo JPG y MP4.", "error");
        }
    }
}
//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message) {
    $(".messages").html("").show();
    $(".messages").html(message);
}
function showMessageMultimedia(message) {
    $(".messagesMultimedia").html("").show();
    $(".messagesMultimedia").html(message);
}

//comprobamos si el archivo a subir es una imagen
//para visualizarla una vez haya subido
function isImage(extension)
{
    switch (extension.toLowerCase())
    {
        case 'jpg':
        case 'gif':
        case 'png':
        case 'jpeg':
            return true;
            break;
        default:
            return false;
            break;
    }
}

function loadJS() {
    $(document).ready(function () {
        $('#VerTodosRecibidos').DataTable();
    });
    cargarDatePicker();
}

function borrarListadoNumeros() {
    document.getElementById("tos").value = "";
}

function cargarDatePicker() {
    $("#fechaEnvioTexto").datepicker({
        inline: true
    });
    $('#horaEnvioTexto').timepicker({
        step: 1,
        timeFormat: 'H:i:s',
        scrollDefault: 'now'
    });
    $('#horaEnvioTexto').timepicker('setTime', new Date());
    $('#horaEnvioTexto').timepicker('option', {useSelect: true});
    $(this).hide();
    $(function () {
        $.datepicker._gotoToday = function (id) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
                inst.selectedDay = inst.currentDay;
                inst.drawMonth = inst.selectedMonth = inst.currentMonth;
                inst.drawYear = inst.selectedYear = inst.currentYear;
            } else {
                var date = new Date();
                inst.selectedDay = date.getDate();
                inst.drawMonth = inst.selectedMonth = date.getMonth();
                inst.drawYear = inst.selectedYear = date.getFullYear();
                // the below two lines are new
                this._setDateDatepicker(target, date);
                this._selectDate(id, this._getDateDatepicker(target));
            }
            this._notifyChange(inst);
            this._adjustDate(target);
        };
        $.datepicker.regional['es'] = {
            showWeek: true,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'yy-mm-dd',
            firstDay: 0,
            minDate: 0,
            showOtherMonths: true,
            selectOtherMonths: true,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
    });
}

/**
 * //cuenta caracteres del texto a enviar
 */
function cuenta() {
    var caracteresIntroducidos = document.getElementById("contenidoMensaje").value.length;
    document.getElementById("caracteresIntroducidos").innerHTML = caracteresIntroducidos;
    document.getElementById("caracteresRestantes").innerHTML = 3000 - caracteresIntroducidos;
}

function seguridad_clave(clave) {
    var seguridad = 0;
    if (clave.length !== 0) {
        if (tiene_numeros(clave) && tiene_letras(clave)) {
            seguridad = seguridad + 30;
        }
        if (tiene_minusculas(clave) && tiene_mayusculas(clave)) {
            seguridad = seguridad + 30;
        }
        if (clave.length >= 4 && clave.length <= 5) {
            seguridad += 10;
        } else {
            if (clave.length >= 6 && clave.length <= 8) {
                seguridad += 30;
            } else {
                if (clave.length > 8) {
                    seguridad += 40;
                }
            }
        }
    }
    return seguridad;
}

function tiene_numeros(texto) {
    if (texto.match(/[0-9]/)) {
        return true;
    } else {
        return false;
    }
}

function tiene_letras(texto) {
    if (texto.match(/[a-zA-Z]/)) {
        return true;
    } else {
        return false;
    }
}

function tiene_minusculas(texto) {
    if (texto.match(/[a-z]/)) {
        return true;
    } else {
        return false;
    }
}

function tiene_mayusculas(texto) {
    if (texto.match(/[A-Z]/)) {
        return true;
    } else {
        return false;
    }
}

function validarSemaforoContrasenia(seguridad) {
    var color;
    if (seguridad >= 0 && seguridad <= 9) {
        color = "#ff0000";
        return color;
    }
    if (seguridad > 9 && seguridad <= 80) {
        color = "#FF9900";
        return color;
    }
    if (seguridad > 81 && seguridad <= 100) {
        color = "#00B200";
        return color;
    }
}
/**
 * funciones para el modulo de login
 */
function validarNombreUsuario() {
    var UsuarioNombre = document.getElementById('UsuarioNombre').value;
    var Nombre = UsuarioNombre.replace(/\s/g, "");
    if (Nombre.length === 0) {
        var alerta = document.getElementById('validacionNombre');
        alerta.innerHTML = "Debes llenar este campo.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        alerta.style.float = 'right';
        return false;
    } else {
        if (UsuarioNombre.match(/[a-zA-Z]/) && !UsuarioNombre.match(/[0-9]/)) {
            var alerta = document.getElementById('validacionNombre');
            alerta.innerHTML = "";
            return UsuarioNombre;
        } else {
            var alerta = document.getElementById('validacionNombre');
            alerta.innerHTML = "El nombre solo puede contener letras.";
            alerta.style.color = "#ff0000";
            alerta.style.fontSize = 10;
            alerta.style.float = 'right';
            return false;
        }
    }
}

function validarApellidoUsuario() {
    var UsuarioApellido = document.getElementById('UsuarioApellido').value;
    var Apellido = UsuarioApellido.replace(/\s/g, "");
    if (Apellido.length === 0) {
        var alerta = document.getElementById('validacionApellido');
        alerta.innerHTML = "Debes llenar este campo.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        alerta.style.float = 'right';
        return false;
    } else {
        if (UsuarioApellido.match(/[a-zA-Z]/) && !UsuarioApellido.match(/[0-9]/)) {
            var alerta = document.getElementById('validacionApellido');
            alerta.innerHTML = "";
            return UsuarioApellido;
        } else {
            var alerta = document.getElementById('validacionApellido');
            alerta.innerHTML = "El apellido solo puede contener letras.";
            alerta.style.color = "#ff0000";
            alerta.style.fontSize = 10;
            alerta.style.float = 'right';
            return false;
        }
    }
}

function validarSobreNombre() {
    var UsuarioSobreNombre = document.getElementById('UsuarioSobreNombre').value;
    var SobreNombre = UsuarioSobreNombre.replace(/\s/g, "");
    if (SobreNombre.length === 0) {
        var alerta = document.getElementById('validacionSobreNombre');
        alerta.innerHTML = "Debes llenar este campo.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        return false;
    } else {
        if (SobreNombre.length > 3) {
            if (UsuarioSobreNombre.match(/^[a-zA-Z0-9._-]+$/)) {
                var alerta = document.getElementById('validacionSobreNombre');
                alerta.innerHTML = "";
                return UsuarioSobreNombre;
            } else {
                var alerta = document.getElementById('validacionSobreNombre');
                alerta.innerHTML = "Caracteres invalidos introducidos.";
                alerta.style.color = "#ff0000";
                alerta.style.fontSize = 10;
                return false;
            }
        } else {
            var alerta = document.getElementById('validacionSobreNombre');
            alerta.innerHTML = "El tamaño minimo es 4.";
            alerta.style.color = "#ff0000";
            alerta.style.fontSize = 10;
            return false;
        }
    }
}

function validarEmail() {
    var UsuarioEmail = document.getElementById('UsuarioEmail').value;
    var UsuarioEmailConfirmacion = document.getElementById('UsuarioEmailConfirmacion').value;
    var Email = UsuarioEmail.replace(/\s/g, "");
    var EmailConfirmacion = UsuarioEmailConfirmacion.replace(/\s/g, "");
    if (Email.length === 0 && EmailConfirmacion.length === 0) {
        var Em = document.getElementById('validacionEmail');
        Em.innerHTML = "Debes llenar este campo.";
        Em.style.color = "#ff0000";
        Em.style.fontSize = 10;
        var Emc = document.getElementById('validacionEmailConfirmacion');
        Emc.innerHTML = "Debes llenar este campo.";
        Emc.style.color = "#ff0000";
        Emc.style.fontSize = 10;
        return false;
    } else {
        if (UsuarioEmail === UsuarioEmailConfirmacion) {
            var patron = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (UsuarioEmail.match(patron)) {
                var Em = document.getElementById('validacionEmail');
                Em.innerHTML = "";
                var Emc = document.getElementById('validacionEmailConfirmacion');
                Emc.innerHTML = "";
                return UsuarioEmail;
            } else {
                var Em = document.getElementById('validacionEmail');
                Em.innerHTML = "Correo introducido no valido.";
                Em.style.color = "#ff0000";
                Em.style.fontSize = 10;
                var Emc = document.getElementById('validacionEmailConfirmacion');
                Emc.innerHTML = "Correo introducido no valido.";
                Emc.style.color = "#ff0000";
                Emc.style.fontSize = 10;
                return false;
            }
        } else {
            var Em = document.getElementById('validacionEmail');
            Em.innerHTML = "Los correos deben ser iguales.";
            Em.style.color = "#ff0000";
            Em.style.fontSize = 10;
            var Emc = document.getElementById('validacionEmailConfirmacion');
            Emc.innerHTML = "Los correos deben ser iguales.";
            Emc.style.color = "#ff0000";
            Emc.style.fontSize = 10;
            return false;
        }
    }
}

function validarContrasenia() {
    var UsuarioContrasenia = document.getElementById('UsuarioContrasenia').value;
    var seguridad = seguridad_clave(UsuarioContrasenia);
    var color = validarSemaforoContrasenia(seguridad);
    var Pwd = document.getElementById('validacionContrasenia');
    Pwd.innerHTML = seguridad + " % segura.";
    Pwd.style.color = color;
    Pwd.style.fontSize = 10;
    Pwd.style.float = 'right';
    if (UsuarioContrasenia.length >= 4) {
        var UsuarioContraseniaConfirmacion = document.getElementById('UsuarioContraseniaConfirmacion').value;
        if (UsuarioContraseniaConfirmacion.length > 0) {
            if (UsuarioContrasenia === UsuarioContraseniaConfirmacion) {
                var Pwdc = document.getElementById('validacionContraseniaConfirmacion');
                Pwdc.innerHTML = "Contraseñas coinciden.";
                Pwdc.style.color = "#00B200";
                Pwdc.style.fontSize = 10;
                Pwdc.style.float = 'right';
                return UsuarioContrasenia;
            } else {
                var Pwdc = document.getElementById('validacionContraseniaConfirmacion');
                Pwdc.innerHTML = "Contraseñas no coinciden.";
                Pwdc.style.color = "#ff0000";
                Pwdc.style.fontSize = 10;
                Pwdc.style.float = 'right';
                return false;
            }
        }
    } else {
        Pwd.innerHTML = seguridad + " % segura. Debe ser mayor o igual 4.";
        Pwd.style.color = color;
        Pwd.style.fontSize = 10;
        Pwd.style.float = 'right';
        return false;
    }

    return false;
}

function validarCelular() {
    var UsuarioNumeroCelular = document.getElementById('UsuarioNumeroCelular').value;
    var NumeroCelular = UsuarioNumeroCelular.replace(/\s/g, "");
    if (NumeroCelular.length === 0) {
        var alerta = document.getElementById('validacionCelular');
        alerta.innerHTML = "Debes llenar este campo.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        alerta.style.float = 'right';
        return false;
    } else {
        if (!NumeroCelular.match(/[a-zA-Z]/) && NumeroCelular.match(/[0-9]/)) {
            var alerta = document.getElementById('validacionCelular');
            alerta.innerHTML = "";
            return NumeroCelular;
        } else {
            var alerta = document.getElementById('validacionCelular');
            alerta.innerHTML = "El celular solo puede contener numeros.";
            alerta.style.color = "#ff0000";
            alerta.style.fontSize = 10;
            return false;
        }
    }
}

function validarSexo() {
    var UsuarioSexo = 0;
    if (document.getElementById('UsuarioSexoHombre').checked) {
        document.getElementById('UsuarioSexoMujer').checked = false;
        UsuarioSexo = document.getElementById('UsuarioSexoHombre').value;
    } else if (document.getElementById('UsuarioSexoMujer').checked) {
        document.getElementById('UsuarioSexoHombre').checked = false;
        UsuarioSexo = document.getElementById('UsuarioSexoMujer').value;
    }
    if (UsuarioSexo === 0) {
        var alerta = document.getElementById('validacionSexo');
        alerta.innerHTML = "Debes llenar este campo.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        return false;
    } else {
        var alerta = document.getElementById('validacionSexo');
        alerta.innerHTML = "";
        return UsuarioSexo;
    }
}

function validarFechaNacimiento() {
    var Dia = document.getElementById('Dia').value;
    var Mes = document.getElementById('Mes').value;
    var Anio = document.getElementById('Anio').value;
    if (Dia === '0' || Mes === '0' || Anio === '0') {
        var alerta = document.getElementById('validacionFechaNacimiento');
        alerta.innerHTML = "Completa tu fecha de nacimiento.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        return false;
    } else {
        var UsuarioFechaNacimiento = Anio + "-" + Mes + "-" + Dia;
        var alerta = document.getElementById('validacionFechaNacimiento');
        alerta.innerHTML = "";
        return UsuarioFechaNacimiento;
    }
}

function existeSobreNombre(UsuarioSobreNombre) {
    var respuesta = $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: "/login/existe-usuario.php",
        data: {
            "UsuarioSobreNombre": UsuarioSobreNombre
        }
    }).responseText;
    if (respuesta !== 'false') {
        var alerta = document.getElementById('validacionSobreNombre');
        alerta.innerHTML = "Nombre de usuario ya existe.";
        alerta.style.color = "#ff0000";
        alerta.style.fontSize = 10;
        return false;
    } else {
        var alerta = document.getElementById('validacionSobreNombre');
        alerta.innerHTML = "";
        return UsuarioSobreNombre;
    }
}

function existeEmail(UsuarioEmail) {
    var respuesta = $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: "/login/existe-email.php",
        data: {
            "UsuarioEmail": UsuarioEmail
        }
    }).responseText;
    if (respuesta !== 'false') {
        var Em = document.getElementById('validacionEmail');
        Em.innerHTML = "Correo electronico ya registrado.";
        Em.style.color = "#ff0000";
        Em.style.fontSize = 10;
        return false;
    } else {
        var Em = document.getElementById('validacionEmail');
        Em.innerHTML = "";
        return UsuarioEmail;
    }
}

function validarRegistroUsuario() {
    var PaisId = document.getElementById("PaisId").value;
    if (validarNombreUsuario() === false || validarApellidoUsuario() === false ||
            validarSobreNombre() === false || validarEmail() === false ||
            validarContrasenia() === false || validarCelular() === false ||
            validarSexo() === false || validarFechaNacimiento() === false) {
    } else {
        var UsuarioSobreNombre = validarSobreNombre();
        var UsuarioEmail = validarEmail();
        if (existeSobreNombre(UsuarioSobreNombre) === false || existeEmail(UsuarioEmail) === false) {
            swal({title: "Error!",
                text: "Datos incorrectos, favor de verificar.",
                type: "warning",
                showCancelButton: false,
                closeOnConfirm: true});
        } else {
            var registro = $.ajax({
                type: "POST",
                cache: false,
                url: "/login/registrar.php",
                data: {
                    "UsuarioNombre": validarNombreUsuario(),
                    "UsuarioApellido": validarApellidoUsuario(),
                    "UsuarioSobreNombre": UsuarioSobreNombre,
                    "UsuarioEmail": UsuarioEmail,
                    "UsuarioContrasenia": validarContrasenia(),
                    "PaisId": PaisId,
                    "UsuarioNumeroCelular": validarCelular(),
                    "UsuarioSexo": validarSexo(),
                    "UsuarioFechaNacimiento": validarFechaNacimiento(),
                    "Crearusuarios": 'Crearusuarios'
                }
            }).responseText;
            if (registro === 'false') {
                swal({title: "Error!",
                    text: "Ocurrio un error durante el registro.",
                    type: "error",
                    showCancelButton: false,
                    closeOnConfirm: true}, function () {
                    location.reload(true);
                });
            } else {
                swal({title: "Enhorabuena!",
                    text: "Te hemos enviado un correo para confirmar tu cuenta.",
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true},
                function () {
                    window.location.href = "/login/login.php";
                });
            }
        }
    }
}

function validarActualziarUsuario(UsuarioSobreNombre, UsuarioEmail) {
    if (validarNombreUsuario() === false || validarApellidoUsuario() === false ||
            validarSobreNombre() === false || validarEmailActualizar() === false ||
            validarCelular() === false || validarSexo() === false || validarFechaNacimiento() === false) {
        swal({title: "Error!",
            text: "Datos incorrectos, favor de verificar.",
            type: "warning",
            showCancelButton: false,
            closeOnConfirm: true});
        return false;
    } else {
        var SobreNombre = validarSobreNombre();
        var Email = validarEmailActualizar();
        var PaisId = document.getElementById("PaisId").value;
        var UsuarioId = document.getElementById("UsuarioId").value;
        if (SobreNombre !== UsuarioSobreNombre) {
            if (existeSobreNombre(SobreNombre) === false) {
                swal({title: "Error!",
                    text: "Datos incorrectos, favor de verificar.",
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true});
                return false;
            }
        }
        if (Email !== UsuarioEmail) {
            if (existeEmail(Email) === false) {
                swal({title: "Error!",
                    text: "Datos incorrectos, favor de verificar.",
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true});
                return false;
            }
        }
        var actualizacion = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/usuarios/Ajax_actualizar.php",
            data: {
                "UsuarioNombre": validarNombreUsuario(),
                "UsuarioApellido": validarApellidoUsuario(),
                "UsuarioSobreNombre": validarSobreNombre(),
                "UsuarioEmail": validarEmailActualizar(),
                "UsuarioNumeroCelular": validarCelular(),
                "UsuarioSexo": validarSexo(),
                "PaisId": PaisId,
                "$UsuarioId": UsuarioId,
                "UsuarioFechaNacimiento": validarFechaNacimiento(),
                "Actualizarusuarios": 'Actualizarusuarios'
            }
        }).responseText;
        if (actualizacion === 'false') {
            swal({title: "Error!",
                text: "Ocurrio un error durante la actualización.",
                type: "error",
                showCancelButton: false,
                closeOnConfirm: true},
            function () {
                location.reload(true);
            });
        } else {
            swal({title: "Enhorabuena!",
                text: "Se actualizaron tus datos correctamente  .",
                type: "success",
                showCancelButton: false,
                closeOnConfirm: true},
            function () {
                window.location.href = "/index.php";
            });
        }
    }
}

function validarEmailActualizar() {
    var UsuarioEmail = document.getElementById('UsuarioEmail').value;
    var Email = UsuarioEmail.replace(/\s/g, "");
    if (Email.length === 0) {
        var Em = document.getElementById('validacionEmail');
        Em.innerHTML = "Debes llenar este campo.";
        Em.style.color = "#ff0000";
        Em.style.fontSize = 10;
        return false;
    } else {
        var patron = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (UsuarioEmail.match(patron)) {
            var Em = document.getElementById('validacionEmail');
            Em.innerHTML = "";
            return UsuarioEmail;
        } else {
            var Em = document.getElementById('validacionEmail');
            Em.innerHTML = "Correo introducido no valido.";
            Em.style.color = "#ff0000";
            Em.style.fontSize = 10;
        }

    }
}

function cambiarRol(RolId, UsuarioIdH) {
    var UsuarioId = UsuarioIdH;
    var RolNuevo;
    if (RolId === 1) {
        RolNuevo = 2;
    } else {
        RolNuevo = 1;
    }
    swal({
        title: "¿Esta seguro?",
        text: "Estas a punto de cambiar el rol del usuario!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, cambiar!",
        closeOnConfirm: false
    }, function () {
        var cambiarRol = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/usuarios/Ajax_Cambiar_Rol.php",
            data: {
                "UsuarioId": UsuarioId,
                "RolNuevo": RolNuevo
            }
        }).responseText;
        if (cambiarRol === 'true') {
            swal("Cambiado!", "Se ha cambiado el rol correctamente.", "success");
        }
    });
}

function cambiarActivo(UsuarioActivo,UsuarioId) {
    var Nuevo;
    if (UsuarioActivo == '1') {
        Nuevo = 0;
    } else {
        Nuevo = 1;
    }
    swal({
        title: "¿Esta seguro?",
        text: "Estas a punto de cambiar la actividad del usuario!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, cambiar!",
        closeOnConfirm: false
    }, function () {
        var cambiarRol = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/usuarios/Ajax_Cambiar_Actividad.php",
            data: {
                "UsuarioId": UsuarioId,
                "UsuarioActivo": Nuevo
            }
        }).responseText;
        if (cambiarRol === 'true') {
            swal("Cambiado!", "Se ha cambiado la actividad correctamente.", "success");
        }
    });
}
//*************************************************************//
//                   empieza whapend                           //
//*************************************************************//

Array.prototype.unique = function () {
    var r = new Array();
    o:for (var i = 0, n = this.length; i < n; i++)
    {
        for (var x = 0, y = r.length; x < y; x++)
        {
            if (r[x] == this[i])
            {
                continue o;
            }
        }
        r[r.length] = this[i];
    }
    return r;
}
function findSMSAddresses(StrObj) {
    var separateEmailsBy = "\n";
    var email = ""; // if no match, use this
    var email2 = ""; // if no match, use this

    //busco las variables extras a los celus
    var datos = "";
    var StrObj2 = "\n" + StrObj;
    var emailsArray3 = StrObj2.match(/(.+)/gi);
    var datosArray = new Array();
    if (emailsArray3) {
        var ii = 0
        for (var i = 0; i < emailsArray3.length; i++)
        {
            linea = emailsArray3[i];
            if (linea.charAt(0) === '+')
                linea = linea.substr(1);

            var emailsArray4 = linea.match(/([0-9-\(\)]+)/gi);
            if (emailsArray4)
            {
                //if (datos!="") datos += separateEmailsBy;
                numero = emailsArray4[0].replace(/-/g, "").replace(/\(/g, "").replace(/\)/g, "");
                lineanueva = linea.replace(/\t/g, "/")
                lineanueva = lineanueva.replace(emailsArray4[0] + "/", "")
                lineanueva = lineanueva.replace(emailsArray4[0], "")
                //datos += numero.trim() + "/" + lineanueva.trim()
                //datos = datos.trim();
                datosArray.push(numero.trim() + "/" + lineanueva.trim());
            }
        }
    }
    datosArray = datosArray.unique();
    datos = datosArray.join(separateEmailsBy);
    datos = datos.trim();

    //alert(datos)
    //return datos;

    //Quito lineas duplicadas
    //datosArray = datos.split("\n").unique();
    //datos=datosArray.join("\n")
    return datos;
}
function corregirnumeros()
{
    v_creditos_por_mensaje = 0;

    function findSMSAddresses(StrObj) {
        var separateEmailsBy = "\n";
        var email = ""; // if no match, use this
        var email2 = ""; // if no match, use this

        //busco las variables extras a los celus
        var datos = "";
        var StrObj2 = "\n" + StrObj;
        var emailsArray3 = StrObj2.match(/(.+)/gi);
        var datosArray = new Array();
        if (emailsArray3) {
            var ii = 0
            for (var i = 0; i < emailsArray3.length; i++)
            {
                linea = emailsArray3[i];
                if (linea.charAt(0) === '+')
                    linea = linea.substr(1);

                var emailsArray4 = linea.match(/([0-9-\(\)]+)/gi);
                if (emailsArray4)
                {
                    //if (datos!="") datos += separateEmailsBy;
                    numero = emailsArray4[0].replace(/-/g, "").replace(/\(/g, "").replace(/\)/g, "");
                    lineanueva = linea.replace(/\t/g, "/")
                    lineanueva = lineanueva.replace(emailsArray4[0] + "/", "")
                    lineanueva = lineanueva.replace(emailsArray4[0], "")
                    //datos += numero.trim() + "/" + lineanueva.trim()
                    //datos = datos.trim();
                    datosArray.push(numero.trim() + "/" + lineanueva.trim());
                }
            }
        }
        datosArray = datosArray.unique();
        datos = datosArray.join(separateEmailsBy);
        datos = datos.trim();

        //alert(datos)
        //return datos;

        //Quito lineas duplicadas
        //datosArray = datos.split("\n").unique();
        //datos=datosArray.join("\n")

        emailsArray3 = datosArray
        if (emailsArray3)
        {
            document.getElementById("cantidad").innerHTML = emailsArray3.length;
        } else {
            document.getElementById("cantidad").innerHTML = 0;
        }
        return datos;
    }

    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    }

    Array.prototype.unique = function () {
        var r = new Array();
        o:for (var i = 0, n = this.length; i < n; i++)
        {
            for (var x = 0, y = r.length; x < y; x++)
            {
                if (r[x] == this[i])
                {
                    continue o;
                }
            }
            r[r.length] = this[i];
        }
        return r;
    }

    function contarPalabras(s) {
        for (x = 1; x < 30; x++)
        {
            s = s.replace(".", " ");
            s = s.replace(",", " ");
            s = s.replace("-", " ");
            s = s.replace("/", " ");
            s = s.replace("  ", " ");
            s = s.replace("  ", " ");
            s = s.replace("  ", " ");
        }
        return s.split(' ').length;
    }
    g_numeros_corregidos = false;

    f1.toserrores.value = ''

    if (f1.idpais.value == '')
    {
        alert('Por favor elija un país.')
    } else if (f1.tos.value == '')
    {
        alert('Por complete con algún número.')
    } else {
        $('#corregircargando').show();
        $('#botonvererrores1').hide();
        $('#botonvererrores2').hide();

        f1.tos.value = findSMSAddresses(f1.tos.value);
        fwa.numeros.value = f1.tos.value;
        fwa.idpais.value = f1.idpais.value;
        fwa.submit();
    }
    return false;
}
function seleccionogrupo()
{
    //Busco los checkbox de los grupos
    var chk_arr = document.getElementsByName('grupoid');
    var chklength = chk_arr.length;
    marcoungrupo = false;

    for (k = 0; k < chklength; k++)
    {
        if (chk_arr[k].checked)
        {
            marcoungrupo = true;
        }
    }

    return marcoungrupo
}

function mostrarsectornumeros()
{
    if (seleccionogrupo())
    {
        document.getElementById("sectornumeros").style.display = 'none';
    } else {
        document.getElementById("sectornumeros").style.display = '';
    }
}

function corregirnumeros_callback()
{
    f1.tos.value = findSMSAddresses(f1.tos.value);

    if (f1.toserrores.value == '')
    {
        $('#botonvererrores1').hide();
        $('#botonvererrores2').hide();
        $('#diverroneos').hide();
    } else {
        var text = $("#toserrores").val();
        var lines = text.split(/\r|\r\n|\n/);
        var count = lines.length;

        $("#botonvererrores1").val(' Mostrar errores (' + count + ')');
        $("#botonvererrores2").val(' Ocultar errores (' + count + ')');

        //$('#botonvererrores1').show();
        //$('#botonvererrores2').hide();

        $('#botonvererrores1').hide();
        $('#botonvererrores2').show();
        $('#diverroneos').show();

    }

    $('#corregircargando').hide();

    g_numeros_corregidos = true;

    if (g_auto_siguiente == true)
    {
        g_auto_siguiente = false;
        $('#mi_wizard').steps('next');
    }
}

function corregirnumeros()
{
    g_numeros_corregidos = false;

    f1.toserrores.value = ''

    if (f1.idpais.value == '')
    {
        alert('Por favor elija un país.')
    } else if (f1.tos.value == '')
    {
        alert('Por complete con algún número.')
    } else {
        $('#corregircargando').show();
        $('#botonvererrores1').hide();
        $('#botonvererrores2').hide();

        f1.tos.value = findSMSAddresses(f1.tos.value);
        fwa.numeros.value = f1.tos.value;
        fwa.idpais.value = f1.idpais.value;
        fwa.submit();
    }
    return false;
}
//*************************************************************//
//                   finaliza whapend                           //
//*************************************************************//

function obtenerDireccionImagen() {
    if ($("#showImage").length > 0) {
        var file = document.getElementById("showImage").src;
        if (file !== "") {
            if (file.indexOf("127.0.0.1") >= 0) {
                return "";
            } else {
                return file;
            }
        } else {
            return "";
        }
    } else {
        return "";
    }
}
function obtenerDireccionMultimedia() {
    if ($("#showMultimedia").length > 0) {
        var file = document.getElementById("showMultimedia").src;
        if (file !== "") {
            if (file.indexOf("127.0.0.1") >= 0) {
                return "";
            } else {
                return file;
            }
        } else {
            return "";
        }
    } else {
        return "";
    }
}

function obtenerListadoNumeros(codigoPais) {
    f1.tos.value = findSMSAddresses(f1.tos.value);
    f1.tos.value = f1.tos.value.replace(/[,-.]*/g, "");
    var listadoDeNumeros1 = document.getElementById("tos").value;
    listadoDeNumeros1 = listadoDeNumeros1.replace(/\n/g, "");
    var listadoDeNumeros = listadoDeNumeros1.split("/");
    var cadenaNumeros = "";

    for (i = 0; i < listadoDeNumeros.length; i++) {
        if (listadoDeNumeros[i].length >= 7 && listadoDeNumeros[i].length <= 10) {
            cadenaNumeros = cadenaNumeros + listadoDeNumeros[i] + "/";
        }
    }
    f1.tos.value = cadenaNumeros;
    var arregloNumerosEnviar = f1.tos.value.split("/");
    var numerosEnviar = "";
    for (i = 0; i < arregloNumerosEnviar.length; i++) {
        if (arregloNumerosEnviar[i].length >= 10 && arregloNumerosEnviar[i].length <= 13) {
            numerosEnviar = numerosEnviar + codigoPais + arregloNumerosEnviar[i] + ",";
        }
    }
    return numerosEnviar;
}
function obtenerFechaEnvio() {
    var fechaEnvio = "";
    if ($("#fechaEnvioTexto").length > 0 && $("#horaEnvioTexto").length > 0) {
        if (document.getElementById("fechaEnvioTexto").value !== "") {
            fechaEnvio = document.getElementById("fechaEnvioTexto").value + " " +
                    document.getElementById("horaEnvioTexto").value;
        }
    }
    return fechaEnvio;
}
function EnviarMensaje() {
    var imagenPerfil = obtenerDireccionImagen();
    var codigoPais = obtenerClavePais();
    var multimedia = obtenerDireccionMultimedia();
    var listadoNumeros = obtenerListadoNumeros(codigoPais);
    var fechaEnvio = obtenerFechaEnvio();
    var contenidoMensaje = document.getElementById("contenidoMensaje").value;
    var UsuarioId = document.getElementById("UsuarioId").value;
    if (listadoNumeros.length > 0 && contenidoMensaje.length > 0 && codigoPais !== "false") {
        var EnviarMensaje = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/mensajes_enviados/enviar_mensaje.php",
            data: {
                PERFILIMAGEN: imagenPerfil,
                TOS: listadoNumeros,
                TEXTO: multimedia + " " + contenidoMensaje,
                IDINTERNO: UsuarioId,
                FECHADESDE: fechaEnvio
            }
        }).responseText;
        if (EnviarMensaje === "false") {
            swal("Error!", EnviarMensaje, "error");
        } else {
            if (EnviarMensaje.indexOf("probando sin enviar") >= 0) {
                swal("", "Probando sin enviar.", "info");
            }
            if (EnviarMensaje.indexOf("OK") >= 0) {
                swal("Exitoso", "Se ha enviado exitosamente.", "success");
            }
        }
    } else {
        swal("Error!", "Favor de llenar todos los campos.", "error");
    }
}

function convertir() {
    $("#fechaEnvio").load("calendarizar.php");
}

function agregar_foto_perfil() {
    $("#foto_perfil").load("foto_perfil.php");
}

function agregar_multimedia() {
    $("#multimedia").load("multimedia.php");
}

function obtenerClavePais() {
    var PaisId = document.getElementById('PaisId').value;
    var PaisClaveBD = $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: "/paises/pais_clave.php",
        data: {
            PaisId: PaisId
        }
    }).responseText;
    return PaisClaveBD;
}

function establecerPais() {
    var PaisId = document.getElementById('PaisId').value;
    if (PaisId !== '0') {
        var PaisNombreBD = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/paises/pais_nombre.php",
            data: {
                PaisId: PaisId
            }
        }).responseText;
        var PaisNombre = document.getElementById('PaisNombre');
        PaisNombre.innerHTML = "El mensaje se enviara a " + PaisNombreBD;
        PaisNombre.style.color = "#000000";
    }
}

function establecerGrupo() {
    var GrupoId = document.getElementById('GrupoId').value;
    var UsuarioId = document.getElementById('idUsuario').value;
    if (GrupoId !== '0') {
        var GrupoNombreBD = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/grupos/obtener_grupo.php",
            data: {
                GrupoId: GrupoId,
                UsuarioId: UsuarioId
            }
        }).responseText;
        GrupoNombreBD = GrupoNombreBD.substr(1);
        GrupoNombreBD = GrupoNombreBD.slice(0,-1);
        
        var objetoJSON = jQuery.parseJSON(GrupoNombreBD);
        
        var GrupoNombre = document.getElementById('GrupoNombre');
        GrupoNombre.innerHTML = "El mensaje se enviara al grupo de " + objetoJSON.GrupoNombre;
        
        var PaisId = document.getElementById('PaisId');
        PaisId.innerHTML = '<option value="'+objetoJSON.PaisId+'">'+objetoJSON.PaisNombre+'</option>';
        
        var PaisNombre = document.getElementById('PaisNombre');
        PaisNombre.innerHTML = "El mensaje se enviara a " + objetoJSON.PaisNombre;
        
        var Numeros = document.getElementById('tos');
        Numeros.innerHTML = objetoJSON.Numeros;
    }
}

function actualizarGrupo() {
    f1.tos.value = findSMSAddresses(f1.tos.value);
}

function crearGrupo() {
    f1.tos.value = findSMSAddresses(f1.tos.value);
    var GrupoNombre = document.getElementById('GrupoNombre').value;
    var PaisId = document.getElementById('PaisId').value;
    var tos = document.getElementById('tos').value;
    var IdUsuario = document.getElementById('IdUsuario').value;

    var Nombre = GrupoNombre.replace(/\s/g, "");
    var Numeros = tos.replace(/\s/g, "");

    if (Nombre.length > 0 && Numeros.length > 0
            && IdUsuario.length > 0 && PaisId !== "0") {
        var GuardarGrupo = $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "/grupos/crear_grupo.php",
            data: {
                GrupoNombre: Nombre,
                Numeros: Numeros,
                IdUsuario: IdUsuario,
                PaisId: PaisId,
                Creargrupos: "Creargrupos"
            }
        }).responseText;
        if (GuardarGrupo === "true") {
            swal({
                title: "Exitoso",
                text: "Se ha guardado el grupo.",
                type: "success"
            },
            function () {
                window.location.href = "/grupos/";
            });
        } else {
            swal("Error", "Ha ocurrido un error.", "error");
        }
    } else {
        swal("Error!", "Debes llenar todos los campos.", "error");
    }
}