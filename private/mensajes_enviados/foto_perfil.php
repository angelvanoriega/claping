<form enctype="multipart/form-data" class="formulario">
    <table style="width: 70%">
        <tr>
            <td>
                <img id="showImage" style="border-style: solid; height: 56px; width: 56px"/>
            </td>
            <td style="width: 10px"></td>
            <td>
                <input name="archivo" type="file" id="imagen"/>
            </td>
            <td>
                <input type="button" onclick="obtenerImagen()" 
                       value="Subir imagen" id="subirImagen" name="subirImagen"/>
            </td>
            <td style="width: 10px"></td>
            <td style="width: 140px">
                <div class="messages"></div>
            <td>
        </tr>
    </table>
</form>
<input type="button" class="btn btn-facebook" value="Sin foto de perfil" id="btn-sin-foto" name="btn-sin-foto">
<script>
    cargarDatePicker();
    $("#btn-sin-foto").click(function () {
        document.getElementById("foto_perfil").innerHTML =
                "<input type='button' class='btn btn-facebook' value='Personalizar foto de perfil' \n\
onclick='agregar_foto_perfil()'>\n\
<font size='1'>Tamaño máximo 2 MB, 1 crédito extra.</font>";
    });
</script>