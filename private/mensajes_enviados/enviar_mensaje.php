<?php

function crearCadena($fields) {
    $fields_string = '';
    $fields_total = count($fields);
    $contador = 0;
    foreach ($fields as $key => $value) {
        $contador++;
        if ($contador == $fields_total) {
            $fields_string .= $key . '=' . $value;
        } else {
            $fields_string .= $key . '=' . $value . '&';
        }
    }
    rtrim($fields_string, '&');
    return $fields_string;
}

function ejecutarAPI($TOS, $TEXTO, $IDINTERNO, $PERFILIMAGEN,$FECHADESDE) {
    $url = 'http://private.whappend.com/wa_send.asp?';
    $fields = array(
        'api' => 1,
        'usuario' => 'WAC31516',
        'clave' => '946297',
        'TOS' => $TOS,
        //'test' => 1,
        'PERFILIMAGEN' => $PERFILIMAGEN,
        'TEXTO' => urlencode($TEXTO),
        'IDINTERNO' => $IDINTERNO,
        'FECHADESDE' => $FECHADESDE
    );
    $fields_string = crearCadena($fields);
    $result = ejecutarCURL($url, $fields, $fields_string);
    return $result;
}

function ejecutarCURL($url, $fields, $fields_string) {
    $conexionCURL = curl_init();
    curl_setopt($conexionCURL, CURLOPT_URL, $url);
    curl_setopt($conexionCURL, CURLOPT_POST, count($fields));
    curl_setopt($conexionCURL, CURLOPT_POSTFIELDS, $fields_string);
    $result = curl_exec($conexionCURL);
    curl_close($conexionCURL);
    return $result;
}

$TOS = filter_input(INPUT_POST, "TOS");
$TEXTO = filter_input(INPUT_POST, "TEXTO");
$PERFILIMAGEN = filter_input(INPUT_POST, "PERFILIMAGEN");
$IDINTERNO = filter_input(INPUT_POST, "IDINTERNO");
$FECHADESDE = filter_input(INPUT_POST, "FECHADESDE");

if (isset($TOS) and isset($TEXTO)) {
    $api = ejecutarAPI($TOS, $TEXTO, $IDINTERNO, $PERFILIMAGEN,$FECHADESDE);
    echo $api;
} else {
    echo "false";
}
