<?php

session_start();
if ($_SESSION['RolId'] == 1 && $_SESSION['UsuarioActivo'] == 1) {
    require_once 'Controlador_usuarios.php';
    $Controlador = new \controlador\Controlador_usuarios();
    $Controlador->borrar();
} else {
    header('Location: /index.php');
}