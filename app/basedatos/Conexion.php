<?php

namespace basedatos;

require_once 'IRU.php';

class Conexion {

    private $Host;
    private $Puerto;
    private $NombreBD;
    private $Usuario;
    private $Contrasenia;

    public function __construct() {
        header('Cache-Control: no cache'); //no cache
        session_cache_limiter('private_no_expire');
        date_default_timezone_set('America/Mexico_City');
        setlocale(LC_TIME, "");
        error_reporting(-1);
    }

    private function conectar() {
        $this->Host = $GLOBALS['HOST'];
        $this->Puerto = $GLOBALS['PUERTO'];
        $this->NombreBD = $GLOBALS['BASEDATOS'];
        $this->Usuario = $GLOBALS['USUARIO'];
        $this->Contrasenia = $GLOBALS['CONTRASENIA'];
        try {
            $conexion = new \PDO("mysql:host=$this->Host;dbname=$this->NombreBD", $this->Usuario, $this->Contrasenia);
            return $conexion;
        } catch (Exception $ex) {
            $this->alert("No se pudo conectar a la base de datos.");
            print "¡Error!: " . $ex->getMessage() . "<br/>";
            die();
        }
    }

    public function validarError($codigo) {
        switch ($codigo) {
            case 0:
                return true;
            case 23503:
                $this->alert("No puedes borrar este elemento, otras entidades dependen de el.");
                return false;
            case 23505:
                $this->alert("Ya existe un registro con estas caracteristicas.");
                return false;
            default:
                $this->alert($codigo . " Contacta al administrador del sitio.");
                return false;
        }
        die();
    }

    public function ejecutarQuery($query) {
        $conexion = $this->conectar();
        $sentenciaPreparada = $conexion->prepare($query);
        $resultado = $sentenciaPreparada->execute();
        if ($resultado) {
            return $sentenciaPreparada;
        } else {
            $codigo = $sentenciaPreparada->errorCode();
            echo var_dump($sentenciaPreparada->errorInfo());
            $this->validarError($codigo);
            exit();
        }
    }

    private function log($conexion_pg) {
        $date = date("Y-m-d H:i:s");
        $error_message = pg_last_error($conexion_pg);
        $archivoTXT = fopen(realpath(dirname(__FILE__)) . "\..\\external\\error_log.txt", "a+");
        fwrite($archivoTXT, "[[ " . $date . " ]] " . $error_message . PHP_EOL);
        fclose($archivoTXT);
        return $error_message;
    }

    public function controlarSesion() {
        session_start();
        if (empty(($_SESSION['EmpleadoId']))) {
            header('Location: /login.php');
        } else {
            $date = date("Y-m-d H:i:s");
            $DiferenciaMinutos = $this->intervaloMinutos($_SESSION['UltimoInicio'], $date);
            if ($DiferenciaMinutos > 30) {
                session_destroy();
                header('Location: /login.php');
            }
        }
    }

    public function intervaloMinutos($fecha1, $fecha2) {
        $fecha1DT = new \DateTime($fecha1);
        $fecha2DT = new \DateTime($fecha2);
        $intervalo = $fecha1DT->diff($fecha2DT);
        $numeroDias = $intervalo->format('%i');
        return $numeroDias;
    }

    public function alert($mensaje) {
        ?>
        <link rel="stylesheet" type="text/css" href="/assets/javascript/sweetalert-master/sweetalert.css" media="all">
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert-dev.js"></script>
        <br><script type='text/javascript'>
            swal({
                title: "ERROR!!!",
                text: "<?php echo $mensaje ?>",
                type: "error"},
            function () {
                javascript:history.back();
            });
        </script><?php
    }

}
