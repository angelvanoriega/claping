<?php
if ($this->getroles()) {
    foreach ($this->getroles() as $_roles) {
        ?>
        <form role="form" method="post" action="/roles/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-files-o"></i> roles</h3>    
                    </div>
                </div><!-- /.box-header -->
                <hr style="width: 98%;">
                <div class="box-body">
                    <table style="margin: 0 auto; width: 60%">
                        <tr>
                            <td><strong>RolId: </strong></td>
                            <td><input type="text" required class="input-sm" id="RolId" name="RolId" value="<?php echo $_roles['RolId'] ?>"/></td>
                        </tr>
                        <tr style="height: 15px"></tr>
                        <tr>
                            <td><strong>RolNombre: </strong></td>
                            <td><input type="text" required class="input-sm" id="RolNombre" name="RolNombre" value="<?php echo $_roles['RolNombre'] ?>"/></td>
                        </tr>
                        <tr style="height: 15px"></tr>
                    </table>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizarroles" id="Actualizarroles" 
                            value="Actualizarroles">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div style="height: 120px"></div>
        <?php
    }
}