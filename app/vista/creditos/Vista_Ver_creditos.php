<?php
$creditos = $this->getcreditos();
if ($creditos) {
    foreach ($creditos as $_creditos) {
        ?>
        <div class="box box-success" style="width: 70%; margin: 0 auto">
            <div class="box-header">
                <div class="col-xs-10">
                    <h3 class="box-title"><i class="fa fa-files-o"></i> creditos</h3>    
                </div>
            </div><!-- /.box-header -->
            <hr style="width: 98%;">
            <div class="box-body">
                <form role="form">
                    <table style="margin: 0 auto; width: 95%">
<tr>
                            <td><strong>CreditoId: </strong></td>
                            <td><?php echo $_creditos['CreditoId'] ?></td>
                        </tr><tr>
                            <td><strong>CreditoCantidad: </strong></td>
                            <td><?php echo $_creditos['CreditoCantidad'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioId: </strong></td>
                            <td><?php echo $_creditos['UsuarioId'] ?></td>
                        </tr>                        <tr style="height: 15px"></tr>
                    </table>
                </form>
            </div>
        </div>
        <div style="height: 120px"></div>
        <?php
    }
} else {
    ?>
    <div style="height: 350px"></div>
    <?php
}