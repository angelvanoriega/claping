<?php
if ($this->getcreditos()) {
    foreach ($this->getcreditos() as $_creditos) {
        ?>
        <form role="form" method="post" action="/creditos/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-files-o"></i> creditos</h3>    
                    </div>
                </div><!-- /.box-header -->
                <hr style="width: 98%;">
                <div class="box-body">
                    <table style="margin: 0 auto; width: 60%">
<tr>
                            <td><strong>CreditoId: </strong></td>
                            <td><input type="text" required class="input-sm" id="CreditoId" name="CreditoId" value="<?php echo $_creditos['CreditoId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>CreditoCantidad: </strong></td>
                            <td><input type="text" required class="input-sm" id="CreditoCantidad" name="CreditoCantidad" value="<?php echo $_creditos['CreditoCantidad'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>UsuarioId: </strong></td>
                            <td><input type="text" required class="input-sm" id="UsuarioId" name="UsuarioId" value="<?php echo $_creditos['UsuarioId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
                    </table>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizarcreditos" id="Actualizarcreditos" 
                            value="Actualizarcreditos">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div style="height: 120px"></div>
        <?php
    }
}