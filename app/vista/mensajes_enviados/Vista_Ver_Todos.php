<?php
$mensajes_enviados = $this->getTodos();
$cantidad_mensajes = count($mensajes_enviados) - 2;
$gran_array = array();
for ($i = 1; $i <= $cantidad_mensajes; $i++) {
    $columnas = preg_split('/\t/', $mensajes_enviados[$i]);
    //var_dump($columnas);
    $array = array(
        "Destino" => $columnas[0],
        "Texto" => utf8_encode($columnas[1]),
        "Estado" => $columnas[2],
        "Estado WhatsApp" => $columnas[3],
        "Denunciado" => $columnas[4],
        "Perfil" => $columnas[5],
        "Rocket" => $columnas[6],
        "Creditos" => $columnas[7],
        "Campaña" => $columnas[8],
        "FechaEnvio" => $columnas[9],
        "FechaCreacion" => $columnas[10],
        "FechaDesde" => $columnas[11],
        "IP" => $columnas[12],
        "Usuario" => $columnas[13],
        "Id" => $columnas[14]
    );
    array_push($gran_array, $array);
}
?>
<script>
    $(document).ready(function () {
        $('#table_mensajes_enviados').DataTable();
    });
</script>
<div style="height: 50px"></div>
<div style="margin: 0 auto; width: 95%;">
</form><table border="1" id="table_mensajes_enviados">
    <thead style="background-color: #40C3D3">
        <tr>
            <th><strong>Destino</strong></th>
            <th><strong>Texto</strong></th>
            <th><strong>Estado</strong></th>
            <th><strong>Estado WhatsApp</strong></th>
            <th><strong>Perfil</strong></th>
            <th><strong>Créditos</strong></th>
            <th><strong>FechaEnvio</strong></th>
            <th><strong>FechaCreacion</strong></th>
        </tr>
    </thead>
    <?php
    if ($gran_array) {
        foreach ($gran_array as $todos) {
            ?>
            <tr>

                <td>
                    <?php echo $todos["Destino"] ?>
                </td>
                <td>
                    <?php echo $todos["Texto"]?>
                </td>
                <td>
                    <?php echo $todos["Estado"] ?>
                </td>
                <td>
                    <?php echo $todos["Estado WhatsApp"] ?>
                </td>
                <td>
                    <?php echo $todos["Perfil"] ?>
                </td>
                <td>
                    <?php echo $todos["Creditos"] ?>
                </td>
                <td>
                    <?php echo $todos["FechaEnvio"] ?>
                </td>
                <td>
                    <?php echo $todos["FechaCreacion"] ?>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table></div>