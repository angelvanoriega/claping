<?php
$mensajes_enviados = $this->getmensajes_enviados();
if ($mensajes_enviados) {
    foreach ($mensajes_enviados as $_mensajes_enviados) {
        ?>
        <div class="box box-success" style="width: 70%; margin: 0 auto">
            <div class="box-header">
                <div class="col-xs-10">
                    <h3 class="box-title"><i class="fa fa-files-o"></i> mensajes_enviados</h3>    
                </div>
            </div><!-- /.box-header -->
            <hr style="width: 98%;">
            <div class="box-body">
                <form role="form">
                    <table style="margin: 0 auto; width: 95%">
<tr>
                            <td><strong>Mensaje_EnviadoId: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoId'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioId: </strong></td>
                            <td><?php echo $_mensajes_enviados['UsuarioId'] ?></td>
                        </tr><tr>
                            <td><strong>ContactoId: </strong></td>
                            <td><?php echo $_mensajes_enviados['ContactoId'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoFechaCreacion: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoFechaCreacion'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoFechaEnvio: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoFechaEnvio'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoContenido: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoContenido'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoOneCheck: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoOneCheck'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoDoubleCheck: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoDoubleCheck'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoFotoPerfil: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoFotoPerfil'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoTexto: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoTexto'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoDenunciado: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoDenunciado'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoCreditos: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoCreditos'] ?></td>
                        </tr><tr>
                            <td><strong>Mensaje_EnviadoEnvidado: </strong></td>
                            <td><?php echo $_mensajes_enviados['Mensaje_EnviadoEnvidado'] ?></td>
                        </tr>                        <tr style="height: 15px"></tr>
                    </table>
                </form>
            </div>
        </div>
        <div style="height: 120px"></div>
        <?php
    }
} else {
    ?>
    <div style="height: 350px"></div>
    <?php
}