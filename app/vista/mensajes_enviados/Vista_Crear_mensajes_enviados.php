<div style="width: 97%; margin: 0 auto">
    <section class="content-header">
        <h1>Mensaje de texto
            <small>Prepara tus mensajes y campañas de solo texto</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Todo el contenido ponlo aqui -->
        <div id="foto_perfil" name="foto_perfil">
            <input type="button" class="btn btn-facebook" value="Personalizar foto de perfil"
                   onclick="agregar_foto_perfil()"/>
            <font size="1">Tamaño máximo 2 MB, 1 crédito extra.</font> 
        </div><br/>
        <div id="multimedia" name="multimedia">
            <input type="button" class="btn btn-facebook" value="Agregar archivos multimedia."
                   onclick="agregar_multimedia()"/>
            <font size="1">Tamaño máximo 2 MB, 1 crédito extra.</font> 
        </div><br/>
        <input type="hidden" id="idUsuario" name="idUsuario"  value="<?php echo $_SESSION['UsuarioId']; ?>">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-1"><strong>Pais: </strong></div>
            <div class="col-xs-6 col-md-4">
                <select class="form-control" id="PaisId" name="PaisId" onclick="establecerPais()">
                    <option value="0">Selecciona un pais</option>
                    <?php
                    if ($this->getPaises()) {
                        foreach ($this->getPaises() as $paises) {
                            ?>
                            <option value="<?php echo $paises['PaisId']; ?>">
                                <?php echo utf8_encode($paises['PaisNombre'] . " (" . $paises['PaisClaveLada'] . ")"); ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5" id="PaisNombre" name="PaisNombre"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-1"><strong>Grupo: </strong></div>
            <div class="col-xs-6 col-md-4">
                <select class="form-control" id="GrupoId" name="GrupoId" onclick="establecerGrupo()">
                    <option value="0">Selecciona un grupo</option>
                    <?php
                    if ($this->getGrupos()) {
                        foreach ($this->getGrupos() as $grupos) {
                            ?>
                            <option value="<?php echo $grupos['GrupoId']; ?>">
                                <?php echo utf8_encode($grupos['GrupoNombre']); ?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5" id="GrupoNombre" name="GrupoNombre"></div>
        </div>
        <br/>
        <form name=f1 id=f1 target=spam action=enviar_sms.asp style='margin:0' method=post>
            <section>
                <div id=sectornumeros>
                    <div style="display:inline;float:right">
                        <a href="javascript://" onclick="document.f1.tos.value = ''">Borrar listado</a> |
                        <a href="javascript://" onclick="f1.tos.value = findSMSAddresses(f1.tos.value);">Quitar renglones duplicados</a> |
                        <a href="javascript://" onclick="f1.tos.value = findSMSAddresses(f1.tos.value);">Analizar listado y preparar para personalización</a> 

                    </div>
                    <div style="display:inline;">
                        <strong>Ingrese los números uno debajo del otro</strong>
                    </div>

                    <textarea rows=4 style='width:100%;resize:none;' id=tos name=tos class="form-control" onchange="g_numeros_corregidos = false;
                            findSMSAddresses(f1.tos.value);"></textarea><br>

                    <div id="divcorregir" style="display:none">
                        <input class=botongrisgrande type=button value="Corregir y limpiar" onclick="
                                fwa.borrarerrores.value = '1';
                                corregirnumeros();">
                    </div>
                </div>
                <iframe name=traerspam id=traerspam src=about:blank frameborder=0 style="display:none;"></iframe>
            </section>
        </form>
        <div id="fechaEnvio" name="fechaEnvio">
            <button class="btn btn-primary" onclick="convertir()">Calendarizar</button>
        </div>
        <br/>
        <form action="#" method="post"> 
            <div class="form-group">
                <label>Texto del mensaje (
                    <label name="caracteresIntroducidos" id="caracteresIntroducidos">0</label> caracteres, restan 
                    <label name="caracteresRestantes" id="caracteresRestantes">3000</label>):
                </label>
                <textarea maxlength="3000" class="form-control" rows="4" style="resize:none;" id="contenidoMensaje"
                          name="contenidoMensaje" onload="cuenta()" onclick="cuenta()" onKeyDown="cuenta()" onKeyUp="cuenta()"></textarea>
                <div name="caracteres" id="caracteres"></div>

            </div>
        </form>
        <button style="float: right" class="btn btn-success" 
                id="EnviarMensaje" name="EnviarMensaje" onclick="EnviarMensaje()">Enviar mensaje</button>
    </section>
</div>
<div style="height: 50px"></div>
