<?php
if ($this->getmensajes_enviados()) {
    foreach ($this->getmensajes_enviados() as $_mensajes_enviados) {
        ?>
        <form role="form" method="post" action="/mensajes_enviados/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-files-o"></i> mensajes_enviados</h3>    
                    </div>
                </div><!-- /.box-header -->
                <hr style="width: 98%;">
                <div class="box-body">
                    <table style="margin: 0 auto; width: 60%">
<tr>
                            <td><strong>Mensaje_EnviadoId: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoId" name="Mensaje_EnviadoId" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>UsuarioId: </strong></td>
                            <td><input type="text" required class="input-sm" id="UsuarioId" name="UsuarioId" value="<?php echo $_mensajes_enviados['UsuarioId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>ContactoId: </strong></td>
                            <td><input type="text" required class="input-sm" id="ContactoId" name="ContactoId" value="<?php echo $_mensajes_enviados['ContactoId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoFechaCreacion: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoFechaCreacion" name="Mensaje_EnviadoFechaCreacion" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoFechaCreacion'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoFechaEnvio: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoFechaEnvio" name="Mensaje_EnviadoFechaEnvio" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoFechaEnvio'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoContenido: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoContenido" name="Mensaje_EnviadoContenido" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoContenido'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoOneCheck: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoOneCheck" name="Mensaje_EnviadoOneCheck" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoOneCheck'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoDoubleCheck: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoDoubleCheck" name="Mensaje_EnviadoDoubleCheck" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoDoubleCheck'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoFotoPerfil: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoFotoPerfil" name="Mensaje_EnviadoFotoPerfil" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoFotoPerfil'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoTexto: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoTexto" name="Mensaje_EnviadoTexto" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoTexto'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoDenunciado: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoDenunciado" name="Mensaje_EnviadoDenunciado" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoDenunciado'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoCreditos: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoCreditos" name="Mensaje_EnviadoCreditos" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoCreditos'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_EnviadoEnvidado: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_EnviadoEnvidado" name="Mensaje_EnviadoEnvidado" value="<?php echo $_mensajes_enviados['Mensaje_EnviadoEnvidado'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
                    </table>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizarmensajes_enviados" id="Actualizarmensajes_enviados" 
                            value="Actualizarmensajes_enviados">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div style="height: 120px"></div>
        <?php
    }
}