<div style="height: 50px"></div>
<?php
if ($this->getusuarios()) {
    foreach ($this->getusuarios() as $_usuarios) {
        ?>
        <form method="post" action="/usuarios/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header with-border">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-user"></i> Actualizar mis datos</h3>    
                    </div>
                </div>
                <div class="box-body clearfix" style="margin: 0 auto; width: 100%;">
                    <div style="margin: 0 auto;">
                        <div class="col-md-12">
                            <div class="col-md-6"><strong>Nombre:</strong>
                                <label id="validacionNombre" name="validacionNombre"></label></div>
                            <div class="col-md-6"><strong>Apellidos:</strong>
                                <label id="validacionApellido" name="validacionApellido"></label></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <input required type="text" id="UsuarioNombre" name="UsuarioNombre" class="form-control"
                                       onload="validarNombreUsuario()" onclick="validarNombreUsuario()" 
                                       onKeyDown="validarNombreUsuario()" onKeyUp="validarNombreUsuario()" 
                                       value="<?php echo $_usuarios['UsuarioNombre'] ?>">
                            </div>
                            <div class="col-md-6">
                                <input required type="text" id="UsuarioApellido" name="UsuarioApellido" class="form-control"
                                       onload="validarApellidoUsuario()" onclick="validarApellidoUsuario()" 
                                       onKeyDown="validarApellidoUsuario()" onKeyUp="validarApellidoUsuario()"
                                       value="<?php echo $_usuarios['UsuarioApellido'] ?>">
                                <input type="hidden" id="UsuarioId" name="UsuarioId" value="<?php echo $_usuarios['UsuarioId'] ?>">
                            </div>
                        </div>
                        <div style="height: 80px"></div>
                        <div class="col-md-12">
                            <div class="col-md-3"><strong>Nombre de usuario:</strong>
                                <label id="validacionSobreNombre" name="validacionSobreNombre"></label></div>
                            <div class="col-md-9">
                                <input required type="text" id="UsuarioSobreNombre" name="UsuarioSobreNombre" class="form-control"
                                       onload="validarSobreNombre()" onclick="validarSobreNombre()" 
                                       onKeyDown="validarSobreNombre()" onKeyUp="validarSobreNombre()"
                                       value="<?php echo $_usuarios['UsuarioSobreNombre'] ?>">
                            </div>
                        </div>
                        <div style="height: 60px"></div>
                        <div class="col-md-12">
                            <div class="col-md-3"><strong>Correo electronico:</strong>
                                <label id="validacionEmail" name="validacionEmail"></label></div>
                            <div class="col-md-9">
                                <input required type="text" id="UsuarioEmail" name="UsuarioEmail" class="form-control"
                                       onload="validarEmailActualizar()" onclick="validarEmailActualizar()" 
                                       onKeyDown="validarEmailActualizar()" onKeyUp="validarEmailActualizar()"
                                       value="<?php echo $_usuarios['UsuarioEmail'] ?>">
                            </div>
                        </div>  
                        <div style="height: 60px"></div>
                        <div class="col-md-12">
                            <div class="col-md-3"><strong>Pais:</strong></div>
                            <div class="col-md-9">
                                <select required class="form-control" name="PaisId" id="PaisId">
                                    <option value="AF">Afganistán (+93)</option>
                                    <option value="AL">Albania (+355)</option>
                                    <option value="DE">Alemania (+49)</option>
                                    <option value="AD">Andorra (+376)</option>
                                    <option value="AO">Angola (+244)</option>
                                    <option value="AQ">Antártida (+672)</option>
                                    <option value="AG">Antigua y Barbuda (+1268)</option>
                                    <option value="AN">Antillas Holandesas (+599)</option>
                                    <option value="SA">Arabia Saudí (+966)</option>
                                    <option value="DZ">Argelia (+213)</option>
                                    <option value="AR">Argentina (+54)</option>
                                    <option value="AM">Armenia (+374)</option>
                                    <option value="AW">Aruba (+297)</option>
                                    <option value="AU">Australia (+61)</option>
                                    <option value="AT">Austria (+43)</option>
                                    <option value="AZ">Azerbaiyán (+994)</option>
                                    <option value="BS">Bahamas (+1242)</option>
                                    <option value="BD">Bangladesh (+880)</option>
                                    <option value="BB">Barbados (+1246)</option>
                                    <option value="BE">Bélgica (+32)</option>
                                    <option value="BJ">Benin (+229)</option>
                                    <option value="BO">Bolivia (+591)</option>
                                    <option value="BA">Bosnia y Herzegovina (+387)</option>
                                    <option value="BW">Botswana (+267)</option>
                                    <option value="BR">Brasil (+55)</option>
                                    <option value="BG">Bulgaria (+359)</option>
                                    <option value="BF">Burkina Faso (+226)</option>
                                    <option value="BI">Burundi (+257)</option>
                                    <option value="CV">Cabo Verde (+238)</option>
                                    <option value="KH">Camboya (+855)</option>
                                    <option value="CM">Camerún (+237)</option>
                                    <option value="CA">Canadá (+1)</option>
                                    <option value="TD">Chad (+235)</option>
                                    <option value="CL">Chile (+56)</option>
                                    <option value="CN">China (+86)</option>
                                    <option value="CY">Chipre (+357)</option>
                                    <option value="CO">Colombia (+57)</option>
                                    <option value="CR">Costa Rica (+506)</option>
                                    <option value="CU">Cuba (+53)</option>
                                    <option value="DK">Dinamarca (+45)</option>
                                    <option value="DJ">Djibouti (+253)</option>
                                    <option value="DM">Dominica (+1767)</option>
                                    <option value="EC">Ecuador (+593)</option>
                                    <option value="EG">Egipto (+20)</option>
                                    <option value="SV">El Salvador (+503)</option>
                                    <option value="AE">Emiratos Árabes Unidos (+971)</option>
                                    <option value="ER">Eritrea (+291)</option>
                                    <option value="SI">Eslovenia (+386)</option>
                                    <option value="ES">España (+34)</option>
                                    <option value="US">Estados Unidos (+1)</option>
                                    <option value="EE">Estonia (+372)</option>
                                    <option value="ET">Etiopía (+251)</option>
                                    <option value="FJ">Fiji (+679)</option>
                                    <option value="PH">Filipinas (+63)</option>
                                    <option value="FI">Finlandia (+358)</option>
                                    <option value="FR">Francia (+33)</option>
                                    <option value="GA">Gabón (+241)</option>
                                    <option value="GM">Gambia (+220)</option>
                                    <option value="GE">Georgia (+995)</option>
                                    <option value="GH">Ghana (+233)</option>
                                    <option value="GI">Gibraltar (+350)</option>
                                    <option value="GD">Granada (+1473)</option>
                                    <option value="GR">Grecia (+30)</option>
                                    <option value="GL">Groenlandia (+299)</option>
                                    <option value="GP">Guadalupe (+590)</option>
                                    <option value="GU">Guam (+1671)</option>
                                    <option value="GT">Guatemala (+502)</option>
                                    <option value="GN">Guinea (+224)</option>
                                    <option value="GQ">Guinea Ecuatorial (+240)</option>
                                    <option value="GW">Guinea-Bissau (+245)</option>
                                    <option value="HT">Haití (+509)</option>
                                    <option value="HN">Honduras (+504)</option>
                                    <option value="HU">Hungría (+36)</option>
                                    <option value="IN">India (+91)</option>
                                    <option value="ID">Indonesia (+62)</option>
                                    <option value="IQ">Irak (+964)</option>
                                    <option value="IR">Irán (+98)</option>
                                    <option value="IS">Islandia (+354)</option>
                                    <option value="KY">Islas Caimán (+1345)</option>
                                    <option value="CK">Islas Cook (+682)</option>
                                    <option value="FK">Islas Malvinas (+500)</option>
                                    <option value="MH">Islas Marshall (+692)</option>
                                    <option value="SB">Islas Salomón (+677)</option>
                                    <option value="TC">Islas Turks y Caicos (+1649)</option>
                                    <option value="IL">Israel (+972)</option>
                                    <option value="IT">Italia (+39)</option>
                                    <option value="JM">Jamaica (+1876)</option>
                                    <option value="JP">Japón (+81)</option>
                                    <option value="JO">Jordania (+962)</option>
                                    <option value="KE">Kenia (+254)</option>
                                    <option value="KI">Kiribati (+686)</option>
                                    <option value="KW">Kuwait (+965)</option>
                                    <option value="LV">Letonia (+371)</option>
                                    <option value="LR">Liberia (+231)</option>
                                    <option value="LY">Libia (+218)</option>
                                    <option value="LI">Liechtenstein (+41)</option>
                                    <option value="LT">Lituania (+370)</option>
                                    <option value="LU">Luxemburgo (+352)</option>
                                    <option value="MG">Madagascar (+261)</option>
                                    <option value="MY">Malasia (+60)</option>
                                    <option value="MW">Malawi (+265)</option>
                                    <option value="MV">Maldivas (+960)</option>
                                    <option value="MT">Malta (+356)</option>
                                    <option value="MA">Marruecos (+212)</option>
                                    <option value="MQ">Martinica (+596)</option>
                                    <option value="MU">Mauricio (+230)</option>
                                    <option value="MR">Mauritania (+222)</option>
                                    <option value="MX">México (+52)</option>
                                    <option value="MD">Moldavia (+373)</option>
                                    <option value="MC">Mónaco (+377)</option>
                                    <option value="MN">Mongolia (+976)</option>
                                    <option value="MS">Montserrat (+1664)</option>
                                    <option value="MZ">Mozambique (+258)</option>
                                    <option value="NA">Namibia (+264)</option>
                                    <option value="NR">Nauru (+674)</option>
                                    <option value="NP">Nepal (+977)</option>
                                    <option value="NI">Nicaragua (+505)</option>
                                    <option value="NE">Níger (+227)</option>
                                    <option value="NG">Nigeria (+234)</option>
                                    <option value="NU">Niue (+683)</option>
                                    <option value="NO">Noruega (+47)</option>
                                    <option value="NC">Nueva Caledonia (+687)</option>
                                    <option value="NZ">Nueva Zelanda (+64)</option>
                                    <option value="OM">Omán (+968)</option>
                                    <option value="NL">Países Bajos (+31)</option>
                                    <option value="PA">Panamá (+507)</option>
                                    <option value="PG">Papúa Nueva Guinea (+675)</option>
                                    <option value="PY">Paraguay (+595)</option>
                                    <option value="PE">Perú (+51)</option>
                                    <option value="PN">Pitcairn (+872)</option>
                                    <option value="PF">Polinesia Francesa (+689)</option>
                                    <option value="PL">Polonia (+48)</option>
                                    <option value="PT">Portugal (+351)</option>
                                    <option value="PR">Puerto Rico (+1)</option>
                                    <option value="QA">Qatar (+974)</option>
                                    <option value="UK">Reino Unido (+44)</option>
                                    <option value="CF">República Centroafricana (+236)</option>
                                    <option value="DO">República Dominicana (+1809)</option>
                                    <option value="RE">Reunión (+262)</option>
                                    <option value="RW">Ruanda (+250)</option>
                                    <option value="RU">Rusia (+7)</option>
                                    <option value="SM">San Marino (+378)</option>
                                    <option value="SH">Santa Helena (+290)</option>
                                    <option value="LC">Santa Lucía (+1758)</option>
                                    <option value="ST">Santo Tomé y Príncipe (+239)</option>
                                    <option value="SN">Senegal (+221)</option>
                                    <option value="SC">Seychelles (+248)</option>
                                    <option value="SL">Sierra Leona (+232)</option>
                                    <option value="SG">Singapur (+65)</option>
                                    <option value="SY">Siria (+963)</option>
                                    <option value="SO">Somalia (+252)</option>
                                    <option value="LK">Sri Lanka (+94)</option>
                                    <option value="SZ">Suazilandia (+268)</option>
                                    <option value="SD">Sudán (+249)</option>
                                    <option value="SE">Suecia (+46)</option>
                                    <option value="CH">Suiza (+41)</option>
                                    <option value="TH">Tailandia (+66)</option>
                                    <option value="TW">Taiwán (+886)</option>
                                    <option value="TZ">Tanzania (+255/259)</option>
                                    <option value="TG">Togo (+228)</option>
                                    <option value="TT">Trinidad y Tobago (+1868)</option>
                                    <option value="TN">Túnez (+216)</option>
                                    <option value="TM">Turkmenistán (+993)</option>
                                    <option value="TR">Turquía (+90)</option>
                                    <option value="TV">Tuvalu (+688)</option>
                                    <option value="UA">Ucrania (+380)</option>
                                    <option value="UG">Uganda (+256)</option>
                                    <option value="UY">Uruguay (+598)</option>
                                    <option value="UZ">Uzbekistán (+998)</option>
                                    <option value="VU">Vanuatu (+678)</option>
                                    <option value="VE">Venezuela (+58)</option>
                                    <option value="VN">Vietnam (+84)</option>
                                    <option value="YE">Yemen (+967)</option>
                                    <option value="YU">Yugoslavia (+381)</option>
                                    <option value="ZM">Zambia (+260)</option>
                                    <option value="ZW">Zimbawe (+263)</option>
                                </select>
                                <script>
                                    document.getElementById("PaisId").value = '<?php echo $_usuarios['PaisId']?>';
                                </script>
                            </div>
                        </div>
                        <div style="height: 60px"></div>
                        <div class="col-md-12">
                            <div class="col-md-3"><strong>Celular:</strong>
                                <label id="validacionCelular" name="validacionCelular"></label></div>
                            <div class="col-md-9">
                                <input required type="text" id="UsuarioNumeroCelular" name="UsuarioNumeroCelular" class="form-control"
                                       onload="validarCelular()" onclick="validarCelular()" 
                                       onKeyDown="validarCelular()" onKeyUp="validarCelular()"
                                       value="<?php echo $_usuarios['UsuarioNumeroCelular'] ?>">
                            </div>
                        </div>
                        <div style="height: 40px"></div>
                        <div class="col-md-12" onload="validarSexo()" onclick="validarSexo()" 
                             onKeyDown="validarSexo()" onKeyUp="validarSexo()">
                            <div class="col-md-3"><strong>Sexo:</strong>
                                <label id="validacionSexo" name="validacionSexo"></label></div>
                            <div class="col-md-4">
                                <input type="radio" id="UsuarioSexoHombre" name="UsuarioSexo" value="H"> Hombre
                            </div>
                            <div class="col-md-4">
                                <input type="radio" id="UsuarioSexoMujer" name="UsuarioSexo" value="M"> Mujer
                            </div>
                            <?php
                            if ($_usuarios['UsuarioSexo'] == 'H') {
                                ?>
                                <script>
                                    document.getElementById("UsuarioSexoHombre").checked = true;
                                    document.getElementById("UsuarioSexoMujer").checked = false;
                                </script>
                                <?php
                            } else {
                                ?>
                                <script>
                                    document.getElementById("UsuarioSexoHombre").checked = false;
                                    document.getElementById("UsuarioSexoMujer").checked = true;
                                </script>
                                <?php
                            }
                            ?>
                        </div>
                        <div style="height: 40px"></div>
                        <div class="col-md-12">
                            <div class="col-md-3"><strong>Fecha de nacimiento:</strong>
                                <label id="validacionFechaNacimiento" name="validacionFechaNacimiento"></label></div>
                            <div class="col-md-3">
                                <select class="form-control" name="Dia" id="Dia"
                                        onload="validarFechaNacimiento()" onclick="validarFechaNacimiento()" 
                                        onKeyDown="validarFechaNacimiento()" onKeyUp="validarFechaNacimiento()">
                                            <?php
                                            $FechaNacimiento = $_usuarios['UsuarioFechaNacimiento'];
                                            $Nacimiento = explode("-", $FechaNacimiento);
                                            $diaBD = $Nacimiento[2];
                                            $MesBD = $Nacimiento[1];
                                            $AnioBD = $Nacimiento[0];
                                            for ($dia = 1; $dia <= 31; $dia++) {
                                                if ($dia < 10) {
                                                    $dia = '0' . $dia;
                                                }
                                                if ($dia == $diaBD) {
                                                    ?>
                                            <option value="<?php echo $diaBD; ?>" selected><?php echo $diaBD; ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $dia; ?>"><?php echo $dia; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="Mes" id="Mes" 
                                        onload="validarFechaNacimiento()" onclick="validarFechaNacimiento()" 
                                        onKeyDown="validarFechaNacimiento()" onKeyUp="validarFechaNacimiento()">
                                            <?php
                                            for ($mes = 1; $mes <= 12; $mes++) {
                                                if ($mes == $MesBD) {
                                                    ?>
                                            <option value="<?php echo $MesBD; ?>" selected><?php echo $MesBD; ?></option>
                                            <?php
                                        } else {
                                            if ($mes < 10) {
                                                $mes = '0' . $mes;
                                            }
                                            ?>
                                            <option value="<?php echo $mes; ?>"><?php echo $mes; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control" name="Anio" id="Anio"
                                        onload="validarFechaNacimiento()" onclick="validarFechaNacimiento()" 
                                        onKeyDown="validarFechaNacimiento()" onKeyUp="validarFechaNacimiento()">
                                            <?php
                                            $AnioLimite = date('Y') - 10;
                                            for ($anio = $AnioLimite; $anio >= 1950; $anio--) {
                                                if ($anio == $AnioBD) {
                                                    ?>
                                            <option value="<?php echo $AnioBD; ?>" selected><?php echo $AnioBD; ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $anio; ?>"><?php echo $anio; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="height: 60px"></div>
                    <input type="hidden" id="UsuarioId" name="UsuarioId" value='<?php echo $_usuarios['UsuarioId']?>'/>
                    <button type="button" class="btn btn-primary" style="float: right" 
                            name="Actualizarusuarios" id="Actualizarusuarios" 
                            value="Actualizarusuarios" onclick="
                                validarActualziarUsuario('<?php echo $_usuarios['UsuarioSobreNombre']?>',
                                '<?php echo $_usuarios['UsuarioEmail']?>')">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <?php
    }
}