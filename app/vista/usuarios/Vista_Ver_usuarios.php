<?php
$usuarios = $this->getusuarios();
if ($usuarios) {
    foreach ($usuarios as $_usuarios) {
        ?>
        <div class="box box-success" style="width: 70%; margin: 0 auto">
            <div class="box-header">
                <div class="col-xs-10">
                    <h3 class="box-title"><i class="fa fa-files-o"></i> usuarios</h3>    
                </div>
            </div><!-- /.box-header -->
            <hr style="width: 98%;">
            <div class="box-body">
                <form role="form">
                    <table style="margin: 0 auto; width: 95%">
<tr>
                            <td><strong>UsuarioId: </strong></td>
                            <td><?php echo $_usuarios['UsuarioId'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioNombre: </strong></td>
                            <td><?php echo $_usuarios['UsuarioNombre'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioSobreNombre: </strong></td>
                            <td><?php echo $_usuarios['UsuarioSobreNombre'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioEmail: </strong></td>
                            <td><?php echo $_usuarios['UsuarioEmail'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioContrasenia: </strong></td>
                            <td><?php echo $_usuarios['UsuarioContrasenia'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioFechaRegistro: </strong></td>
                            <td><?php echo $_usuarios['UsuarioFechaRegistro'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioActivo: </strong></td>
                            <td><?php echo $_usuarios['UsuarioActivo'] ?></td>
                        </tr><tr>
                            <td><strong>PaisId: </strong></td>
                            <td><?php echo $_usuarios['PaisId'] ?></td>
                        </tr><tr>
                            <td><strong>UsuarioNumeroCelular: </strong></td>
                            <td><?php echo $_usuarios['UsuarioNumeroCelular'] ?></td>
                        </tr><tr>
                            <td><strong>RolId: </strong></td>
                            <td><?php echo $_usuarios['RolId'] ?></td>
                        </tr>                        <tr style="height: 15px"></tr>
                    </table>
                </form>
            </div>
        </div>
        <div style="height: 120px"></div>
        <?php
    }
} else {
    ?>
    <div style="height: 350px"></div>
    <?php
}