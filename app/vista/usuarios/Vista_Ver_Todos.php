<div style="height: 30px"></div>
<div style="margin: 0 auto; width: 95%;">
    <script>
        $(document).ready(function () {
            $('#TableUsuarios').DataTable();
        });
    </script>
    <table id="TableUsuarios"  border="1" style="background-color: #40C3D3">
        <thead>
            <tr>
                <th><strong>Nombre</strong></th>
                <th><strong>Sobre nombre</strong></th>
                <th><strong>Correo</strong></th>
                <th><strong>Celular</strong></th>
                <th><strong>Rol</strong></th>
                <th><strong>Activo</strong></th>
                <th><strong>Pais</strong></th>
                <th><strong>Editar</strong></th>
                <th><strong>Borrar</strong></th>
            </tr>
        </thead>
        <?php
        if ($this->getTodos()) {
            foreach ($this->getTodos() as $todos) {
                ?>
                <tr>
                    <td>
                        <form action='/usuarios/ver.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo $todos["UsuarioNombre"] . " " . $todos["UsuarioApellido"] ?>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form action='/usuarios/ver.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo $todos["UsuarioSobreNombre"] ?>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form action='/usuarios/ver.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo $todos["UsuarioEmail"] ?>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form action='/usuarios/ver.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo $todos["UsuarioNumeroCelular"] ?>
                            </button>
                        </form>
                    </td>
                    <td>
                        <button style="width: 100%; height: 100%" 
                        <?php if ($todos["RolId"] == 1) {
                            ?>class="btn btn-success"<?php
                                } else {
                                    ?>class="btn btn-primary"<?php }
                                ?> 
                                id="CambiarRol" name="CambiarRol" type='button' 
                                onclick="cambiarRol(<?php echo $todos["RolId"] ?>,<?php echo $todos["UsuarioId"] ?>)">
                                    <?php echo $todos["RolNombre"]; ?>
                        </button>
                    </td>
                    <td>
                        <input type="hidden" id="UsuarioActivo" name=UsuarioActivo" value="<?php echo $todos["UsuarioActivo"] ?>">
                        <input type="hidden" id="UsuarioId" name=UsuarioId" value="<?php echo $todos["UsuarioId"] ?>">
                        <button style="width: 100%; height: 100%" 
                        <?php if ($todos["UsuarioActivo"] == 1) {
                            ?>class="btn btn-success"<?php
                                } else {
                                    ?>class="btn btn-primary"<?php }
                                ?> id="CambiarActivo" name="CambiarActivo" type='button' 
                                onclick="cambiarActivo(<?php echo $todos["UsuarioActivo"] ?>,<?php echo $todos["UsuarioId"] ?>)">
                                    <?php
                                    if ($todos["UsuarioActivo"] == 1) {
                                        echo 'Si';
                                    } else {
                                        echo 'No';
                                    }
                                    ?>
                        </button>
                    </td>
                    <td>
                        <form action='/usuarios/ver.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
        <?php echo $todos["PaisNombre"]; ?>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form action='/usuarios/actualizar.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent" 
                                    class="btn" type='submit'>
                                <i class='fa fa-pencil'>
                                </i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form id="borrarusuarios" action='/usuarios/borrar.php' method='post'>
                            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $todos["UsuarioId"] ?>'/>
                            <button style="background: transparent" class="btn" type='submit'>
                                <i class='fa fa-times'>
                                </i>
                            </button>
                        </form>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</div>
<div style="height: 30px"></div>