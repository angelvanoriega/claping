<div style="width: 97%; margin: 0 auto">
    <form name="cambiar_clave" id="cambiar_clave" action="cambiar-clave.php" method="post" autocomplete="off">
        <h2>Cambiar contraseña</h2>
        <table style="width: 30%">
            <tr>
                <td>Contraseña antigua: </td>
                <td><input type="password" id="UsuarioContraseniaMod"  class="form-control"
                           name="UsuarioContraseniaMod" autocomplete="off" required="true"></td>
            </tr>
            <tr style="height: 20px"></tr>
            <tr>
                <td style="height: 20%">Contraseña nueva:</td>
                <td><input type="password" required="true" id="UsuarioContrasenia" class="form-control"
                           name="UsuarioContrasenia" onload="validarContrasenia()" onclick="validarContrasenia()" 
                           onKeyDown="validarContrasenia()" onKeyUp="validarContrasenia()">
                </td>
            </tr>
            <tr style="height: 20px">
                <td></td>
                <td><label style="float: left"id="validacionContrasenia" name="validacionContrasenia"></label></td>
            </tr>
            <tr>
                <td>Repetir contraseña:</td>
                <td><input type="password" required="true" id="UsuarioContraseniaConfirmacion" class="form-control"
                           name="UsuarioContraseniaConfirmacion" onload="validarContrasenia()" onclick="validarContrasenia()" 
                           onKeyDown="validarContrasenia()" onKeyUp="validarContrasenia()"></td>
            </tr>
            <tr style="height: 20px">
                <td></td>
                <td><label style="float: left"id="validacionContraseniaConfirmacion" 
                           name="validacionContraseniaConfirmacion"></label></td>
            </tr>
            <tr>
                <td></td>
                <td style="float: left">
                    <input type="submit" value="Cambiar"></td>
            </tr>
        </table>
    </form>
</div>