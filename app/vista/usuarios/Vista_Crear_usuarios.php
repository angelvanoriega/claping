<div class="box box-primary" style="width: 95%; margin: 0 auto;">
    <div class="box-header" style="height: 40px">
        <h3 class="box-title"><i class="fa fa-calendar"></i> Crear nuevo</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body" style="height: 30%;">
        <div style="text-align: center" >
            <form name="Crearusuarios" id="Crearusuarios" action="nuevo.php" method="post">
                <table style="margin: 0 auto; width: 95%">
                    <tr>
                        <td><strong>UsuarioId</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioId" name="UsuarioId" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioNombre</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioNombre" name="UsuarioNombre" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioSobreNombre</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioSobreNombre" name="UsuarioSobreNombre" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioEmail</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioEmail" name="UsuarioEmail" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioContrasenia</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioContrasenia" name="UsuarioContrasenia" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioFechaRegistro</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioFechaRegistro" name="UsuarioFechaRegistro" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioActivo</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioActivo" name="UsuarioActivo" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>PaisId</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="PaisId" name="PaisId" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>UsuarioNumeroCelular</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="UsuarioNumeroCelular" name="UsuarioNumeroCelular" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td><strong>RolId</strong></td>
                        <td style="width: 20px"></td>
                        <td><input required type="text" id="RolId" name="RolId" class="form-control"></td>
                    </tr>
                    <tr style="height: 20px"></tr>
                </table>
                <button style="float: right; width: 150px" class="btn btn-success" 
                        type="submit" id="Crearusuarios" name="Crearusuarios" value="Crearusuarios">Grabar</button>
            </form>
        </div>
    </div>
</div>