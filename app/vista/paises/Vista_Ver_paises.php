<?php
$paises = $this->getpaises();
if ($paises) {
    foreach ($paises as $_paises) {
        ?>
        <div class="box box-success" style="width: 70%; margin: 0 auto">
            <div class="box-header">
                <div class="col-xs-10">
                    <h3 class="box-title"><i class="fa fa-files-o"></i> paises</h3>    
                </div>
            </div><!-- /.box-header -->
            <hr style="width: 98%;">
            <div class="box-body">
                <form role="form">
                    <table style="margin: 0 auto; width: 95%">
<tr>
                            <td><strong>PaisId: </strong></td>
                            <td><?php echo $_paises['PaisId'] ?></td>
                        </tr><tr>
                            <td><strong>PaisNombre: </strong></td>
                            <td><?php echo $_paises['PaisNombre'] ?></td>
                        </tr><tr>
                            <td><strong>PaisClaveLada: </strong></td>
                            <td><?php echo $_paises['PaisClaveLada'] ?></td>
                        </tr>                        <tr style="height: 15px"></tr>
                    </table>
                </form>
            </div>
        </div>
        <div style="height: 120px"></div>
        <?php
    }
} else {
    ?>
    <div style="height: 350px"></div>
    <?php
}