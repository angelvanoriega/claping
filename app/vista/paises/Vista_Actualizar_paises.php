<?php
if ($this->getpaises()) {
    foreach ($this->getpaises() as $_paises) {
        ?>
        <form role="form" method="post" action="/paises/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-files-o"></i> paises</h3>    
                    </div>
                </div><!-- /.box-header -->
                <hr style="width: 98%;">
                <div class="box-body">
                    <table style="margin: 0 auto; width: 60%">
<tr>
                            <td><strong>PaisId: </strong></td>
                            <td><input type="text" required class="input-sm" id="PaisId" name="PaisId" value="<?php echo $_paises['PaisId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>PaisNombre: </strong></td>
                            <td><input type="text" required class="input-sm" id="PaisNombre" name="PaisNombre" value="<?php echo $_paises['PaisNombre'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>PaisClaveLada: </strong></td>
                            <td><input type="text" required class="input-sm" id="PaisClaveLada" name="PaisClaveLada" value="<?php echo $_paises['PaisClaveLada'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
                    </table>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizarpaises" id="Actualizarpaises" 
                            value="Actualizarpaises">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div style="height: 120px"></div>
        <?php
    }
}