<?php
$grupos = $this->getgrupos();
if ($grupos) {
    foreach ($grupos as $_grupos) {
        ?>
        <div style="width: 95%; margin: 0 auto;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Grupo - <?php echo $_grupos['GrupoNombre'] ?></h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <form name="f1" id="f1" target=spam role="form" method="post" action="/grupos/actualizar.php">
                    <input type="hidden" id="UsuarioId" name="UsuarioId" value="<?php echo $_SESSION['UsuarioId']; ?>">
                    <input type="hidden" id="GrupoId" name="GrupoId" value="<?php echo $_grupos['GrupoId']; ?>">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-1"><strong>Nombre: </strong></div>
                        <div class="col-xs-6 col-md-4">
                            <input required type="text" id="GrupoNombre" name="GrupoNombre" value="<?php echo $_grupos['GrupoNombre'] ?>"
                                   maxlength="45" class="form-control" >
                        </div>
                        <?php if ($_SESSION['RolId'] == 1) {
                            ?>
                            <br>
                            <br>
                            <br>
                            <div class="col-xs-12 col-sm-6 col-md-1"><strong>Creador: </strong></div>
                            <div class="col-xs-6 col-md-4">
                                <input disabled="true" type="text" value="<?php echo $_grupos['UsuarioNombre'] ?>"
                                       maxlength="45" class="form-control" >
                            </div>
                            <?php
                        }
                        ?>
                        <br>
                        <br>
                        <br>
                        <div class="col-xs-12 col-sm-6 col-md-1"><strong>Pais: </strong></div>
                        <div class="col-xs-6 col-md-4">
                            <select class="form-control" id="PaisId" name="PaisId">
                                <option value="0">Selecciona un pais</option>
                                <?php
                                if ($this->getPaises()) {
                                    foreach ($this->getPaises() as $paises) {
                                        ?>
                                        <option value="<?php echo $paises['PaisId']; ?>">
                                            <?php echo utf8_encode($paises['PaisNombre'] . " (" . $paises['PaisClaveLada'] . ")"); ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <br/>
                    <div id=sectornumeros>
                        <div style="display:inline;float:right">
                            <a href="javascript://" onclick="document.f1.tos.value = ''">Borrar listado</a> |
                            <a href="javascript://" onclick="f1.tos.value = findSMSAddresses(f1.tos.value);">Quitar renglones duplicados</a> |
                            <a href="javascript://" onclick="f1.tos.value = findSMSAddresses(f1.tos.value);">Analizar listado y preparar para personalización</a> 

                        </div>
                        <div style="display:inline;">
                            <strong>Ingrese los números uno debajo del otro</strong>
                        </div>

                        <textarea rows=4 style='width:100%;resize:none;' id=tos name=tos 
                                  class="form-control" onchange="g_numeros_corregidos = false;
                                        findSMSAddresses(f1.tos.value);">
                                      <?php echo $_grupos['Numeros'] ?>
                        </textarea><br>

                        <div id="divcorregir" style="display:none">
                            <input class=botongrisgrande type=button value="Corregir y limpiar" onclick="
                                            fwa.borrarerrores.value = '1';
                                            corregirnumeros();">
                        </div>
                    </div>
                    <script>
                        var num = document.getElementById('tos').value;
                        document.getElementById('tos').value = num.replace(/ /g, "");
                        document.getElementById('PaisId').value = '<?php echo $_grupos['PaisId'] ?>';
                    </script>
                    <br>
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizargrupos" id="Actualizargrupos" 
                            value="Actualizargrupos">Actualizar
                    </button>
                </form>
            </section>
            <?php
        }
    } else {
        ?>
        <div style="height: 350px"></div>
        <?php
    }