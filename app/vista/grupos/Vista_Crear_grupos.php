<div style="width: 95%; margin: 0 auto;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Grupos de envios</h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="/grupos/">Grupos</a></li>
            <li class="active">Nuevo</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <form name="f1" id="f1" action="nuevo.php" style='margin:0' method=post>
            <input type="hidden" id="IdUsuario" name="IdUsuario" value="<?php echo $_SESSION['UsuarioId'];?>">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-1"><strong>Nombre: </strong></div>
                <div class="col-xs-6 col-md-4">
                    <input required type="text" id="GrupoNombre" name="GrupoNombre" 
                           maxlength="45" class="form-control" >
                </div>
                <br>
                <br>
                <br>
                <div class="col-xs-12 col-sm-6 col-md-1"><strong>Pais: </strong></div>
                <div class="col-xs-6 col-md-4">
                    <select class="form-control" id="PaisId" name="PaisId" onclick="establecerPais()">
                        <option value="0">Selecciona un pais</option>
                        <?php
                        if ($this->getPaises()) {
                            foreach ($this->getPaises() as $paises) {
                                ?>
                                <option value="<?php echo $paises['PaisId']; ?>">
                                    <?php echo utf8_encode($paises['PaisNombre'] . " (" . $paises['PaisClaveLada'] . ")"); ?>
                                </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <br>
            <br/>
            <div id=sectornumeros>
                <div style="display:inline;float:right">
                    <a href="javascript://" onclick="document.f1.tos.value = ''">Borrar listado</a> |
                    <a href="javascript://" onclick="f1.tos.value = findSMSAddresses(f1.tos.value);">Quitar renglones duplicados</a> |
                    <a href="javascript://" onclick="f1.tos.value = findSMSAddresses(f1.tos.value);">Analizar listado y preparar para personalización</a> 

                </div>
                <div style="display:inline;">
                    <strong>Ingrese los números uno debajo del otro</strong>
                </div>

                <textarea rows=4 style='width:100%;resize:none;' id=tos name=tos class="form-control" onchange="g_numeros_corregidos = false;
                        findSMSAddresses(f1.tos.value);"></textarea><br>

                <div id="divcorregir" style="display:none">
                    <input class=botongrisgrande type=button value="Corregir y limpiar" onclick="
                            fwa.borrarerrores.value = '1';
                            corregirnumeros();">
                </div>
            </div>
            <button style="float: right; width: 150px" class="btn btn-success" 
                    type="button" value="Creargrupos" onclick="crearGrupo()">Grabar</button>
        </form>
    </section>
</div>