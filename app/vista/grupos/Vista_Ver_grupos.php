<?php
$grupos = $this->getgrupos();
if ($grupos) {
    foreach ($grupos as $_grupos) {
        ?>
        <div style="width: 95%; margin: 0 auto;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Grupo - <?php echo $_grupos['GrupoNombre'] ?></h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i>Inicio</a></li>
                    <li><a href="/grupos/">Grupos</a></li>
                    <li class="active">Nuevo</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <input type="hidden" id="IdUsuario" name="IdUsuario" value="<?php echo $_SESSION['UsuarioId']; ?>">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-1"><strong>Nombre: </strong></div>
                    <div class="col-xs-6 col-md-4">
                        <input required type="text" id="GrupoNombre" name="GrupoNombre"  disabled value="<?php echo $_grupos['GrupoNombre'] ?>"
                               maxlength="45" class="form-control" >
                    </div>
                    <?php if ($_SESSION['RolId'] == 1) {
                        ?>
                    <br>
                        <br>
                        <br>
                        <div class="col-xs-12 col-sm-6 col-md-1"><strong>Creador: </strong></div>
                        <div class="col-xs-6 col-md-4">
                            <input type="text" disabled value="<?php echo $_grupos['UsuarioNombre'] ?>"
                                   maxlength="45" class="form-control" >
                        </div>
                        <?php
                    }
                    ?>
                    <br>
                    <br>
                    <br>
                    <div class="col-xs-12 col-sm-6 col-md-1"><strong>Pais: </strong></div>
                    <div class="col-xs-6 col-md-4">
                        <select disabled="true" class="form-control" id="PaisId" name="PaisId" onclick="establecerPais()">
                            <option value="<?php echo $_grupos['PaisId'] ?>"><?php echo $_grupos['PaisNombre'] ?></option>
                        </select>
                    </div>
                </div>
                <br/>
                <div style="display:inline;">
                    <strong>Numeros</strong>
                </div>

                <textarea rows=4 class="form-control" disabled="true"><?php echo $_grupos['Numeros'] ?></textarea><br>
            </section>
            <?php
        }
    } else {
        ?>
        <div style="height: 350px"></div>
        <?php
    }