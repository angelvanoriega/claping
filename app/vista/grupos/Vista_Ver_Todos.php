<script>
    $(document).ready(function () {
        $('#TableVerTodosGrupos').DataTable();
    });
</script>
<div style="height: 50px"></div>
<div style="margin: 0 auto; width: 95%;">
    <table border="2" style="background-color: #40C3D3" id="TableVerTodosGrupos">
        <thead>
            <tr>
                <th><strong>Nombre</strong></th>
                <?php if ($_SESSION['RolId'] == 1) {
                    ?>
                    <th><strong>Creador</strong></th>
                    <?php
                }
                ?>
                <th style="width:1%"><strong>Editar</strong></th>
                <th style="width:1%"><strong>Borrar</strong></th>
            </tr>
        </thead>
        <?php
        if ($this->getTodos()) {
            foreach ($this->getTodos() as $todos) {
                ?>
                <tr>
                    <td>
                        <form action='/grupos/ver.php' method='post'>
                            <input type='hidden' name='GrupoId' id='GrupoId' value='<?php echo $todos["GrupoId"] ?>'/>
                            <input type="hidden" id="UsuarioId" name="UsuarioId" value="<?php echo $todos["UsuarioId"] ?>">
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo $todos["GrupoNombre"] ?>
                            </button>
                        </form>
                    </td>
                    <?php if ($_SESSION['RolId'] == 1) {
                        ?>
                        <td>
                            <form action='/grupos/ver.php' method='post'>
                                <input type='hidden' name='GrupoId' id='GrupoId' value='<?php echo $todos["GrupoId"] ?>'/>
                                <input type="hidden" id="UsuarioId" name="UsuarioId" value="<?php echo $todos["UsuarioId"] ?>">
                                <button style="background: transparent; width: 100%; height: 100%" 
                                        class="btn" type='submit'>
                                            <?php echo $todos["UsuarioNombre"] ?>
                                </button>
                            </form>
                        </td>
                        <?php
                    }
                    ?>

                    <td>
                        <form action='/grupos/actualizar.php' method='post'>
                            <input type='hidden' name='GrupoId' id='GrupoId' value='<?php echo $todos["GrupoId"] ?>'/>
                            <input type="hidden" id="UsuarioId" name="UsuarioId" value="<?php echo $todos["UsuarioId"] ?>">
                            <button style="background: transparent" 
                                    class="btn" type='submit'>
                                <i class='fa fa-pencil'>
                                </i>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form id="borrargrupos" action='/grupos/borrar.php' method='post'>
                            <input type='hidden' name='GrupoId' id='GrupoId' value='<?php echo $todos["GrupoId"] ?>'/>
                            <input type="hidden" id="UsuarioId" name="UsuarioId" value="<?php echo $todos["UsuarioId"] ?>">
                            <button style="background: transparent" class="btn" type='submit'>
                                <i class='fa fa-times'>
                                </i>
                            </button>
                        </form>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</div>