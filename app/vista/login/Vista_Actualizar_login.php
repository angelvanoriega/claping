<?php
if ($this->getlogin()) {
    foreach ($this->getlogin() as $_login) {
        ?>
        <form role="form" method="post" action="/login/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-files-o"></i> login</h3>    
                    </div>
                </div><!-- /.box-header -->
                <hr style="width: 98%;">
                <div class="box-body">
                    <table style="margin: 0 auto; width: 60%">
<tr>
                            <td><strong>LoginId: </strong></td>
                            <td><input type="text" required class="input-sm" id="LoginId" name="LoginId" value="<?php echo $_login['LoginId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Usuario: </strong></td>
                            <td><input type="text" required class="input-sm" id="Usuario" name="Usuario" value="<?php echo $_login['Usuario'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Contrasenia: </strong></td>
                            <td><input type="text" required class="input-sm" id="Contrasenia" name="Contrasenia" value="<?php echo $_login['Contrasenia'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>FechaEntrada: </strong></td>
                            <td><input type="text" required class="input-sm" id="FechaEntrada" name="FechaEntrada" value="<?php echo $_login['FechaEntrada'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
                    </table>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizarlogin" id="Actualizarlogin" 
                            value="Actualizarlogin">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div style="height: 120px"></div>
        <?php
    }
}