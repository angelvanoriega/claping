<div style="height: 10%"></div>
<form method="post" action="/login/registrar-usuario.php">
    <div class="box box-primary" 
         style="margin: 0 auto; width: 70%;">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-user"></i> Crear una cuenta</h3>  
        </div>
        <div class="box-body clearfix" style="margin: 0 auto; width: 100%;">

            <div style="margin: 0 auto;">
                <div class="col-md-12">
                    <div class="col-md-6"><strong>Nombre:</strong>
                        <label id="validacionNombre" name="validacionNombre"></label></div>
                    <div class="col-md-6"><strong>Apellidos:</strong>
                        <label id="validacionApellido" name="validacionApellido"></label></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <input required type="text" id="UsuarioNombre" name="UsuarioNombre" class="form-control"
                               onload="validarNombreUsuario()" onclick="validarNombreUsuario()" 
                               onKeyDown="validarNombreUsuario()" onKeyUp="validarNombreUsuario()">
                    </div>
                    <div class="col-md-6">
                        <input required type="text" id="UsuarioApellido" name="UsuarioApellido" class="form-control"
                               onload="validarApellidoUsuario()" onclick="validarApellidoUsuario()" 
                               onKeyDown="validarApellidoUsuario()" onKeyUp="validarApellidoUsuario()">
                    </div>
                </div>
                <div style="height: 10%"></div>
                <div class="col-md-12">
                    <div class="col-md-3"><strong>Nombre de usuario:</strong>
                        <label id="validacionSobreNombre" name="validacionSobreNombre"></label></div>
                    <div class="col-md-9">
                        <input required type="text" id="UsuarioSobreNombre" name="UsuarioSobreNombre" class="form-control"
                               onload="validarSobreNombre()" onclick="validarSobreNombre()" 
                               onKeyDown="validarSobreNombre()" onKeyUp="validarSobreNombre()">
                    </div>
                </div>
                <div style="height: 7%"></div>
                <div class="col-md-12">
                    <div class="col-md-3"><strong>Correo electronico:</strong>
                        <label id="validacionEmail" name="validacionEmail"></label></div>
                    <div class="col-md-9">
                        <input required type="text" id="UsuarioEmail" name="UsuarioEmail" class="form-control"
                               onload="validarEmail()" onclick="validarEmail()" 
                               onKeyDown="validarEmail()" onKeyUp="validarEmail()">
                    </div>
                </div>  
                <div style="height: 7%"></div>
                <div class="col-md-12">
                    <div class="col-md-3"><strong>Confirmar correo electronico:</strong>
                        <label id="validacionEmailConfirmacion" name="validacionEmailConfirmacion"></label></div>
                    <div class="col-md-9">
                        <input required type="text" id="UsuarioEmailConfirmacion" 
                               name="UsuarioEmailConfirmacion" class="form-control"
                               onload="validarEmail()" onclick="validarEmail()" 
                               onKeyDown="validarEmail()" onKeyUp="validarEmail()">
                    </div>
                </div>  
                <div style="height: 7%"></div>
                <div class="col-md-12">
                    <div class="col-md-6"><strong>Contraseña:</strong>
                        <label id="validacionContrasenia" name="validacionContrasenia"></label></div>
                    <div class="col-md-6"><strong>Confirmar contraseña:</strong>
                        <label id="validacionContraseniaConfirmacion" name="validacionContraseniaConfirmacion"></label></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <input required type="password" id="UsuarioContrasenia" name="UsuarioContrasenia" class="form-control"
                               onload="validarContrasenia()" onclick="validarContrasenia()" 
                               onKeyDown="validarContrasenia()" onKeyUp="validarContrasenia()">
                    </div>
                    <div class="col-md-6">
                        <input required type="password" id="UsuarioContraseniaConfirmacion" 
                               name="UsuarioContraseniaConfirmacion" class="form-control" 
                               onload="validarContrasenia()" onclick="validarContrasenia()" 
                               onKeyDown="validarContrasenia()" onKeyUp="validarContrasenia()">
                    </div>
                </div>
                <div style="height: 10%"></div>
                <div class="col-md-12">
                    <div class="col-md-3"><strong>Pais:</strong></div>
                    <div class="col-md-9">
                        <select required class="form-control" name="PaisId" id="PaisId">
                            <option value="0">Selecciona un pais</option>
                            <?php
                            if ($this->getPaises()) {
                                foreach ($this->getPaises() as $paises) {
                                    ?>
                                    <option value="<?php echo $paises['PaisId']; ?>">
                                        <?php echo utf8_encode($paises['PaisNombre'] . " (" . $paises['PaisClaveLada'] . ")"); ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div style="height: 7%"></div>
                <div class="col-md-12">
                    <div class="col-md-3"><strong>Celular:</strong>
                        <label id="validacionCelular" name="validacionCelular"></label></div>
                    <div class="col-md-9">
                        <input required type="text" id="UsuarioNumeroCelular" name="UsuarioNumeroCelular" class="form-control"
                               onload="validarCelular()" onclick="validarCelular()" 
                               onKeyDown="validarCelular()" onKeyUp="validarCelular()">
                    </div>
                </div>
                <div style="height: 7%"></div>
                <div class="col-md-12" onload="validarSexo()" onclick="validarSexo()" 
                     onKeyDown="validarSexo()" onKeyUp="validarSexo()">
                    <div class="col-md-3"><strong>Sexo:</strong>
                        <label id="validacionSexo" name="validacionSexo"></label></div>
                    <div class="col-md-4">
                        <input type="radio" id="UsuarioSexoHombre" name="UsuarioSexo" value="H"> Hombre
                    </div>
                    <div class="col-md-4">
                        <input type="radio" id="UsuarioSexoMujer" name="UsuarioSexo" value="M"> Mujer
                    </div>
                </div>
                <div style="height: 7%"></div>
                <div class="col-md-12">
                    <div class="col-md-3"><strong>Fecha de nacimiento:</strong>
                        <label id="validacionFechaNacimiento" name="validacionFechaNacimiento"></label></div>
                    <div class="col-md-3">
                        <select class="form-control" name="Dia" id="Dia"
                                onload="validarFechaNacimiento()" onclick="validarFechaNacimiento()" 
                                onKeyDown="validarFechaNacimiento()" onKeyUp="validarFechaNacimiento()">
                            <option value="0">Dia</option>
                            <?php
                            for ($dia = 1; $dia <= 31; $dia++) {
                                if ($dia < 10) {
                                    $dia = '0' . $dia;
                                }
                                ?>
                                <option value="<?php echo $dia; ?>"><?php echo $dia; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" name="Mes" id="Mes" 
                                onload="validarFechaNacimiento()" onclick="validarFechaNacimiento()" 
                                onKeyDown="validarFechaNacimiento()" onKeyUp="validarFechaNacimiento()">
                            <option value="0">Mes</option>
                            <option value="01">Enero</option>
                            <option value="02">Febrero</option>
                            <option value="03">Marzo</option>
                            <option value="04">Abril</option>
                            <option value="05">Mayo</option>
                            <option value="06">Junio</option>
                            <option value="07">Julio</option>
                            <option value="08">Agosto</option>
                            <option value="09">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" name="Anio" id="Anio"
                                onload="validarFechaNacimiento()" onclick="validarFechaNacimiento()" 
                                onKeyDown="validarFechaNacimiento()" onKeyUp="validarFechaNacimiento()">
                            <option value="0">Año</option>
                            <?php
                            $AnioLimite = date('Y') - 10;
                            for ($anio = 1950; $anio <= $AnioLimite; $anio++) {
                                ?>
                                <option value="<?php echo $anio; ?>"><?php echo $anio; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div style="height: 7%"></div>
            <button style="float: right; width: 150px" class="btn btn-primary" 
                    type="button" id="Crearusuarios" name="Crearusuarios" 
                    value="Crearusuarios" onclick="validarRegistroUsuario()">Registrarme</button>
        </div>
    </div>
</form>