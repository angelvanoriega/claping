<div style="height: 30%"></div>
<form method="post" action="/login/recuperar-contrasenia.php">
    <div class="box box-success" 
         style="margin: 0 auto; width: 40%;">
        <div class="box-header with-border"><h4><strong>Recuperar contraseña</strong></h4></div>
        <div class="box-body clearfix">
            <table style="width: 80%; margin: 0 auto;">
                <tr>
                    <td>Introduce tu correo electronico: </td>
                    <td><input type="text" required="true" id="correoe" name="correoe"></td>
                </tr>
                <tr style="height: 20px"></tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Recuperar"></td>
                </tr>
            </table>
        </div>
    </div>
</form>