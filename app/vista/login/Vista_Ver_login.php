<?php
$login = $this->getlogin();
if ($login) {
    foreach ($login as $_login) {
        ?>
        <div class="box box-success" style="width: 70%; margin: 0 auto">
            <div class="box-header">
                <div class="col-xs-10">
                    <h3 class="box-title"><i class="fa fa-files-o"></i> login</h3>    
                </div>
            </div><!-- /.box-header -->
            <hr style="width: 98%;">
            <div class="box-body">
                <form role="form">
                    <table style="margin: 0 auto; width: 95%">
<tr>
                            <td><strong>LoginId: </strong></td>
                            <td><?php echo $_login['LoginId'] ?></td>
                        </tr><tr>
                            <td><strong>Usuario: </strong></td>
                            <td><?php echo $_login['Usuario'] ?></td>
                        </tr><tr>
                            <td><strong>Contrasenia: </strong></td>
                            <td><?php echo $_login['Contrasenia'] ?></td>
                        </tr><tr>
                            <td><strong>FechaEntrada: </strong></td>
                            <td><?php echo $_login['FechaEntrada'] ?></td>
                        </tr>                        <tr style="height: 15px"></tr>
                    </table>
                </form>
            </div>
        </div>
        <div style="height: 120px"></div>
        <?php
    }
} else {
    ?>
    <div style="height: 350px"></div>
    <?php
}