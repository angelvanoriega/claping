<div class="login-box" style="border-width: 100px; color: black">
    <div class="login-box-body">
        <div class="login-logo">
            <a style="color: black"><b>Clap</b>ing</a>
        </div>
        <form action="/login/login.php" method="post">
            <div class="form-group has-feedback">
                <input required type="text" id="Usuario" name="Usuario" class="form-control" placeholder="Usuario">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input required type="password" id="Contrasenia" name="Contrasenia" 
                       class="form-control" placeholder="Contraseña">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <button type="submit" id="login" name="login" value="login" 
                            class="btn btn-primary btn-block btn-flat">Ingresar</button>
                </div><!-- /.col -->
            </div>
        </form>
        <a href="/login/recuperar-contrasenia.php">Olvide mi contraseña</a><br>
        <a href="/login/registrar-usuario.php" class="text-center">Registrar una nueva cuenta</a>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
