<?php
$mensajes_recibidos = $this->getmensajes_recibidos();
$cantidad_mensajes = count($mensajes_recibidos) - 2;
$gran_array = array();
for ($i = 0; $i <= $cantidad_mensajes; $i++) {
    $columnas = preg_split('/\t+/', $mensajes_recibidos[$i]);
    $array = array(
        "NUMERO" => $columnas[0],
        "TEXTO" => $columnas[1],
        "FECHA" => $columnas[2],
        "ID_WHAPPEND" => $columnas[3]
    );
    array_push($gran_array, $array);
}

function date_compare($a, $b) {
    $t1 = strtotime($a['FECHA']);
    $t2 = strtotime($b['FECHA']);
    return $t1 - $t2;
}

usort($gran_array, 'date_compare');
if ($mensajes_recibidos) {
    ?>
    <div style="height: 50px; width: 95%;margin: 0 auto;">
    <?php $array_numero = $gran_array[0]; ?>
        <h4>
            <i class="fa fa-files-o"></i> Respuesta de 
            <strong><?php echo $array_numero["NUMERO"] ?>
            </strong>
        </h4> 
        <table style="margin: auto 0" border="1">
            <tr style="background-color: #40C3D3">
                <td style="width: 60%;"><strong>Mensaje </strong></td>
                <td style="width: 30%;"><strong>Fecha </strong></td>
            </tr>
            <?php
            foreach ($gran_array as $todos) {
                ?>
                <tr>
                    <td><?php echo $todos["TEXTO"] ?></td>
                    <td><?php echo $todos["FECHA"] ?></td>
                </tr>
            <?php }
            ?>
        </table>
        <div style="height: 80px"></div>
    </div>
    <?php
} else {
    ?>
    <div style="height: 350px"></div>
    <?php
}