<?php
$mensajes_recibidos = $this->getTodos();
$cantidad_mensajes = count($mensajes_recibidos) - 2;
$gran_array = array();
for ($i = 0; $i <= $cantidad_mensajes; $i++) {
    $columnas = preg_split('/\t+/', $mensajes_recibidos[$i]);
    if (intval($columnas[4]) == $_SESSION['UsuarioId']) {
        $array = array(
            "NUMERO" => $columnas[0],
            "TEXTO" => $columnas[1],
            "FECHA" => $columnas[2],
            "ID_WHAPPEND" => $columnas[3],
            "ID_WHAPP" => $columnas[4]
        );
        array_push($gran_array, $array);
    }
}
?>
<div style="margin: 0 auto; width: 95%;">
    <div style="height: 40px"></div>
    <table border="1" id="VerTodosRecibidos" >
        <thead style="background-color: #40C3D3">
            <tr>
                <th><strong>Numero</strong></th>
                <th><strong>Mensaje</strong></th>
                <th><strong>Fecha</strong></th>
            </tr>
        </thead>
        <?php
        if ($gran_array > 0) {
            foreach ($gran_array as $todos) {
                ?>
                <tr>

                    <td>
                        <form action='/mensajes_recibidos/ver.php' method='post'>
                            <input type='hidden' name='Mensaje_RecibidoId' id='Mensaje_RecibidoId' value='<?php echo $todos["NUMERO"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo $todos["NUMERO"] ?>
                            </button>
                        </form>
                    </td>

                    <td>
                        <form action='/mensajes_recibidos/ver.php' method='post'>
                            <input type='hidden' name='Mensaje_RecibidoId' id='Mensaje_RecibidoId' value='<?php echo $todos["NUMERO"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                        <?php echo substr($todos["TEXTO"], 0, 50) ?>
                            </button>
                        </form>
                    </td>

                    <td>
                        <form action='/mensajes_recibidos/ver.php' method='post'>
                            <input type='hidden' name='Mensaje_RecibidoId' id='Mensaje_RecibidoId' value='<?php echo $todos["NUMERO"] ?>'/>
                            <button style="background: transparent; width: 100%; height: 100%" 
                                    class="btn" type='submit'>
                                <?php echo $todos["FECHA"] ?>
                            </button>
                        </form>
                    </td>

                </tr>
                <?php
            }
        }
        ?>
    </table></div>
<div style="height: 40px"></div>