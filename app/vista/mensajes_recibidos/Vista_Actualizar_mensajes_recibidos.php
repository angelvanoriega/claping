<?php
if ($this->getmensajes_recibidos()) {
    foreach ($this->getmensajes_recibidos() as $_mensajes_recibidos) {
        ?>
        <form role="form" method="post" action="/mensajes_recibidos/actualizar.php">
            <div class="box box-success" style="width: 70%; margin: 0 auto">
                <div class="box-header">
                    <div class="col-xs-10">
                        <h3 class="box-title"><i class="fa fa-files-o"></i> mensajes_recibidos</h3>    
                    </div>
                </div><!-- /.box-header -->
                <hr style="width: 98%;">
                <div class="box-body">
                    <table style="margin: 0 auto; width: 60%">
<tr>
                            <td><strong>Mensaje_RecibidoId: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_RecibidoId" name="Mensaje_RecibidoId" value="<?php echo $_mensajes_recibidos['Mensaje_RecibidoId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>UsuarioId: </strong></td>
                            <td><input type="text" required class="input-sm" id="UsuarioId" name="UsuarioId" value="<?php echo $_mensajes_recibidos['UsuarioId'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_RecibidoRemitente: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_RecibidoRemitente" name="Mensaje_RecibidoRemitente" value="<?php echo $_mensajes_recibidos['Mensaje_RecibidoRemitente'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_RecibidoTexto: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_RecibidoTexto" name="Mensaje_RecibidoTexto" value="<?php echo $_mensajes_recibidos['Mensaje_RecibidoTexto'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
<tr>
                            <td><strong>Mensaje_RecibidoFecha: </strong></td>
                            <td><input type="text" required class="input-sm" id="Mensaje_RecibidoFecha" name="Mensaje_RecibidoFecha" value="<?php echo $_mensajes_recibidos['Mensaje_RecibidoFecha'] ?>"/></td>
                        </tr>
<tr style="height: 15px"></tr>
                    </table>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" style="float: right" 
                            name="Actualizarmensajes_recibidos" id="Actualizarmensajes_recibidos" 
                            value="Actualizarmensajes_recibidos">Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div style="height: 120px"></div>
        <?php
    }
}