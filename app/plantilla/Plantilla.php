<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace plantilla;

/**
 * Description of Plantilla
 *
 * @author practica3.auditoria
 */
class Plantilla {

    public function __construct() {
        header('Content-Type: text/html; charset=UTF-8');
    }

    public function cargarPagina($titulo, $Vista) {
        $this->obtenerCabeceraHTML($titulo);
        require_once $Vista;
        $this->obtenerPieHTML();
    }

    public function alertError($mensaje) {
        ?>
        <link rel="stylesheet" type="text/css" href="/assets/javascript/sweetalert-master/sweetalert.css" media="all">
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert-dev.js"></script>
        <br><script type='text/javascript'>
            swal({
                title: "ERROR!!!",
                text: "<?php echo $mensaje ?>",
                type: "error"},
            function () {
                javascript:history.back();
            });
        </script><?php
    }

    public function alertSuccess($mensaje) {
        ?>
        <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/javascript/sweetalert-master/sweetalert.css" media="all">
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert-dev.js"></script>
        <br><script type='text/javascript'>
            swal({
                title: "Enhorabuena!!!",
                text: "<?php echo $mensaje ?>",
                type: "success"},
            function () {
                javascript:history.back();
            });
        </script><?php
    }

    public function agregarCSS() {
        ?>
        <link rel="stylesheet" href="/assets/jquery-ui-1.11.4.custom/jquery-ui.css">
        <link rel="stylesheet" href="/assets/plugins/timepicker/jquery.timepicker.css">
        <link rel="stylesheet" href="/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/assets/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="/assets/estilos/css/estilo.css">
        <link rel="stylesheet" href="/assets/estilos/css/ionicons.min.css">
        <link rel="stylesheet" href="/assets/estilos/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/assets/javascript/sweetalert-master/sweetalert.css" media="all">
        <link rel="stylesheet" href="/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="/assets/plugins/iCheck/flat/blue.css">
        <link rel="stylesheet" href="/assets/plugins/morris/morris.css">
        <link rel="stylesheet" href="/assets/plugins/datatables/datatables.min.css">
        <?php
    }

    public function agregarJS() {
        ?>
        <script type="text/javascript" src="/assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/jQuery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
        <script type="text/javascript" src="/assets/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
        <script type="text/javascript" src="/assets/plugins/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/timepicker/jquery.timepicker.js"></script>
        <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/dist/js/app.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/funciones-js.js"></script>
        <script type="text/javascript" src="/assets/javascript/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert-dev.js"></script>
        <script>$.widget.bridge('uibutton', $.ui.button);</script>
        <?php
    }

    public function header() {
        ?>
        <table style="width: 100%">
            <tr>
                <td>
                    <header class="main-header" style="width: 100%; position: fixed">
                        <!-- Logo -->
                        <a href="/" class="logo">
                            <!-- mini logo for sidebar mini 50x50 pixels -->
                            <span class="logo-mini"><img src="/assets/dist/img/Clap.png"></span>
                            <!-- logo for regular state and mobile devices -->
                            <span class="logo-lg"><img src="/assets/dist/img/Claping-Logo-180x60-web.png"></span>
                        </a>
                        <!-- Header Navbar: style can be found in header.less -->
                        <nav class="navbar navbar-static-top" role="navigation">
                            <!-- Sidebar toggle button-->
                            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                                <span class="sr-only">Toggle navigation</span>
                            </a>
                            <div class="navbar-custom-menu">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown user user-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="hidden-xs">Usuario conectado: <?php echo $_SESSION['UsuarioNombre'] ?></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <!-- User image -->
                                            <li class="user-header">
                                                <p>
                                                    Le quedan 10 creditos
                                                    <small>Hasta el 16 de octubre 2015</small>
                                                </p>
                                            </li>
                                            <!-- Menu Body -->

                                            <!-- Menu Footer-->
                                            <li class="user-footer">
                                                <div class="pull-left">
                                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                                </div>
                                                <div class="pull-right">
                                                    <a href="/login/out.php" class="btn btn-default btn-flat">Sign out</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- Control Sidebar Toggle Button -->
                                    <li>
                                        <a href="/login/out.php" ><i class="fa fa-power-off"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>    
                </td>
            </tr>
        </table>
        <?php
    }

    public function headHTML($titulo) {
        ?>
        <!DOCTYPE html5>
        <?xml version="1.0"?>
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title><?php echo $titulo; ?></title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <!--<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">-->
                <?php
                $this->agregarCSS();
                $this->agregarJS();
                ?>
            </head><?php
        }

        public function barraLateral() {
            ?>
            <table style="width: 100%">
                <tr>
                    <td>
                        <aside class="main-sidebar" style="position: fixed">
                            <!-- sidebar: style can be found in sidebar.less -->
                            <section class="sidebar">
                                <!-- Sidebar user panel -->

                                <!-- sidebar menu: : style can be found in sidebar.less -->
                                <ul class="sidebar-menu">
                                    <li>
                                        <a href="/">
                                            <i class="fa fa-tachometer"></i> <span>Dashboard</span>
                                        </a>
                                    </li>
                                    <li class="treeview">
                                        <a href="/">
                                            <i class="fa fa-paper-plane-o"></i>
                                            <span>Mensajes</span> 
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="active"><a href="/mensajes_enviados/nuevo.php"><i class="fa fa-circle-o"></i> Texto</a></li>
                                            <li class="active"><a href="/mensajes_enviados/mensaje_imagen.php"><i class="fa fa-circle-o"></i> Imagen</a></li>
                                            <li class="active"><a href="/mensajes_enviados/mensaje_video.php"><i class="fa fa-circle-o"></i> Video</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="/mensajes_enviados/">
                                            <i class="fa fa-exchange"></i> <span>Bandeja de salida</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/mensajes_recibidos/">
                                            <i class="fa fa-inbox"></i> <span>Bandeja de entrada</span>
                                        </a>
                                    </li>
                                    <li class="treeview">
                                        <a href="/">
                                            <i class="fa fa-user"></i>
                                            <span>Usuario</span> 
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </a>
                                        <ul class="treeview-menu">
                                            <?php
                                            if ($_SESSION['RolId'] == 1) {
                                                ?>
                                                <li class="active">
                                                    <a href="javascript:;document.getElementById('AdministrarUsuarios').submit();">
                                                        <i class="fa fa-pencil"></i> Administrar usuarios</a>

                                                </li>
                                            <?php } ?>
                                            <li class="active">
                                                <a href="javascript:;document.getElementById('UsuarioActualizar').submit();">
                                                    <i class="fa fa-pencil"></i> Modificar mis datos</a>

                                            </li>
                                            <li class="active">
                                                <a href="/usuarios/cambiar-clave.php">
                                                    <i class="fa fa-magic"></i> <span>Cambiar clave</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="treeview">
                                        <a href="/">
                                            <i class="fa fa-users"></i>
                                            <span>Mis grupos</span> 
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="active"><a href="/grupos/"><i class="fa fa-cogs"></i> Administrar</a></li>
                                            <li class="active"><a href="/grupos/nuevo.php"><i class="fa fa-plus"></i> Agregar</a></li>
                                        </ul>
                                    </li>
                                    <!--<li>
                                        <a href="/creditos/">
                                            <i class="fa fa-money"></i> <span>Mis creditos</span>
                                        </a>
                                    </li>-->
                                    <?php
                                    if ($_SESSION['RolId'] == 1) {
                                        ?>
                                        <li class="treeview">
                                            <a href="/">
                                                <i class="fa fa-flag-o"></i> 
                                                <span>Administrar paises</span>
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li class="active"><a href="/paises/"><i class="fa fa-cogs"></i> Administrar</a></li>
                                                <li class="active"><a href="/paises/nuevo.php"><i class="fa fa-plus"></i> Agregar</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="/">
                                                <i class="fa fa-lock"></i> 
                                                <span>Administrar roles</span>
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li class="active"><a href="/roles/"><i class="fa fa-cogs"></i> Administrar</a></li>
                                                <li class="active"><a href="/roles/nuevo.php"><i class="fa fa-plus"></i> Agregar</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="/">
                                                <i class="fa fa-download"></i> 
                                                <span>Ver logins</span>
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li class="active"><a href="/login/"><i class="fa fa-cogs"></i> Administrar</a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <!--<li>
                                        <a href="historial.html">
                                            <i class="fa fa-history"></i> <span>Historial</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="comprar-creditos.html">
                                            <i class="fa fa-credit-card"></i> <span>Comprar creditos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-exclamation-circle"></i> <span>Ayuda</span>
                                        </a>
                                    </li>
                                </ul>
                            </section>-->
                                    <!-- /.sidebar -->
                                    </aside>
                                    </td>
                                    </tr>
                                    </table>
                                    <form action='/usuarios/actualizar.php' method='post' id="UsuarioActualizar">
                                        <input type='hidden' name='UsuarioId' id='UsuarioId' 
                                               value='<?php echo $_SESSION['UsuarioId'] ?>'/>
                                    </form>
                                    <form action='/usuarios/ver-todos.php' method='post' id="AdministrarUsuarios">
                                    </form>
                                    <?php
                                }

                                public function obtenerCabeceraHTML($titulo) {
                                    $this->headHTML($titulo);
                                    if (stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'login.php') ||
                                            stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'recuperar-contrasenia') ||
                                            stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'registrar-usuario.php')) {
                                        ?>
                                        <body class="hold-transition skin-blue sidebar-mini" background="/assets/dist/img/13237-NP29WF.jpg" style="background-image: '/dist/img/13237-NP29WF.jpg';">
                                            <?php
                                        } else {
                                            ?>
                                        <body class="hold-transition skin-blue sidebar-mini sidebar-collapse" 
                                              style="background-color: #ECF0F5" onload="loadJS()">
                                                  <?php
                                                  if (stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'login.php') ||
                                                          stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'recuperar-contrasenia') ||
                                                          stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'registrar-usuario.php')) {
                                                      
                                                  } else {
                                                      $this->header();
                                                      ?>
                                                <div class="content-wrapper">
                                                    <table class="Pagina" style="width: 100%;" ><tr>
                                                            <td >
                                                                <?php
                                                                if (stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'login.php') ||
                                                                        stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'recuperar-contrasenia') ||
                                                                        stripos(filter_input(INPUT_SERVER, 'PHP_SELF'), 'registrar-usuario.php')) {
                                                                    
                                                                } else {
                                                                    $this->barraLateral();
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <div style="height: 50px"></div>
                                                                <?php
                                                            }
                                                        }
                                                    }

                                                    public function obtenerPieHTML() {
                                                        ?>
                                                    </td>
                                                </tr></table>
                                        </div>
                                    </body>
                                    </html>
                                    <?php
                                }

                            }
                            