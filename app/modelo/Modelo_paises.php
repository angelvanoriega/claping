<?php

namespace modelo;

require_once("Modelo.php");

class Modelo_paises extends \modelo\Modelo {

    private $ConexionBD;

    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }

    public function irpaisesPagina($IDS, $Pagina) {
        ?><form method="post" action="/paises/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
            <input type='hidden' name='PaisId' id='PaisId' value='<?php echo $IDS["PaisId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

    public function actualizar($parametros) {
        $Actualizarpaises = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizarpaises == false) {
            return false;
        } else {
            $IDS = array('PaisId' => $parametros['PaisId']);
            $this->irpaisesPagina($IDS, 'ver');
        }
    }

    public function queryActualizar($parametros) {
        $QueryActualizarpaises = "UPDATE paises SET PaisId = '$parametros[PaisId]',
PaisNombre = '$parametros[PaisNombre]',
PaisClaveLada = '$parametros[PaisClaveLada]'
 WHERE PaisId = '$parametros[PaisId]';";
        return $QueryActualizarpaises;
    }

    public function borrar($parametros) {
        $Borrarpaises = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarpaises == FALSE || $Borrarpaises == TRUE) {
            header('Location: /paises/ver-todos.php');
        }
    }

    public function queryBorrar($parametros) {
        $QueryBorrarpaises = "DELETE FROM paises WHERE PaisId = '$parametros[PaisId]'";
        return $QueryBorrarpaises;
    }

    public function crear($parametros) {
        $Crearpaises = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearpaises == true) {
            header('Location: /paises/ver-todos.php');
        } else {
            return false;
        }
    }

    public function queryCrear($parametros) {
        $QueryCrearpaises = "INSERT INTO paises (PaisId,
PaisNombre,
PaisClaveLada
) VALUES ('$parametros[PaisId]',
'$parametros[PaisNombre]',
'$parametros[PaisClaveLada]'
)";
        return $QueryCrearpaises;
    }

    public function obtenerPorId($parametros) {
        $this->paises = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->paises == false) {
            return false;
        } else {
            return $this->paises;
        }
    }

    public function queryObtenerPorId($parametros) {
        $Querypaises = "SELECT PaisId,
PaisNombre,
PaisClaveLada
 FROM paises WHERE PaisId ='$parametros[PaisId]';";
        return $Querypaises;
    }

    public function obtenerTodos() {
        $this->paisesTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->paisesTodos == TRUE) {
            return $this->paisesTodos;
        } else {
            return false;
        }
    }

    public function queryObtenerTodos() {
        $QuerypaisesTodos = "SELECT PaisId,
PaisNombre,
PaisClaveLada
FROM paises;";
        return $QuerypaisesTodos;
    }

}
