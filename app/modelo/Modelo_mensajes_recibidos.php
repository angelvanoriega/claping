<?php
namespace modelo;

require_once("Modelo.php");

class Modelo_mensajes_recibidos extends \modelo\Modelo {

private $ConexionBD;
    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }
public function irmensajes_recibidosPagina($IDS, $Pagina) {
        ?><form method="post" action="/mensajes_recibidos/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
<input type='hidden' name='Mensaje_RecibidoId' id='Mensaje_RecibidoId' value='<?php echo $IDS["Mensaje_RecibidoId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

public function actualizar($parametros) {
        $Actualizarmensajes_recibidos = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizarmensajes_recibidos == false) {
            return false;
        } else {
            $IDS = array('Mensaje_RecibidoId' => $parametros['Mensaje_RecibidoId']);
            $this->irmensajes_recibidosPagina($IDS, 'ver');
        }
    }
public function queryActualizar($parametros) {
        $QueryActualizarmensajes_recibidos = "UPDATE mensajes_recibidos SET Mensaje_RecibidoId = '$parametros[Mensaje_RecibidoId]',
UsuarioId = '$parametros[UsuarioId]',
Mensaje_RecibidoRemitente = '$parametros[Mensaje_RecibidoRemitente]',
Mensaje_RecibidoTexto = '$parametros[Mensaje_RecibidoTexto]',
Mensaje_RecibidoFecha = '$parametros[Mensaje_RecibidoFecha]'
 WHERE Mensaje_RecibidoId = $parametros[Mensaje_RecibidoId]";
        return $QueryActualizarmensajes_recibidos;
    }

public function borrar($parametros) {
        $Borrarmensajes_recibidos = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarmensajes_recibidos == FALSE || $Borrarmensajes_recibidos == TRUE) {
            header('Location: /mensajes_recibidos/ver-todos.php');
        }
    }

public function queryBorrar($parametros) {
        $QueryBorrarmensajes_recibidos = "DELETE FROM mensajes_recibidos WHERE Mensaje_RecibidoId = $parametros[Mensaje_RecibidoId]";
        return $QueryBorrarmensajes_recibidos;
    }

public function crear($parametros) {
        $Crearmensajes_recibidos = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearmensajes_recibidos == true) {
            header('Location: /mensajes_recibidos/ver-todos.php');
        } else {
            return false;
        }
    }

public function queryCrear($parametros) {
        $QueryCrearmensajes_recibidos = "INSERT INTO mensajes_recibidos (Mensaje_RecibidoId,
UsuarioId,
Mensaje_RecibidoRemitente,
Mensaje_RecibidoTexto,
Mensaje_RecibidoFecha
) VALUES ('$parametros[Mensaje_RecibidoId]',
'$parametros[UsuarioId]',
'$parametros[Mensaje_RecibidoRemitente]',
'$parametros[Mensaje_RecibidoTexto]',
'$parametros[Mensaje_RecibidoFecha]'
)";
        return $QueryCrearmensajes_recibidos;
    }

public function obtenerPorId($parametros) {
        $this->mensajes_recibidos = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->mensajes_recibidos == false) {
            return false;
        } else {
            return $this->mensajes_recibidos;
        }
    }

public function queryObtenerPorId($parametros) {
        $Querymensajes_recibidos = "SELECT Mensaje_RecibidoId,
UsuarioId,
Mensaje_RecibidoRemitente,
Mensaje_RecibidoTexto,
Mensaje_RecibidoFecha
 FROM mensajes_recibidos WHERE Mensaje_RecibidoId = $parametros[Mensaje_RecibidoId];";
        return $Querymensajes_recibidos;
    }

public function obtenerTodos() {
        $this->mensajes_recibidosTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->mensajes_recibidosTodos == TRUE) {
            return $this->mensajes_recibidosTodos;
        } else {
            return false;
        }
    }

public function queryObtenerTodos() {
        $Querymensajes_recibidosTodos = "SELECT Mensaje_RecibidoId,
UsuarioId,
Mensaje_RecibidoRemitente,
Mensaje_RecibidoTexto,
Mensaje_RecibidoFecha
FROM mensajes_recibidos;";
        return $Querymensajes_recibidosTodos;
    }

}