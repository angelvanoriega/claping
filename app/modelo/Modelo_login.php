<?php

namespace modelo;

require_once("Modelo.php");

class Modelo_login extends \modelo\Modelo {

    private $ConexionBD;

    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }

    public function irloginPagina($IDS, $Pagina) {
        ?><form method="post" action="/login/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
            <input type='hidden' name='LoginId' id='LoginId' value='<?php echo $IDS["LoginId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

    public function actualizar($parametros) {
        $Actualizarlogin = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizarlogin == false) {
            return false;
        } else {
            $IDS = array('LoginId' => $parametros['LoginId']);
            $this->irloginPagina($IDS, 'ver');
        }
    }

    public function queryActualizar($parametros) {
        $QueryActualizarlogin = "UPDATE login SET LoginId = '$parametros[LoginId]',
Usuario = '$parametros[Usuario]',
Contrasenia = '$parametros[Contrasenia]',
FechaEntrada = '$parametros[FechaEntrada]'
 WHERE LoginId = $parametros[LoginId]";
        return $QueryActualizarlogin;
    }

    public function borrar($parametros) {
        $Borrarlogin = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarlogin == FALSE || $Borrarlogin == TRUE) {
            header('Location: /login/ver-todos.php');
        }
    }

    public function queryBorrar($parametros) {
        $QueryBorrarlogin = "DELETE FROM login WHERE LoginId = $parametros[LoginId]";
        return $QueryBorrarlogin;
    }

    public function crear($parametros) {
        $Crearlogin = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearlogin == true) {
            header('Location: /index.php');
        } else {
            return false;
        }
    }

    public function queryCrear($parametros) {
        $QueryCrearlogin = "INSERT INTO login (Usuario,
Contrasenia,
FechaEntrada
) VALUES ('$parametros[Usuario]',
'$parametros[Contrasenia]',
'$parametros[FechaEntrada]'
)";
        return $QueryCrearlogin;
    }

    public function obtenerPorId($parametros) {
        $this->login = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->login == false) {
            return false;
        } else {
            return $this->login;
        }
    }

    public function queryObtenerPorId($parametros) {
        $Querylogin = "SELECT LoginId,
Usuario,
Contrasenia,
FechaEntrada
 FROM login WHERE LoginId = $parametros[LoginId];";
        return $Querylogin;
    }

    public function obtenerTodos() {
        $this->loginTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->loginTodos == TRUE) {
            return $this->loginTodos;
        } else {
            return false;
        }
    }

    public function queryObtenerTodos() {
        $QueryloginTodos = "SELECT LoginId,
Usuario,
Contrasenia,
FechaEntrada
FROM login;";
        return $QueryloginTodos;
    }

}
