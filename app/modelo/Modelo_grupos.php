<?php

namespace modelo;

require_once("Modelo.php");

class Modelo_grupos extends \modelo\Modelo {

    private $ConexionBD;

    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }

    public function irgruposPagina($IDS, $Pagina) {
        ?><form method="post" action="/grupos/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
            <input type='hidden' name='GrupoId' id='GrupoId' value='<?php echo $IDS["GrupoId"] ?>'/>
            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $IDS["GrupoCreador_UsuarioId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

    public function actualizar($parametros) {
        $Actualizargrupos = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizargrupos == false) {
            return false;
        } else {
            $this->irgruposPagina($parametros, 'ver');
        }
    }

    public function queryActualizar($parametros) {
        $QueryActualizargrupos = "UPDATE grupos SET GrupoId = '$parametros[GrupoId]',"
                . "GrupoNombre = '$parametros[GrupoNombre]',"
                . "PaisId = '$parametros[PaisId]',"
                . "Numeros = '$parametros[Numeros]',"
                . "GrupoCreador_UsuarioId = '$parametros[GrupoCreador_UsuarioId]' "
                . "WHERE GrupoId = $parametros[GrupoId]";
        return $QueryActualizargrupos;
    }

    public function borrar($parametros) {
        $Borrargrupos = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrargrupos == FALSE || $Borrargrupos == TRUE) {
            header('Location: /grupos/ver-todos.php');
        }
    }

    public function queryBorrar($parametros) {
        $QueryBorrargrupos = "DELETE FROM grupos WHERE GrupoId = $parametros[GrupoId]";
        return $QueryBorrargrupos;
    }

    public function crear($parametros) {
        $query = $this->queryCrear($parametros);
        $Creargrupos = $this->ConexionBD->ejecutarQuery($query);
        if ($Creargrupos == true) {
            return true;
        } else {
            return false;
        }
    }

    public function queryCrear($parametros) {
        $QueryCreargrupos = "INSERT INTO `grupos`(`GrupoNombre`, "
                . "`GrupoCreador_UsuarioId`, `Numeros`, `PaisId`) "
                . "VALUES ('$parametros[GrupoNombre]'," . "'$parametros[IdUsuario]', "
                . "'$parametros[Numeros]','$parametros[PaisId]')";
        return $QueryCreargrupos;
    }

    public function obtenerPorId($parametros) {
        $this->grupos = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        return $this->grupos;
    }

    public function queryObtenerPorId($parametros) {
        $Querygrupos = "SELECT grupos.*, paises.*, usuarios.UsuarioNombre FROM grupos "
                . "inner join paises "
                . "inner join usuarios "
                . "ON grupos.PaisId = paises.PaisId "
                . "AND usuarios.UsuarioId = grupos.GrupoCreador_UsuarioId "
                . "WHERE GrupoId = $parametros[GrupoId] AND usuarios.UsuarioId = $parametros[UsuarioId];";
        return $Querygrupos;
    }

    public function obtenerTodos() {
        $this->gruposTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->gruposTodos == TRUE) {
            return $this->gruposTodos;
        } else {
            return false;
        }
    }

    public function queryObtenerTodos() {
        $IdUsuario = $_SESSION['UsuarioId'];
        if ($_SESSION['RolId'] == 1) {
            $QuerygruposTodos = "SELECT * FROM grupos "
                    . "inner join usuarios ON usuarios.UsuarioId = grupos.GrupoCreador_UsuarioId;";
        } else {
            $QuerygruposTodos = "SELECT * FROM grupos "
                    . "inner join usuarios ON usuarios.UsuarioId = grupos.GrupoCreador_UsuarioId "
                    . "where GrupoCreador_UsuarioId = '$IdUsuario';";
        }
        return $QuerygruposTodos;
    }

}
