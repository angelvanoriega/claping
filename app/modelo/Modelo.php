<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modelo;

require_once("Conexion.php");

/**
 * Description of Modelo
 *
 * @author angel_000
 */
abstract class Modelo {

    public abstract function obtenerTodos();

    public abstract function queryObtenerTodos();

    public abstract function obtenerPorId($id);

    public abstract function queryObtenerPorId($id);

    public abstract function crear($parametros);

    public abstract function queryCrear($parametros);

    public abstract function actualizar($parametros);

    public abstract function queryActualizar($parametros);

    public abstract function borrar($id);

    public abstract function queryBorrar($id);
}
