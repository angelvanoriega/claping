<?php

namespace modelo;

require_once("Modelo.php");

class Modelo_API extends \modelo\Modelo {

    function crearCadena($fields) {
        $fields_string = '';
        $fields_total = count($fields);
        $contador = 0;
        foreach ($fields as $key => $value) {
            $contador++;
            if ($contador == $fields_total) {
                $fields_string .= $key . '=' . $value;
            } else {
                $fields_string .= $key . '=' . $value . '&';
            }
        }
        rtrim($fields_string, '&');
        return $fields_string;
    }

    function consultar_mensajes_recibidos() {
        $url = 'http://private.whappend.com/wa_get_inbox.asp?usuario=WAC31516&clave=946297&TRAERIDINTERNO=1';
        $fields = array(
        );
        $fields_string = $this->crearCadena($fields);
        $result = $this->ejecutarCURL($url, $fields, $fields_string);
        return $result;
    }
    
    function consultar_mensajes_enviados() {
        $url = 'http://private.whappend.com/ver_informe.asp?';
        $fields = array(
            'formato' => 'tabs',
            'anio' => '2016',
            'mes' => '6',
            'usuario' => 'WAC31516',
            'clave' => '946297'
        );
        $fields_string = $this->crearCadena($fields);
        $result = $this->ejecutarCURL($url, $fields, $fields_string);
        return $result;
    }

    function consultar_mensajes_recibidos_id($ORIGEN) {
        $url = 'http://private.whappend.com/wa_get_inbox.asp?';
        $fields = array(
            'usuario' => 'WAC31516',
            'clave' => '946297',
            'ORIGEN' => $ORIGEN
        );
        $fields_string = $this->crearCadena($fields);
        $result = $this->ejecutarCURL($url, $fields, $fields_string);
        return $result;
    }

    function ejecutarCURL($url, $fields, $fields_string) {
        $conexionCURL = curl_init();
        curl_setopt($conexionCURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($conexionCURL, CURLOPT_URL, $url);
        curl_setopt($conexionCURL, CURLOPT_POST, count($fields));
        curl_setopt($conexionCURL, CURLOPT_POSTFIELDS, $fields_string);
        $result = curl_exec($conexionCURL);
        curl_close($conexionCURL);
        return $result;
    }

    public function actualizar($parametros) {
        
    }

    public function borrar($id) {
        
    }

    public function crear($parametros) {
        
    }

    public function obtenerPorId($id) {
        
    }

    public function obtenerTodos() {
        
    }

    public function queryActualizar($parametros) {
        
    }

    public function queryBorrar($id) {
        
    }

    public function queryCrear($parametros) {
        
    }

    public function queryObtenerPorId($id) {
        
    }

    public function queryObtenerTodos() {
        
    }

}
