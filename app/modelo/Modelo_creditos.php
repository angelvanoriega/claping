<?php
namespace modelo;

require_once("Modelo.php");

class Modelo_creditos extends \modelo\Modelo {

private $ConexionBD;
    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }
public function ircreditosPagina($IDS, $Pagina) {
        ?><form method="post" action="/creditos/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
<input type='hidden' name='CreditoId' id='CreditoId' value='<?php echo $IDS["CreditoId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

public function actualizar($parametros) {
        $Actualizarcreditos = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizarcreditos == false) {
            return false;
        } else {
            $IDS = array('CreditoId' => $parametros['CreditoId']);
            $this->ircreditosPagina($IDS, 'ver');
        }
    }
public function queryActualizar($parametros) {
        $QueryActualizarcreditos = "UPDATE creditos SET CreditoId = '$parametros[CreditoId]',
CreditoCantidad = '$parametros[CreditoCantidad]',
UsuarioId = '$parametros[UsuarioId]'
 WHERE CreditoId = $parametros[CreditoId]";
        return $QueryActualizarcreditos;
    }

public function borrar($parametros) {
        $Borrarcreditos = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarcreditos == FALSE || $Borrarcreditos == TRUE) {
            header('Location: /creditos/ver-todos.php');
        }
    }

public function queryBorrar($parametros) {
        $QueryBorrarcreditos = "DELETE FROM creditos WHERE CreditoId = $parametros[CreditoId]";
        return $QueryBorrarcreditos;
    }

public function crear($parametros) {
        $Crearcreditos = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearcreditos == true) {
            header('Location: /creditos/ver-todos.php');
        } else {
            return false;
        }
    }

public function queryCrear($parametros) {
        $QueryCrearcreditos = "INSERT INTO creditos (CreditoId,
CreditoCantidad,
UsuarioId
) VALUES ('$parametros[CreditoId]',
'$parametros[CreditoCantidad]',
'$parametros[UsuarioId]'
)";
        return $QueryCrearcreditos;
    }

public function obtenerPorId($parametros) {
        $this->creditos = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->creditos == false) {
            return false;
        } else {
            return $this->creditos;
        }
    }

public function queryObtenerPorId($parametros) {
        $Querycreditos = "SELECT CreditoId,
CreditoCantidad,
UsuarioId
 FROM creditos WHERE CreditoId = $parametros[CreditoId];";
        return $Querycreditos;
    }

public function obtenerTodos() {
        $this->creditosTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->creditosTodos == TRUE) {
            return $this->creditosTodos;
        } else {
            return false;
        }
    }

public function queryObtenerTodos() {
        $QuerycreditosTodos = "SELECT CreditoId,
CreditoCantidad,
UsuarioId
FROM creditos;";
        return $QuerycreditosTodos;
    }

}