<?php
namespace modelo;

require_once("Modelo.php");

class Modelo_mensajes_enviados extends \modelo\Modelo {

private $ConexionBD;
    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }
public function irmensajes_enviadosPagina($IDS, $Pagina) {
        ?><form method="post" action="/mensajes_enviados/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
<input type='hidden' name='Mensaje_EnviadoId' id='Mensaje_EnviadoId' value='<?php echo $IDS["Mensaje_EnviadoId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

public function actualizar($parametros) {
        $Actualizarmensajes_enviados = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizarmensajes_enviados == false) {
            return false;
        } else {
            $IDS = array('Mensaje_EnviadoId' => $parametros['Mensaje_EnviadoId']);
            $this->irmensajes_enviadosPagina($IDS, 'ver');
        }
    }
public function queryActualizar($parametros) {
        $QueryActualizarmensajes_enviados = "UPDATE mensajes_enviados SET Mensaje_EnviadoId = '$parametros[Mensaje_EnviadoId]',
UsuarioId = '$parametros[UsuarioId]',
ContactoId = '$parametros[ContactoId]',
Mensaje_EnviadoFechaCreacion = '$parametros[Mensaje_EnviadoFechaCreacion]',
Mensaje_EnviadoFechaEnvio = '$parametros[Mensaje_EnviadoFechaEnvio]',
Mensaje_EnviadoContenido = '$parametros[Mensaje_EnviadoContenido]',
Mensaje_EnviadoOneCheck = '$parametros[Mensaje_EnviadoOneCheck]',
Mensaje_EnviadoDoubleCheck = '$parametros[Mensaje_EnviadoDoubleCheck]',
Mensaje_EnviadoFotoPerfil = '$parametros[Mensaje_EnviadoFotoPerfil]',
Mensaje_EnviadoTexto = '$parametros[Mensaje_EnviadoTexto]',
Mensaje_EnviadoDenunciado = '$parametros[Mensaje_EnviadoDenunciado]',
Mensaje_EnviadoCreditos = '$parametros[Mensaje_EnviadoCreditos]',
Mensaje_EnviadoEnvidado = '$parametros[Mensaje_EnviadoEnvidado]'
 WHERE Mensaje_EnviadoId = $parametros[Mensaje_EnviadoId]";
        return $QueryActualizarmensajes_enviados;
    }

public function borrar($parametros) {
        $Borrarmensajes_enviados = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarmensajes_enviados == FALSE || $Borrarmensajes_enviados == TRUE) {
            header('Location: /mensajes_enviados/ver-todos.php');
        }
    }

public function queryBorrar($parametros) {
        $QueryBorrarmensajes_enviados = "DELETE FROM mensajes_enviados WHERE Mensaje_EnviadoId = $parametros[Mensaje_EnviadoId]";
        return $QueryBorrarmensajes_enviados;
    }

public function crear($parametros) {
        $Crearmensajes_enviados = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearmensajes_enviados == true) {
            header('Location: /mensajes_enviados/ver-todos.php');
        } else {
            return false;
        }
    }

public function queryCrear($parametros) {
        $QueryCrearmensajes_enviados = "INSERT INTO mensajes_enviados (Mensaje_EnviadoId,
UsuarioId,
ContactoId,
Mensaje_EnviadoFechaCreacion,
Mensaje_EnviadoFechaEnvio,
Mensaje_EnviadoContenido,
Mensaje_EnviadoOneCheck,
Mensaje_EnviadoDoubleCheck,
Mensaje_EnviadoFotoPerfil,
Mensaje_EnviadoTexto,
Mensaje_EnviadoDenunciado,
Mensaje_EnviadoCreditos,
Mensaje_EnviadoEnvidado
) VALUES ('$parametros[Mensaje_EnviadoId]',
'$parametros[UsuarioId]',
'$parametros[ContactoId]',
'$parametros[Mensaje_EnviadoFechaCreacion]',
'$parametros[Mensaje_EnviadoFechaEnvio]',
'$parametros[Mensaje_EnviadoContenido]',
'$parametros[Mensaje_EnviadoOneCheck]',
'$parametros[Mensaje_EnviadoDoubleCheck]',
'$parametros[Mensaje_EnviadoFotoPerfil]',
'$parametros[Mensaje_EnviadoTexto]',
'$parametros[Mensaje_EnviadoDenunciado]',
'$parametros[Mensaje_EnviadoCreditos]',
'$parametros[Mensaje_EnviadoEnvidado]'
)";
        return $QueryCrearmensajes_enviados;
    }

public function obtenerPorId($parametros) {
        $this->mensajes_enviados = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->mensajes_enviados == false) {
            return false;
        } else {
            return $this->mensajes_enviados;
        }
    }

public function queryObtenerPorId($parametros) {
        $Querymensajes_enviados = "SELECT Mensaje_EnviadoId,
UsuarioId,
ContactoId,
Mensaje_EnviadoFechaCreacion,
Mensaje_EnviadoFechaEnvio,
Mensaje_EnviadoContenido,
Mensaje_EnviadoOneCheck,
Mensaje_EnviadoDoubleCheck,
Mensaje_EnviadoFotoPerfil,
Mensaje_EnviadoTexto,
Mensaje_EnviadoDenunciado,
Mensaje_EnviadoCreditos,
Mensaje_EnviadoEnvidado
 FROM mensajes_enviados WHERE Mensaje_EnviadoId = $parametros[Mensaje_EnviadoId];";
        return $Querymensajes_enviados;
    }

public function obtenerTodos() {
        $this->mensajes_enviadosTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->mensajes_enviadosTodos == TRUE) {
            return $this->mensajes_enviadosTodos;
        } else {
            return false;
        }
    }

public function queryObtenerTodos() {
        $Querymensajes_enviadosTodos = "SELECT Mensaje_EnviadoId,
UsuarioId,
ContactoId,
Mensaje_EnviadoFechaCreacion,
Mensaje_EnviadoFechaEnvio,
Mensaje_EnviadoContenido,
Mensaje_EnviadoOneCheck,
Mensaje_EnviadoDoubleCheck,
Mensaje_EnviadoFotoPerfil,
Mensaje_EnviadoTexto,
Mensaje_EnviadoDenunciado,
Mensaje_EnviadoCreditos,
Mensaje_EnviadoEnvidado
FROM mensajes_enviados;";
        return $Querymensajes_enviadosTodos;
    }

}