<?php
namespace modelo;

require_once("Modelo.php");

class Modelo_roles extends \modelo\Modelo {

private $ConexionBD;
    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }
public function irrolesPagina($IDS, $Pagina) {
        ?><form method="post" action="/roles/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
<input type='hidden' name='RolId' id='RolId' value='<?php echo $IDS["RolId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

public function actualizar($parametros) {
        $Actualizarroles = $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
        if ($Actualizarroles == false) {
            return false;
        } else {
            $IDS = array('RolId' => $parametros['RolId']);
            $this->irrolesPagina($IDS, 'ver');
        }
    }
public function queryActualizar($parametros) {
        $QueryActualizarroles = "UPDATE roles SET RolId = '$parametros[RolId]',
RolNombre = '$parametros[RolNombre]'
 WHERE RolId = $parametros[RolId]";
        return $QueryActualizarroles;
    }

public function borrar($parametros) {
        $Borrarroles = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarroles == FALSE || $Borrarroles == TRUE) {
            header('Location: /roles/ver-todos.php');
        }
    }

public function queryBorrar($parametros) {
        $QueryBorrarroles = "DELETE FROM roles WHERE RolId = $parametros[RolId]";
        return $QueryBorrarroles;
    }

public function crear($parametros) {
        $Crearroles = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearroles == true) {
            header('Location: /roles/ver-todos.php');
        } else {
            return false;
        }
    }

public function queryCrear($parametros) {
        $QueryCrearroles = "INSERT INTO roles (RolId,
RolNombre
) VALUES ('$parametros[RolId]',
'$parametros[RolNombre]'
)";
        return $QueryCrearroles;
    }

public function obtenerPorId($parametros) {
        $this->roles = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->roles == false) {
            return false;
        } else {
            return $this->roles;
        }
    }

public function queryObtenerPorId($parametros) {
        $Queryroles = "SELECT RolId,
RolNombre
 FROM roles WHERE RolId = $parametros[RolId];";
        return $Queryroles;
    }

public function obtenerTodos() {
        $this->rolesTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->rolesTodos == TRUE) {
            return $this->rolesTodos;
        } else {
            return false;
        }
    }

public function queryObtenerTodos() {
        $QueryrolesTodos = "SELECT RolId,
RolNombre
FROM roles;";
        return $QueryrolesTodos;
    }

}