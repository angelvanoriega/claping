<?php

namespace modelo;

require_once("Modelo.php");

class Modelo_usuarios extends \modelo\Modelo {

    private $ConexionBD;

    public function __construct() {
        $this->ConexionBD = new \basedatos\Conexion();
    }

    public function irusuariosPagina($IDS, $Pagina) {
        ?><form method="post" action="/usuarios/<?php echo $Pagina ?>.php" id="formphp" name="formphp">
            <input type='hidden' name='UsuarioId' id='UsuarioId' value='<?php echo $IDS["UsuarioId"] ?>'/>
        </form><script>document.forms["formphp"].submit();</script><?php
    }

    public function actualizar($parametros) {
        return $this->ConexionBD->ejecutarQuery($this->queryActualizar($parametros));
    }

    public function queryActualizar($parametros) {
        return "UPDATE usuarios SET "
                . "UsuarioId = '$parametros[UsuarioId]', "
                . "UsuarioNombre = '$parametros[UsuarioNombre]',"
                . "UsuarioSobreNombre = '$parametros[UsuarioSobreNombre]',"
                . "UsuarioEmail = '$parametros[UsuarioEmail]',"
                . "UsuarioApellido = '$parametros[UsuarioApellido]',"
                . "UsuarioSexo = '$parametros[UsuarioSexo]',"
                . "UsuarioFechaNacimiento = '$parametros[UsuarioFechaNacimiento]',"
                . "PaisId = '$parametros[PaisId]',"
                . "UsuarioNumeroCelular = '$parametros[UsuarioNumeroCelular]'"
                . " WHERE UsuarioId = $parametros[UsuarioId]";
    }

    public function borrar($parametros) {
        $Borrarusuarios = $this->ConexionBD->ejecutarQuery($this->queryBorrar($parametros));
        if ($Borrarusuarios == FALSE || $Borrarusuarios == TRUE) {
            header('Location: /usuarios/ver-todos.php');
        }
    }

    public function queryBorrar($parametros) {
        $QueryBorrarusuarios = "DELETE FROM usuarios WHERE UsuarioId = $parametros[UsuarioId]";
        return $QueryBorrarusuarios;
    }

    public function crear($parametros) {
        $Crearusuarios = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($Crearusuarios == true) {
            header('Location: /usuarios/ver-todos.php');
        } else {
            return false;
        }
    }

    public function registrarse($parametros) {
        $registrarse = $this->ConexionBD->ejecutarQuery($this->queryCrear($parametros));
        if ($registrarse == true) {
            return true;
        } else {
            return false;
        }
    }

    public function queryCrear($parametros) {
        $QueryCrearusuarios = "INSERT INTO usuarios (UsuarioNombre, UsuarioSobreNombre, UsuarioEmail, UsuarioContrasenia, 
            UsuarioFechaRegistro, UsuarioActivo, PaisId, UsuarioNumeroCelular, RolId, UsuarioSexo, UsuarioApellido, UsuarioFechaNacimiento) 
            VALUES ('$parametros[UsuarioNombre]','$parametros[UsuarioSobreNombre]','$parametros[UsuarioEmail]',"
                . "'$parametros[UsuarioContrasenia]','$parametros[UsuarioFechaRegistro]','$parametros[UsuarioActivo]','"
                . "$parametros[PaisId]','$parametros[UsuarioNumeroCelular]','$parametros[RolId]',"
                . "'$parametros[UsuarioSexo]','$parametros[UsuarioApellido]','$parametros[UsuarioFechaNacimiento]')";
        return $QueryCrearusuarios;
    }

    /*
     * ***************************************************************************************************
     */

    public function cambiarContrasenia($parametros) {
        $ActualizarContrasenia = $this->ConexionBD->ejecutarQuery($this->queryActualizarContrasenia($parametros));
        if ($ActualizarContrasenia == false) {
            return false;
        } else {
            return true;
        }
    }

    public function queryActualizarContrasenia($parametros) {
        $QueryActualizarContrasenia = "UPDATE usuarios "
                . "SET UsuarioContrasenia = '$parametros[ContraseniaNueva]' "
                . "WHERE UsuarioId = $parametros[UsuarioId];";
        return $QueryActualizarContrasenia;
    }

    /*     * ******************************************************************************************************** */

    public function cambiarRolUsuarioM($parametros) {
        $this->cambioRol = $this->ConexionBD->ejecutarQuery($this->queryCambiarRolUsuario($parametros));
        return $this->cambioRol->fetchAll();
    }

    public function queryCambiarRolUsuario($parametros) {
        $QueryActualizarContrasenia = "UPDATE usuarios "
                . "SET RolId = '$parametros[RolId]' "
                . "WHERE UsuarioId = $parametros[UsuarioId];";
        return $QueryActualizarContrasenia;
    }
    public function cambiarActivoUsuarioM($parametros) {
        $this->cambioActivo = $this->ConexionBD->ejecutarQuery($this->queryCambiarActivoUsuario($parametros));
        return $this->cambioActivo->fetchAll();
    }

    public function queryCambiarActivoUsuario($parametros) {
        $queryCambiarActivoUsuario = "UPDATE usuarios "
                . "SET UsuarioActivo = '$parametros[UsuarioActivo]' "
                . "WHERE UsuarioId = $parametros[UsuarioId];";
        return $queryCambiarActivoUsuario;
    }

    public function existeUsuario($parametros) {
        $this->usuario = $this->ConexionBD->ejecutarQuery($this->queryExisteUsuario($parametros));
        $usuario = $this->usuario->fetchAll();
        if ($usuario == false) {
            return false;
        } else {
            return $usuario;
        }
    }

    public function queryExisteUsuario($parametros) {
        $Queryusuarios = "SELECT UsuarioId, UsuarioNombre,UsuarioSobreNombre,UsuarioEmail,UsuarioContrasenia,
            UsuarioFechaRegistro,UsuarioActivo,PaisId,UsuarioNumeroCelular, RolId FROM usuarios 
            WHERE UsuarioSobreNombre = '$parametros[UsuarioSobreNombre]' "
                . "AND UsuarioContrasenia = '$parametros[UsuarioContrasenia]';";
        return $Queryusuarios;
    }

    public function existeSobreNombre($UsuarioSobreNombre) {
        $this->usuario = $this->ConexionBD->ejecutarQuery($this->queryExisteSobreNombre($UsuarioSobreNombre));
        $usuario = $this->usuario->fetchAll();
        if ($usuario == false) {
            return false;
        } else {
            return true;
        }
    }

    public function queryExisteSobreNombre($UsuarioSobreNombre) {
        $Queryusuarios = "SELECT * FROM usuarios WHERE UsuarioSobreNombre = '$UsuarioSobreNombre';";
        return $Queryusuarios;
    }

    public function existeCorreo($UsuarioEmail) {
        $this->usuario = $this->ConexionBD->ejecutarQuery($this->queryExisteCorreo($UsuarioEmail));
        $correo = $this->usuario->fetchAll();
        return $correo;
    }

    public function queryExisteCorreo($UsuarioEmail) {
        $Queryusuarios = "SELECT * FROM usuarios WHERE UsuarioEmail = '$UsuarioEmail';";
        return $Queryusuarios;
    }

    /*     * ******************************************************************************************************** */

    public function verificarContraseniaUsuario($parametros) {
        $this->usuario = $this->ConexionBD->ejecutarQuery($this->queryVerificarContraseniaUsuario($parametros));
        $usuario = $this->usuario->fetchAll();
        if ($usuario == false) {
            return false;
        } else {
            return true;
        }
    }

    public function queryVerificarContraseniaUsuario($parametros) {
        $Queryusuarios = "SELECT * FROM usuarios 
            WHERE UsuarioContrasenia = '$parametros[UsuarioContrasenia]' "
                . "AND UsuarioId = '$parametros[UsuarioId]';";
        return $Queryusuarios;
    }

    /*     * ******************************************************************************************************** */

    public function obtenerPorId($parametros) {
        $this->usuarios = $this->ConexionBD->ejecutarQuery($this->queryObtenerPorId($parametros));
        if ($this->usuarios == false) {
            return false;
        } else {
            return $this->usuarios;
        }
    }

    public function queryObtenerPorId($parametros) {
        $Queryusuarios = "SELECT usuarios.UsuarioId, usuarios.UsuarioNombre, usuarios.UsuarioSobreNombre, "
                . "usuarios.UsuarioEmail, usuarios.UsuarioContrasenia, usuarios.UsuarioFechaRegistro, "
                . "usuarios.UsuarioActivo, usuarios.UsuarioNumeroCelular, usuarios.UsuarioSexo, usuarios.UsuarioApellido, "
                . "usuarios.UsuarioFechaNacimiento, usuarios.PaisId, paises.PaisNombre, paises.PaisClaveLada, usuarios.RolId, "
                . "roles.RolNombre FROM (usuarios INNER JOIN paises ON usuarios.PaisId = paises.PaisId) "
                . "INNER JOIN roles ON usuarios.RolId = roles.RolId "
                . "WHERE usuarios.UsuarioId = $parametros[UsuarioId];";
        return $Queryusuarios;
    }

    public function obtenerTodos() {
        $this->usuariosTodos = $this->ConexionBD->ejecutarQuery($this->queryObtenerTodos());
        if ($this->usuariosTodos == TRUE) {
            return $this->usuariosTodos;
        } else {
            return false;
        }
    }

    public function queryObtenerTodos() {
        $QueryusuariosTodos = "SELECT usuarios.UsuarioId, usuarios.UsuarioNombre, usuarios.UsuarioSobreNombre, "
                . "usuarios.UsuarioEmail, usuarios.UsuarioContrasenia, usuarios.UsuarioFechaRegistro, "
                . "usuarios.UsuarioActivo, usuarios.UsuarioNumeroCelular, usuarios.UsuarioSexo, usuarios.UsuarioApellido, "
                . "usuarios.UsuarioFechaNacimiento, usuarios.PaisId, paises.PaisNombre, paises.PaisClaveLada, usuarios.RolId, "
                . "roles.RolNombre FROM (usuarios INNER JOIN paises ON usuarios.PaisId = paises.PaisId) "
                . "INNER JOIN roles ON usuarios.RolId = roles.RolId ";
        return $QueryusuariosTodos;
    }

}
