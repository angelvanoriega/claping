<?php

namespace controlador;

require_once("Controlador.php");

class Controlador_grupos extends \controlador\Controlador {

    private $todos;
    private $grupos;

    public function __construct() {
        parent::__construct();
        $this->Modelogrupos = new \modelo\Modelo_grupos();
        $this->ModeloPaises = new \modelo\Modelo_paises();
        $this->ConexionBD = new \basedatos\Conexion();
    }

    public function getPaises() {
        return $this->paises;
    }

    public function setPaises($paises) {
        $this->paises = $paises->fetchAll();
        return $this;
    }

    public function obtenerModelo() {
        return $this->Modelogrupos;
    }

    public function getTodos() {
        return $this->todos;
    }

    public function setTodos($todos) {
        $this->todos = $todos->fetchAll();
        return $this;
    }

    public function getgrupos() {
        return $this->grupos;
    }

    public function setgrupos($grupos) {
        $this->grupos = $grupos->fetchAll(\PDO::FETCH_ASSOC);
        return $this;
    }

    public function crear_grupo_bd() {
        $GrupoNombre = filter_input(INPUT_POST, "GrupoNombre");
        $Numeros = filter_input(INPUT_POST, "Numeros");
        $IdUsuario = filter_input(INPUT_POST, "IdUsuario");
        $PaisId = filter_input(INPUT_POST, "PaisId");
        if (filter_input(INPUT_POST, "Creargrupos") == 'Creargrupos') {
            if (isset($GrupoNombre) && isset($Numeros) &&
                    isset($IdUsuario) && isset($PaisId)) {
                $parametros = array('GrupoNombre' => $GrupoNombre, 'Numeros' => $Numeros,
                    'IdUsuario' => $IdUsuario, 'PaisId' => $PaisId);
                $crear = $this->obtenerModelo()->crear($parametros);
                return $crear;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function crear() {
        $titulo = "Crear grupos";
        $Vista = 'grupos/Vista_Crear_grupos.php';
        $this->setPaises($this->ModeloPaises->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

    public function actualizar() {
        $GrupoId = filter_input(INPUT_POST, "GrupoId");
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        
        if (isset($GrupoId) && isset($UsuarioId)) {
            $parametros = array('GrupoId' => $GrupoId, 'UsuarioId' => $UsuarioId);
            $this->setgrupos($this->obtenerModelo()->obtenerPorId($parametros));
            
            foreach ($this->grupos as $_grupos) {
                $PaisIdBD = $_grupos['PaisId'];
                $GrupoNombreBD = $_grupos['GrupoNombre'];
                $NumerosBD = $_grupos['Numeros'];
                $GrupoCreador_UsuarioIdBD = $_grupos['GrupoCreador_UsuarioId'];
            }
        }

        if (filter_input(INPUT_POST, "Actualizargrupos") == 'Actualizargrupos') {
            $PaisId = filter_input(INPUT_POST, "PaisId");
            $GrupoNombre = filter_input(INPUT_POST, "GrupoNombre");
            $Numeros = filter_input(INPUT_POST, "tos");
            $GrupoCreador_UsuarioId = filter_input(INPUT_POST, "UsuarioId");
            
            if (strcmp($PaisId, $PaisIdBD) == 0 &&
                    strcmp($GrupoNombre, $GrupoNombreBD) == 0 &&
                    strcmp($Numeros, $NumerosBD) == 0 &&
                    strcmp($GrupoCreador_UsuarioId, $GrupoCreador_UsuarioIdBD) == 0) {
                header('Location: /grupos/ver-todos.php');
            } else {
                $parametrosActualizar = array('GrupoId' => $GrupoId, 'PaisId' => $PaisId, 'Numeros' => $Numeros,
                    'GrupoNombre' => $GrupoNombre, 'GrupoCreador_UsuarioId' => $GrupoCreador_UsuarioId);
                $this->obtenerModelo()->actualizar($parametrosActualizar);
            }
        }

        $this->setPaises($this->ModeloPaises->obtenerTodos());
        $titulo = "Editar grupos";
        $Vista = "grupos/Vista_Actualizar_grupos.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function obtener_grupo() {
        $GrupoId = filter_input(INPUT_POST, "GrupoId");
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        if (isset($GrupoId) && isset($UsuarioId)) {
            $parametros = array('GrupoId' => $GrupoId, 'UsuarioId' => $UsuarioId);
            $this->grupos = $this->obtenerModelo()->obtenerPorId($parametros);
            if (isset($this->grupos)) {
                $sentencia = $this->grupos->fetchAll(\PDO::FETCH_ASSOC);
                return utf8_encode(json_encode($sentencia));
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function obtenerPorId() {
        $GrupoId = filter_input(INPUT_POST, "GrupoId");
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        if (isset($GrupoId) && isset($UsuarioId)) {
            $parametros = array('GrupoId' => $GrupoId, 'UsuarioId' => $UsuarioId);
            $this->grupos = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver grupos";
        $Vista = "grupos/Vista_Ver_grupos.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function borrar() {
        $GrupoId = filter_input(INPUT_POST, "GrupoId");
        if (isset($GrupoId)) {
            $parametros = array('GrupoId' => $GrupoId);
            $Borrargrupos = $this->obtenerModelo()->borrar($parametros);
            if ($Borrargrupos === false) {
                header('Location: grupos/ver-todos.php');
            }
        }
    }

    public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'grupos/Vista_Ver_Todos.php';
        $this->setTodos($this->obtenerModelo()->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

}
