<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controlador;

require_once("Plantilla.php");
require_once("modelos.php");

/**
 * Description of Controlador
 *
 * @author angel_000
 */
abstract class Controlador {

    public function __construct() {
        $this->Plantilla = new \plantilla\Plantilla();
        $this->controlarSesion();
    }

    public function controlarSesion() {
        session_start();
        if (empty(($_SESSION['UsuarioId']))) {
            header('Location: /login/login.php');
        } else {
            $date = date("Y-m-d H:i:s");
            $DiferenciaMinutos = $this->intervaloMinutos($_SESSION['UltimoInicio'], $date);
            if ($DiferenciaMinutos > 30000000) {
                session_destroy();
                header('Location: /login/login.php');
            }
        }
    }

    public function intervaloMinutos($fecha1, $fecha2) {
        $fecha1DT = new \DateTime($fecha1);
        $fecha2DT = new \DateTime($fecha2);
        $intervalo = $fecha1DT->diff($fecha2DT);
        $numeroDias = $intervalo->format('%i');
        return $numeroDias;
    }

    public function cargarPagina($titulo, $Vista) {
        $this->Plantilla->obtenerCabeceraHTML($titulo);
        require_once $Vista;
        $this->Plantilla->obtenerPieHTML();
    }
    
    public function alertError($mensaje) {
        ?>
        <link rel="stylesheet" type="text/css" href="/assets/javascript/sweetalert-master/sweetalert.css" media="all">
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert-dev.js"></script>
        <br><script type='text/javascript'>
            swal({
                title: "ERROR!!!",
                text: "<?php echo $mensaje ?>",
                type: "error"},
            function () {
                javascript:history.back();
            });
        </script><?php
    }
    
    public function alertSuccess($mensaje) {
        ?>
        <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/javascript/sweetalert-master/sweetalert.css" media="all">
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/javascript/sweetalert-master/sweetalert-dev.js"></script>
        <br><script type='text/javascript'>
            swal({
                title: "Enhorabuena!!!",
                text: "<?php echo $mensaje ?>",
                type: "success"},
            function () {
                javascript:history.back();
            });
        </script><?php
    }

    /**
     * Funcion abstracta para obtener todos los objetos del modelo.
     */
    public abstract function obtenerTodos();

    public abstract function obtenerPorId();

    public abstract function crear();

    public abstract function actualizar();

    public abstract function borrar();
}
