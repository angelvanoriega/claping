<?php

namespace controlador;

require_once("Plantilla.php");
require_once("modelos.php");

class Controlador_login extends \plantilla\Plantilla {

    private $todos;
    private $login;
    private $paises;

    public function __construct() {
        $this->Modelologin = new \modelo\Modelo_login();
        $this->Modelo_usuarios = new \modelo\Modelo_usuarios();
        $this->ModeloPaises = new \modelo\Modelo_paises();
    }

    public function obtenerModelo() {
        return $this->Modelologin;
    }

    public function getTodos() {
        return $this->todos;
    }

    public function setTodos($todos) {
        $this->todos = $todos->fetchAll();
        return $this;
    }

    public function getPaises() {
        return $this->paises;
    }

    public function setPaises($paises) {
        $this->paises = $paises->fetchAll();
        return $this;
    }

    public function getlogin() {
        return $this->login;
    }

    public function setlogin($login) {
        $this->login = $login->fetchAll();
        return $this;
    }

    public function controlarSesion() {
        session_start();
        if (empty(($_SESSION['UsuarioId']))) {
            header('Location: /login/login.php');
        } else {
            $date = date("Y-m-d H:i:s");
            $DiferenciaMinutos = $this->intervaloMinutos($_SESSION['UltimoInicio'], $date);
            if ($DiferenciaMinutos > 30) {
                session_destroy();
                header('Location: /login.php');
            }
        }
    }

    public function intervaloMinutos($fecha1, $fecha2) {
        $fecha1DT = new \DateTime($fecha1);
        $fecha2DT = new \DateTime($fecha2);
        $intervalo = $fecha1DT->diff($fecha2DT);
        $numeroDias = $intervalo->format('%i');
        return $numeroDias;
    }

    public function iniciar_sesion($usuario) {
        session_start();
        $date = date("Y-m-d H:i:s");
        foreach ($usuario as $datosUsuario) {
            $_SESSION['UsuarioId'] = $datosUsuario["UsuarioId"];
            $_SESSION['UsuarioNombre'] = $datosUsuario["UsuarioNombre"];
            $_SESSION['UsuarioSobreNombre'] = $datosUsuario["UsuarioSobreNombre"];
            $_SESSION['UsuarioEmail'] = $datosUsuario["UsuarioEmail"];
            $_SESSION['UsuarioContrasenia'] = $datosUsuario["UsuarioContrasenia"];
            $_SESSION['UsuarioFechaRegistro'] = $datosUsuario["UsuarioFechaRegistro"];
            $_SESSION['UsuarioActivo'] = $datosUsuario["UsuarioActivo"];
            $_SESSION['PaisId'] = $datosUsuario["PaisId"];
            $_SESSION['UsuarioNumeroCelular'] = $datosUsuario["UsuarioNumeroCelular"];
            $_SESSION['RolId'] = $datosUsuario["RolId"];
            $_SESSION['UltimoInicio'] = $date;
        }
    }

    public function recuperarContrasenia() {
        $titulo = "Recuperar contraseña";
        $Vista = 'login/Vista_Recuperar_Contrasenia.php';
        $CorreoE = filter_input(INPUT_POST, "correoe");
        if (isset($CorreoE)) {
            $this->alertSuccess("Se ha enviado mensaje al correo: " . $CorreoE);
        }
        $this->cargarPagina($titulo, $Vista);
    }

    public function registrar() {
        if (filter_input(INPUT_POST, "Crearusuarios") == 'Crearusuarios') {
            $UsuarioNombre = filter_input(INPUT_POST, "UsuarioNombre");
            $UsuarioApellido = filter_input(INPUT_POST, "UsuarioApellido");
            $UsuarioSobreNombre = filter_input(INPUT_POST, "UsuarioSobreNombre");
            $UsuarioEmail = filter_input(INPUT_POST, "UsuarioEmail");
            $UsuarioContrasenia = filter_input(INPUT_POST, "UsuarioContrasenia");
            $PaisId = filter_input(INPUT_POST, "PaisId");
            $UsuarioNumeroCelular = filter_input(INPUT_POST, "UsuarioNumeroCelular");
            $UsuarioSexo = filter_input(INPUT_POST, "UsuarioSexo");
            $UsuarioFechaNacimiento = filter_input(INPUT_POST, "UsuarioFechaNacimiento");
            $UsuarioFechaRegistro = date("Y-m-d");
            $UsuarioActivo = 0;
            $RolId = 2;
            if (isset($UsuarioNombre) && isset($UsuarioApellido) && isset($UsuarioSobreNombre) && isset($UsuarioFechaNacimiento) &&
                    isset($UsuarioEmail) && isset($UsuarioContrasenia) && isset($UsuarioFechaRegistro) && isset($UsuarioActivo) &&
                    isset($PaisId) && isset($UsuarioNumeroCelular) && isset($RolId) && isset($UsuarioSexo)) {

                $parametros = array('UsuarioNombre' => $UsuarioNombre, 'UsuarioApellido' => $UsuarioApellido,
                    'UsuarioSobreNombre' => $UsuarioSobreNombre, 'UsuarioEmail' => $UsuarioEmail, 'UsuarioContrasenia' => $UsuarioContrasenia,
                    'UsuarioFechaRegistro' => $UsuarioFechaRegistro, 'UsuarioActivo' => $UsuarioActivo, 'UsuarioSexo' => $UsuarioSexo,
                    'PaisId' => $PaisId, 'UsuarioNumeroCelular' => $UsuarioNumeroCelular, 'RolId' => $RolId, 'UsuarioFechaNacimiento' => $UsuarioFechaNacimiento);

                $registro = $this->Modelo_usuarios->registrarse($parametros);
                if ($registro) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function registrarse() {
        $titulo = "Registrar usuario nuevo";
        $Vista = 'login/Vista_Registrar_Usuario.php';
        $this->setPaises($this->ModeloPaises->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

    public function login() {
        $Usuario = filter_input(INPUT_POST, "Usuario");
        $Contrasenia = filter_input(INPUT_POST, "Contrasenia");
        if (filter_input(INPUT_POST, "login") == 'login') {
            if (isset($Usuario) && isset($Contrasenia)) {
                $parametros = array('UsuarioSobreNombre' => $Usuario, 'UsuarioContrasenia' => $Contrasenia);
                $usuario = $this->Modelo_usuarios->existeUsuario($parametros);
                if ($usuario[0][6] == 1) {
                    $this->iniciar_sesion($usuario);
                    $datetime = new \DateTime();
                    $FechaEntrada = $datetime->format('Y-m-d H:i:s');
                    $ContraseniaMD5 = md5($Contrasenia);
                    $parametrosLogin = array('Usuario' => $Usuario, 'Contrasenia' => $ContraseniaMD5, 'FechaEntrada' => $FechaEntrada);
                    $this->obtenerModelo()->crear($parametrosLogin);
                }
            }
        }
        $titulo = "Iniciar sesion";
        $Vista = 'login/Vista_login.php';
        $this->cargarPagina($titulo, $Vista);
    }

    public function existeSobreNombre() {
        $UsuarioSobreNombre = filter_input(INPUT_POST, "UsuarioSobreNombre");
        if (isset($UsuarioSobreNombre)) {
            $existe = $this->Modelo_usuarios->existeSobreNombre($UsuarioSobreNombre);
            if ($existe == true) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function existeCorreo() {
        $UsuarioEmail = filter_input(INPUT_POST, "UsuarioEmail");
        if (isset($UsuarioEmail)) {
            $correo = $this->Modelo_usuarios->existeCorreo($UsuarioEmail);
            return $correo;
        }
    }

    public function crear() {
        $titulo = "Crear login";
        $Vista = 'login/Vista_Crear_login.php';
        $FechaEntrada = date();
        if (filter_input(INPUT_POST, "Crearlogin") == 'Crearlogin') {
            if (isset($this->usuario) && isset($this->Contrasenia) && isset($FechaEntrada)) {
                exit();
                $parametros = array('LoginId' => $LoginId, 'Usuario' => $Usuario, 'Contrasenia' => $Contrasenia, 'FechaEntrada' => $FechaEntrada);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

    public function actualizar() {
        $LoginId = filter_input(INPUT_POST, "LoginId");
        if (isset($LoginId)) {
            $parametros = array('LoginId' => $LoginId);
            $this->setlogin($this->obtenerModelo()->obtenerPorId($parametros));
            if (filter_input(INPUT_POST, "Actualizarlogin") == 'Actualizarlogin') {
                $LoginId = filter_input(INPUT_POST, "LoginId");
                $Usuario = filter_input(INPUT_POST, "Usuario");
                $Contrasenia = filter_input(INPUT_POST, "Contrasenia");
                $FechaEntrada = filter_input(INPUT_POST, "FechaEntrada");
                if (isset($LoginId) && isset($Usuario) && isset($Contrasenia) && isset($FechaEntrada)) {
                    foreach ($this->login as $_login) {
                        $LoginIdBD = $_login['LoginId'];
                        $UsuarioBD = $_login['Usuario'];
                        $ContraseniaBD = $_login['Contrasenia'];
                        $FechaEntradaBD = $_login['FechaEntrada'];
                    }
                    if (strcmp($LoginId, $LoginIdBD) == 0 &&
                            strcmp($Usuario, $UsuarioBD) == 0 &&
                            strcmp($Contrasenia, $ContraseniaBD) == 0 &&
                            strcmp($FechaEntrada, $FechaEntradaBD) == 0) {
                        header('Location: /login/ver-todos.php');
                    } else {
                        $parametrosActualizar = array('LoginId' => $LoginId, 'Usuario' => $Usuario, 'Contrasenia' => $Contrasenia, 'FechaEntrada' => $FechaEntrada);
                        $this->obtenerModelo()->actualizar($parametrosActualizar);
                    }
                }
            }
        }
        $titulo = "Editar login";
        $Vista = "login/Vista_Actualizar_login.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function obtenerPorId() {
        $LoginId = filter_input(INPUT_POST, "LoginId");
        if (isset($LoginId)) {
            $parametros = array('LoginId' => $LoginId);
            $this->login = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver login";
        $Vista = "login/Vista_Ver_login.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function borrar() {
        $LoginId = filter_input(INPUT_POST, "LoginId");
        if (isset($LoginId)) {
            $parametros = array('LoginId' => $LoginId);
            $Borrarlogin = $this->obtenerModelo()->borrar($parametros);
            if ($Borrarlogin === false) {
                header('Location: login/ver-todos.php');
            }
        }
    }

    public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'login/Vista_Ver_Todos.php';
        $this->setTodos($this->obtenerModelo()->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

}
