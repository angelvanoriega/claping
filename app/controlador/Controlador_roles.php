<?php
namespace controlador;

require_once("Controlador.php");
class Controlador_roles extends \controlador\Controlador {

private $todos;
private $roles;

public function __construct() {
        parent::__construct();
        $this->Modeloroles = new \modelo\Modelo_roles();
}

public function obtenerModelo() {
        return $this->Modeloroles;
    }

public function getTodos() {
    return $this->todos;
}

public function setTodos($todos) {
    $this->todos = $todos->fetchAll();
    return $this;
}
public function getroles() {
    return $this->roles;
}

public function setroles($roles) {
    $this->roles = $roles->fetchAll();
    return $this;
}

public function crear() {
        $titulo = "Crear roles";
        $Vista = 'roles/Vista_Crear_roles.php';
        $RolId = filter_input(INPUT_POST, "RolId");
        $RolNombre = filter_input(INPUT_POST, "RolNombre");
        if (filter_input(INPUT_POST, "Crearroles") == 'Crearroles') {
            if (isset($RolId) && isset($RolNombre)) {
                $parametros = array('RolId' => $RolId,'RolNombre' => $RolNombre);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

public function actualizar() {
        $RolId = filter_input(INPUT_POST, "RolId");
        if (isset($RolId)) {
            $parametros = array('RolId' => $RolId);
            $this->setroles($this->obtenerModelo()->obtenerPorId($parametros));
            if (filter_input(INPUT_POST, "Actualizarroles") == 'Actualizarroles') {
        $RolId = filter_input(INPUT_POST, "RolId");
        $RolNombre = filter_input(INPUT_POST, "RolNombre");
                if (isset($RolId) && isset($RolNombre)) {
                    foreach ($this->roles as $_roles) {
           $RolIdBD = $_roles['RolId'];
           $RolNombreBD = $_roles['RolNombre'];
                    }
                        if (strcmp($RolId,$RolIdBD)== 0 && 
strcmp($RolNombre,$RolNombreBD)== 0) {
                            header('Location: /roles/ver-todos.php');
                        } else {
                            $parametrosActualizar = array('RolId' => $RolId,'RolNombre' => $RolNombre);
                            $this->obtenerModelo()->actualizar($parametrosActualizar);
                        }
                    }
                }
            }
        $titulo = "Editar roles";
        $Vista = "roles/Vista_Actualizar_roles.php";
        $this->cargarPagina($titulo, $Vista);
    }
public function obtenerPorId() {
        $RolId = filter_input(INPUT_POST, "RolId");
        if (isset($RolId)) {
            $parametros = array('RolId' => $RolId);
            $this->roles = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver roles";
        $Vista = "roles/Vista_Ver_roles.php";
        $this->cargarPagina($titulo, $Vista);
    }
public function borrar() {
        $RolId = filter_input(INPUT_POST, "RolId");
        if (isset($RolId)) {
            $parametros = array('RolId' => $RolId);
        $Borrarroles = $this->obtenerModelo()->borrar($parametros);
        if ($Borrarroles === false) {
            header('Location: roles/ver-todos.php');
        }
        }
    }
public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'roles/Vista_Ver_Todos.php';
        $this->setTodos($this->obtenerModelo()->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

}