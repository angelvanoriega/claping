<?php

namespace controlador;

require_once("Controlador.php");

class Controlador_usuarios extends \controlador\Controlador {

    private $todos;
    private $usuarios;

    public function __construct() {
        parent::__construct();
        $this->Modelousuarios = new \modelo\Modelo_usuarios();
    }

    public function obtenerModelo() {
        return $this->Modelousuarios;
    }

    public function getTodos() {
        return $this->todos;
    }

    public function setTodos($todos) {
        $this->todos = $todos->fetchAll();
        return $this;
    }

    public function getusuarios() {
        return $this->usuarios;
    }

    public function setusuarios($usuarios) {
        $this->usuarios = $usuarios->fetchAll();
        return $this;
    }

    public function cambiarClave() {
        $titulo = "Cambiar clave";
        $Vista = 'usuarios/Vista_Cambiar_Clave.php';
        $UsuarioContrasenia = filter_input(INPUT_POST, "UsuarioContraseniaMod");
        $UsuarioId = $_SESSION['UsuarioId'];
        if (isset($UsuarioContrasenia) && isset($UsuarioId)) {
            $parametros = array('UsuarioContrasenia' => $UsuarioContrasenia,
                'UsuarioId' => $UsuarioId);
            $this->usuarios = $this->obtenerModelo()->verificarContraseniaUsuario($parametros);
            if ($this->usuarios == false) {
                $this->alertError("Datos incorrectos");
            } else {
                $contraseniaAnterior = $this->usuarios[0]['UsuarioContrasenia'];
                $ContraseniaNueva = filter_input(INPUT_POST, "UsuarioContrasenia");
                $ContraseniaConfirmacion = filter_input(INPUT_POST, "UsuarioContraseniaConfirmacion");
                if (isset($ContraseniaNueva) && isset($ContraseniaConfirmacion)) {
                    if (strcmp($contraseniaAnterior, $ContraseniaNueva) === 0) {
                        $this->alertError("La contraseña debe ser diferente a la anterior.");
                    } else {
                        $this->validacionContrasenia($ContraseniaNueva, $ContraseniaConfirmacion, $UsuarioId);
                    }
                }
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

    public function validacionContrasenia($ContraseniaNueva, $ContraseniaConfirmacion, $UsuarioId) {
        $parametros = array('ContraseniaNueva' => $ContraseniaNueva,
            'UsuarioId' => $UsuarioId);
        $cambio = $this->obtenerModelo()->cambiarContrasenia($parametros);
        if ($cambio === true) {
            $this->alertSuccess("Se ha cambiado la contraseña");
        } else {
            $this->alertError("Contraseñas diferentes");
        }
    }

    public function crear() {
        $titulo = "Crear usuarios";
        $Vista = 'usuarios/Vista_Crear_usuarios.php';
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        $UsuarioNombre = filter_input(INPUT_POST, "UsuarioNombre");
        $UsuarioSobreNombre = filter_input(INPUT_POST, "UsuarioSobreNombre");
        $UsuarioEmail = filter_input(INPUT_POST, "UsuarioEmail");
        $UsuarioContrasenia = filter_input(INPUT_POST, "UsuarioContrasenia");
        $UsuarioFechaRegistro = filter_input(INPUT_POST, "UsuarioFechaRegistro");
        $UsuarioActivo = filter_input(INPUT_POST, "UsuarioActivo");
        $PaisId = filter_input(INPUT_POST, "PaisId");
        $UsuarioNumeroCelular = filter_input(INPUT_POST, "UsuarioNumeroCelular");
        $RolId = filter_input(INPUT_POST, "RolId");
        if (filter_input(INPUT_POST, "Crearusuarios") == 'Crearusuarios') {
            if (isset($UsuarioId) && isset($UsuarioNombre) && isset($UsuarioSobreNombre) && isset($UsuarioEmail) && isset($UsuarioContrasenia) && isset($UsuarioFechaRegistro) && isset($UsuarioActivo) && isset($PaisId) && isset($UsuarioNumeroCelular) && isset($RolId)) {
                $parametros = array('UsuarioId' => $UsuarioId, 'UsuarioNombre' => $UsuarioNombre, 'UsuarioSobreNombre' => $UsuarioSobreNombre, 'UsuarioEmail' => $UsuarioEmail, 'UsuarioContrasenia' => $UsuarioContrasenia, 'UsuarioFechaRegistro' => $UsuarioFechaRegistro, 'UsuarioActivo' => $UsuarioActivo, 'PaisId' => $PaisId, 'UsuarioNumeroCelular' => $UsuarioNumeroCelular, 'RolId' => $RolId);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

    public function actualizar() {
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        if (isset($UsuarioId)) {
            $parametros = array('UsuarioId' => $UsuarioId);
            $this->setusuarios($this->obtenerModelo()->obtenerPorId($parametros));
            $titulo = "Editar usuario";
            $Vista = "usuarios/Vista_Actualizar_usuarios.php";
            $this->cargarPagina($titulo, $Vista);
        }
    }

    public function cambiarRolUsuario() {
        $UsuarioId = filter_input(INPUT_POST, 'UsuarioId');
        $RolId = filter_input(INPUT_POST, 'RolNuevo');
        if (isset($UsuarioId) && isset($RolId)) {
            $parametros = array('UsuarioId' => $UsuarioId, 'RolId' => $RolId);
            return $this->obtenerModelo()->cambiarRolUsuarioM($parametros);
        }
    }
    
    public function cambiarActivoUsuario() {
        $UsuarioId = filter_input(INPUT_POST, 'UsuarioId');
        $UsuarioActivo = filter_input(INPUT_POST, 'UsuarioActivo');
        if (isset($UsuarioId) && isset($UsuarioActivo)) {
            $parametros = array('UsuarioId' => $UsuarioId, 'UsuarioActivo' => $UsuarioActivo);
            return $this->obtenerModelo()->cambiarActivoUsuarioM($parametros);
        }
    }

    public function actualizarUsuario() {
        if (filter_input(INPUT_POST, "Actualizarusuarios") == 'Actualizarusuarios') {
            $UsuarioId = $_SESSION['UsuarioId'];
            if (isset($UsuarioId)) {
                $parametros = array('UsuarioId' => $UsuarioId);
                $this->setusuarios($this->obtenerModelo()->obtenerPorId($parametros));
                $UsuarioNombre = filter_input(INPUT_POST, "UsuarioNombre");
                $UsuarioApellido = filter_input(INPUT_POST, "UsuarioApellido");
                $UsuarioSobreNombre = filter_input(INPUT_POST, "UsuarioSobreNombre");
                $UsuarioEmail = filter_input(INPUT_POST, "UsuarioEmail");
                $UsuarioNumeroCelular = filter_input(INPUT_POST, "UsuarioNumeroCelular");
                $UsuarioSexo = filter_input(INPUT_POST, "UsuarioSexo");
                $PaisId = filter_input(INPUT_POST, "PaisId");
                $UsuarioFechaNacimiento = filter_input(INPUT_POST, "UsuarioFechaNacimiento");
                $parametrosActualizar = array('UsuarioId' => $UsuarioId,
                    'UsuarioApellido' => $UsuarioApellido, 'UsuarioNombre' => $UsuarioNombre,
                    'UsuarioSobreNombre' => $UsuarioSobreNombre, 'UsuarioEmail' => $UsuarioEmail,
                    'UsuarioSexo' => $UsuarioSexo, 'UsuarioFechaNacimiento' => $UsuarioFechaNacimiento,
                    'PaisId' => $PaisId, 'UsuarioNumeroCelular' => $UsuarioNumeroCelular);
                return $this->obtenerModelo()->actualizar($parametrosActualizar);
            } else {
                return FALSE;
            }
        } else {
            return false;
        }
    }

    public function obtenerPorId() {
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        if (isset($UsuarioId)) {
            $parametros = array('UsuarioId' => $UsuarioId);
            $this->usuarios = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver usuarios";
        $Vista = "usuarios/Vista_Ver_usuarios.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function borrar() {
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        if (isset($UsuarioId)) {
            $parametros = array('UsuarioId' => $UsuarioId);
            $Borrarusuarios = $this->obtenerModelo()->borrar($parametros);
            if ($Borrarusuarios === false) {
                header('Location: usuarios/ver-todos.php');
            }
        }
    }

    public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'usuarios/Vista_Ver_Todos.php';
        $this->setTodos($this->obtenerModelo()->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

}
