<?php

namespace controlador;

require_once("Controlador.php");

class Controlador_mensajes_enviados extends \controlador\Controlador {

    private $todos;
    private $mensajes_enviados;
    private $contactos;
    private $paises;
    private $grupos;

    public function __construct() {
        parent::__construct();
        $this->Modelomensajes_enviados = new \modelo\Modelo_mensajes_enviados();
        $this->ModeloPaises = new \modelo\Modelo_paises();
        $this->ModeloGrupos = new \modelo\Modelo_grupos();
        $this->Modelo_API = new \modelo\Modelo_API();
    }

    public function getPaises() {
        return $this->paises;
    }

    public function setPaises($paises) {
        $this->paises = $paises->fetchAll();
        return $this;
    }
    
    public function getGrupos() {
        return $this->grupos;
    }

    public function setGrupos($grupos) {
        $this->grupos = $grupos->fetchAll();
        return $this;
    }

    public function getContactos() {
        return $this->contactos;
    }

    public function setContactos($contactos) {
        $this->contactos = $contactos->fetchAll();
        return $this;
    }

    public function obtenerModelo() {
        return $this->Modelomensajes_enviados;
    }

    public function getTodos() {
        return $this->todos;
    }

    public function setTodos($todos) {
        $this->todos = $todos;
        return $this;
    }

    public function getmensajes_enviados() {
        return $this->mensajes_enviados;
    }

    public function setmensajes_enviados($mensajes_enviados) {
        $this->mensajes_enviados = $mensajes_enviados->fetchAll();
        return $this;
    }

    public function crear() {
        $titulo = "Crear mensajes_enviados";
        $Vista = 'mensajes_enviados/Vista_Crear_mensajes_enviados.php';
        $UsuarioId = $_SESSION['UsuarioId'];
        $ContactoId = filter_input(INPUT_POST, "ContactoId");
        $Mensaje_EnviadoFechaCreacion = filter_input(INPUT_POST, "Mensaje_EnviadoFechaCreacion");
        $Mensaje_EnviadoFechaEnvio = filter_input(INPUT_POST, "Mensaje_EnviadoFechaEnvio");
        $Mensaje_EnviadoContenido = filter_input(INPUT_POST, "Mensaje_EnviadoContenido");
        $Mensaje_EnviadoOneCheck = filter_input(INPUT_POST, "Mensaje_EnviadoOneCheck");
        $Mensaje_EnviadoDoubleCheck = filter_input(INPUT_POST, "Mensaje_EnviadoDoubleCheck");
        $Mensaje_EnviadoFotoPerfil = filter_input(INPUT_POST, "Mensaje_EnviadoFotoPerfil");
        $Mensaje_EnviadoTexto = filter_input(INPUT_POST, "Mensaje_EnviadoTexto");
        $Mensaje_EnviadoDenunciado = filter_input(INPUT_POST, "Mensaje_EnviadoDenunciado");
        $Mensaje_EnviadoCreditos = filter_input(INPUT_POST, "Mensaje_EnviadoCreditos");
        $Mensaje_EnviadoEnvidado = filter_input(INPUT_POST, "Mensaje_EnviadoEnvidado");
        if (filter_input(INPUT_POST, "Crearmensajes_enviados") == 'Crearmensajes_enviados') {
            if (isset($UsuarioId) && isset($ContactoId) && isset($Mensaje_EnviadoFechaCreacion) && isset($Mensaje_EnviadoFechaEnvio) && isset($Mensaje_EnviadoContenido) && isset($Mensaje_EnviadoOneCheck) && isset($Mensaje_EnviadoDoubleCheck) && isset($Mensaje_EnviadoFotoPerfil) && isset($Mensaje_EnviadoTexto) && isset($Mensaje_EnviadoDenunciado) && isset($Mensaje_EnviadoCreditos) && isset($Mensaje_EnviadoEnvidado)) {
                $parametros = array('Mensaje_EnviadoId' => $Mensaje_EnviadoId, 'UsuarioId' => $UsuarioId, 'ContactoId' => $ContactoId, 'Mensaje_EnviadoFechaCreacion' => $Mensaje_EnviadoFechaCreacion, 'Mensaje_EnviadoFechaEnvio' => $Mensaje_EnviadoFechaEnvio, 'Mensaje_EnviadoContenido' => $Mensaje_EnviadoContenido, 'Mensaje_EnviadoOneCheck' => $Mensaje_EnviadoOneCheck, 'Mensaje_EnviadoDoubleCheck' => $Mensaje_EnviadoDoubleCheck, 'Mensaje_EnviadoFotoPerfil' => $Mensaje_EnviadoFotoPerfil, 'Mensaje_EnviadoTexto' => $Mensaje_EnviadoTexto, 'Mensaje_EnviadoDenunciado' => $Mensaje_EnviadoDenunciado, 'Mensaje_EnviadoCreditos' => $Mensaje_EnviadoCreditos, 'Mensaje_EnviadoEnvidado' => $Mensaje_EnviadoEnvidado);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->setPaises($this->ModeloPaises->obtenerTodos());
        $this->setGrupos($this->ModeloGrupos->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

    public function actualizar() {
        $Mensaje_EnviadoId = filter_input(INPUT_POST, "Mensaje_EnviadoId");
        if (isset($Mensaje_EnviadoId)) {
            $parametros = array('Mensaje_EnviadoId' => $Mensaje_EnviadoId);
            $this->setmensajes_enviados($this->obtenerModelo()->obtenerPorId($parametros));
            if (filter_input(INPUT_POST, "Actualizarmensajes_enviados") == 'Actualizarmensajes_enviados') {
                $Mensaje_EnviadoId = filter_input(INPUT_POST, "Mensaje_EnviadoId");
                $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
                $ContactoId = filter_input(INPUT_POST, "ContactoId");
                $Mensaje_EnviadoFechaCreacion = filter_input(INPUT_POST, "Mensaje_EnviadoFechaCreacion");
                $Mensaje_EnviadoFechaEnvio = filter_input(INPUT_POST, "Mensaje_EnviadoFechaEnvio");
                $Mensaje_EnviadoContenido = filter_input(INPUT_POST, "Mensaje_EnviadoContenido");
                $Mensaje_EnviadoOneCheck = filter_input(INPUT_POST, "Mensaje_EnviadoOneCheck");
                $Mensaje_EnviadoDoubleCheck = filter_input(INPUT_POST, "Mensaje_EnviadoDoubleCheck");
                $Mensaje_EnviadoFotoPerfil = filter_input(INPUT_POST, "Mensaje_EnviadoFotoPerfil");
                $Mensaje_EnviadoTexto = filter_input(INPUT_POST, "Mensaje_EnviadoTexto");
                $Mensaje_EnviadoDenunciado = filter_input(INPUT_POST, "Mensaje_EnviadoDenunciado");
                $Mensaje_EnviadoCreditos = filter_input(INPUT_POST, "Mensaje_EnviadoCreditos");
                $Mensaje_EnviadoEnvidado = filter_input(INPUT_POST, "Mensaje_EnviadoEnvidado");
                if (isset($Mensaje_EnviadoId) && isset($UsuarioId) && isset($ContactoId) && isset($Mensaje_EnviadoFechaCreacion) && isset($Mensaje_EnviadoFechaEnvio) && isset($Mensaje_EnviadoContenido) && isset($Mensaje_EnviadoOneCheck) && isset($Mensaje_EnviadoDoubleCheck) && isset($Mensaje_EnviadoFotoPerfil) && isset($Mensaje_EnviadoTexto) && isset($Mensaje_EnviadoDenunciado) && isset($Mensaje_EnviadoCreditos) && isset($Mensaje_EnviadoEnvidado)) {
                    foreach ($this->mensajes_enviados as $_mensajes_enviados) {
                        $Mensaje_EnviadoIdBD = $_mensajes_enviados['Mensaje_EnviadoId'];
                        $UsuarioIdBD = $_mensajes_enviados['UsuarioId'];
                        $ContactoIdBD = $_mensajes_enviados['ContactoId'];
                        $Mensaje_EnviadoFechaCreacionBD = $_mensajes_enviados['Mensaje_EnviadoFechaCreacion'];
                        $Mensaje_EnviadoFechaEnvioBD = $_mensajes_enviados['Mensaje_EnviadoFechaEnvio'];
                        $Mensaje_EnviadoContenidoBD = $_mensajes_enviados['Mensaje_EnviadoContenido'];
                        $Mensaje_EnviadoOneCheckBD = $_mensajes_enviados['Mensaje_EnviadoOneCheck'];
                        $Mensaje_EnviadoDoubleCheckBD = $_mensajes_enviados['Mensaje_EnviadoDoubleCheck'];
                        $Mensaje_EnviadoFotoPerfilBD = $_mensajes_enviados['Mensaje_EnviadoFotoPerfil'];
                        $Mensaje_EnviadoTextoBD = $_mensajes_enviados['Mensaje_EnviadoTexto'];
                        $Mensaje_EnviadoDenunciadoBD = $_mensajes_enviados['Mensaje_EnviadoDenunciado'];
                        $Mensaje_EnviadoCreditosBD = $_mensajes_enviados['Mensaje_EnviadoCreditos'];
                        $Mensaje_EnviadoEnvidadoBD = $_mensajes_enviados['Mensaje_EnviadoEnvidado'];
                    }
                    if (strcmp($Mensaje_EnviadoId, $Mensaje_EnviadoIdBD) == 0 &&
                            strcmp($UsuarioId, $UsuarioIdBD) == 0 &&
                            strcmp($ContactoId, $ContactoIdBD) == 0 &&
                            strcmp($Mensaje_EnviadoFechaCreacion, $Mensaje_EnviadoFechaCreacionBD) == 0 &&
                            strcmp($Mensaje_EnviadoFechaEnvio, $Mensaje_EnviadoFechaEnvioBD) == 0 &&
                            strcmp($Mensaje_EnviadoContenido, $Mensaje_EnviadoContenidoBD) == 0 &&
                            strcmp($Mensaje_EnviadoOneCheck, $Mensaje_EnviadoOneCheckBD) == 0 &&
                            strcmp($Mensaje_EnviadoDoubleCheck, $Mensaje_EnviadoDoubleCheckBD) == 0 &&
                            strcmp($Mensaje_EnviadoFotoPerfil, $Mensaje_EnviadoFotoPerfilBD) == 0 &&
                            strcmp($Mensaje_EnviadoTexto, $Mensaje_EnviadoTextoBD) == 0 &&
                            strcmp($Mensaje_EnviadoDenunciado, $Mensaje_EnviadoDenunciadoBD) == 0 &&
                            strcmp($Mensaje_EnviadoCreditos, $Mensaje_EnviadoCreditosBD) == 0 &&
                            strcmp($Mensaje_EnviadoEnvidado, $Mensaje_EnviadoEnvidadoBD) == 0) {
                        header('Location: /mensajes_enviados/ver-todos.php');
                    } else {
                        $parametrosActualizar = array('Mensaje_EnviadoId' => $Mensaje_EnviadoId, 'UsuarioId' => $UsuarioId, 'ContactoId' => $ContactoId, 'Mensaje_EnviadoFechaCreacion' => $Mensaje_EnviadoFechaCreacion, 'Mensaje_EnviadoFechaEnvio' => $Mensaje_EnviadoFechaEnvio, 'Mensaje_EnviadoContenido' => $Mensaje_EnviadoContenido, 'Mensaje_EnviadoOneCheck' => $Mensaje_EnviadoOneCheck, 'Mensaje_EnviadoDoubleCheck' => $Mensaje_EnviadoDoubleCheck, 'Mensaje_EnviadoFotoPerfil' => $Mensaje_EnviadoFotoPerfil, 'Mensaje_EnviadoTexto' => $Mensaje_EnviadoTexto, 'Mensaje_EnviadoDenunciado' => $Mensaje_EnviadoDenunciado, 'Mensaje_EnviadoCreditos' => $Mensaje_EnviadoCreditos, 'Mensaje_EnviadoEnvidado' => $Mensaje_EnviadoEnvidado);
                        $this->obtenerModelo()->actualizar($parametrosActualizar);
                    }
                }
            }
        }
        $titulo = "Editar mensajes_enviados";
        $Vista = "mensajes_enviados/Vista_Actualizar_mensajes_enviados.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function obtenerPorId() {
        $Mensaje_EnviadoId = filter_input(INPUT_POST, "Mensaje_EnviadoId");
        if (isset($Mensaje_EnviadoId)) {
            $parametros = array('Mensaje_EnviadoId' => $Mensaje_EnviadoId);
            $this->mensajes_enviados = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver mensajes_enviados";
        $Vista = "mensajes_enviados/Vista_Ver_mensajes_enviados.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function borrar() {
        $Mensaje_EnviadoId = filter_input(INPUT_POST, "Mensaje_EnviadoId");
        if (isset($Mensaje_EnviadoId)) {
            $parametros = array('Mensaje_EnviadoId' => $Mensaje_EnviadoId);
            $Borrarmensajes_enviados = $this->obtenerModelo()->borrar($parametros);
            if ($Borrarmensajes_enviados === false) {
                header('Location: mensajes_enviados/ver-todos.php');
            }
        }
    }

    public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'mensajes_enviados/Vista_Ver_Todos.php';
        $mensajes_enviados = preg_split('/\n/', $this->Modelo_API->consultar_mensajes_enviados());
        $this->setTodos($mensajes_enviados);
        $this->cargarPagina($titulo, $Vista);
    }

}
