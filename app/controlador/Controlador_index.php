<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controlador;

require_once("Controlador.php");

class Controlador_index extends \controlador\Controlador {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $titulo = "Pagina de inicio";
        $Vista = 'index/Vista_Index.php';
        $this->cargarPagina($titulo, $Vista);
    }

    public function actualizar() {
        
    }

    public function borrar() {
        
    }

    public function crear() {
        
    }

    public function obtenerPorId() {
        
    }

    public function obtenerTodos() {
        
    }

//put your code here
}
