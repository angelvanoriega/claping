<?php

namespace controlador;

require_once("Controlador.php");

class Controlador_mensajes_recibidos extends \controlador\Controlador {

    private $todos;
    private $mensajes_recibidos;

    public function __construct() {
        parent::__construct();
        $this->Modelomensajes_recibidos = new \modelo\Modelo_mensajes_recibidos();
        $this->Modelo_API = new \modelo\Modelo_API();
    }

    public function obtenerModelo() {
        return $this->Modelomensajes_recibidos;
    }

    public function getTodos() {
        return $this->todos;
    }

    public function setTodos($todos) {
        $this->todos = $todos;
        return $this;
    }

    public function getmensajes_recibidos() {
        return $this->mensajes_recibidos;
    }

    public function setmensajes_recibidos($mensajes_recibidos) {
        $this->mensajes_recibidos = $mensajes_recibidos;
        return $this;
    }

    public function crear() {
        $titulo = "Crear mensajes_recibidos";
        $Vista = 'mensajes_recibidos/Vista_Crear_mensajes_recibidos.php';
        $Mensaje_RecibidoId = filter_input(INPUT_POST, "Mensaje_RecibidoId");
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        $Mensaje_RecibidoRemitente = filter_input(INPUT_POST, "Mensaje_RecibidoRemitente");
        $Mensaje_RecibidoTexto = filter_input(INPUT_POST, "Mensaje_RecibidoTexto");
        $Mensaje_RecibidoFecha = filter_input(INPUT_POST, "Mensaje_RecibidoFecha");
        if (filter_input(INPUT_POST, "Crearmensajes_recibidos") == 'Crearmensajes_recibidos') {
            if (isset($Mensaje_RecibidoId) && isset($UsuarioId) && isset($Mensaje_RecibidoRemitente) && isset($Mensaje_RecibidoTexto) && isset($Mensaje_RecibidoFecha)) {
                $parametros = array('Mensaje_RecibidoId' => $Mensaje_RecibidoId, 'UsuarioId' => $UsuarioId, 'Mensaje_RecibidoRemitente' => $Mensaje_RecibidoRemitente, 'Mensaje_RecibidoTexto' => $Mensaje_RecibidoTexto, 'Mensaje_RecibidoFecha' => $Mensaje_RecibidoFecha);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

    public function actualizar() {
        $Mensaje_RecibidoId = filter_input(INPUT_POST, "Mensaje_RecibidoId");
        if (isset($Mensaje_RecibidoId)) {
            $parametros = array('Mensaje_RecibidoId' => $Mensaje_RecibidoId);
            $this->setmensajes_recibidos($this->obtenerModelo()->obtenerPorId($parametros));
            if (filter_input(INPUT_POST, "Actualizarmensajes_recibidos") == 'Actualizarmensajes_recibidos') {
                $Mensaje_RecibidoId = filter_input(INPUT_POST, "Mensaje_RecibidoId");
                $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
                $Mensaje_RecibidoRemitente = filter_input(INPUT_POST, "Mensaje_RecibidoRemitente");
                $Mensaje_RecibidoTexto = filter_input(INPUT_POST, "Mensaje_RecibidoTexto");
                $Mensaje_RecibidoFecha = filter_input(INPUT_POST, "Mensaje_RecibidoFecha");
                if (isset($Mensaje_RecibidoId) && isset($UsuarioId) && isset($Mensaje_RecibidoRemitente) && isset($Mensaje_RecibidoTexto) && isset($Mensaje_RecibidoFecha)) {
                    foreach ($this->mensajes_recibidos as $_mensajes_recibidos) {
                        $Mensaje_RecibidoIdBD = $_mensajes_recibidos['Mensaje_RecibidoId'];
                        $UsuarioIdBD = $_mensajes_recibidos['UsuarioId'];
                        $Mensaje_RecibidoRemitenteBD = $_mensajes_recibidos['Mensaje_RecibidoRemitente'];
                        $Mensaje_RecibidoTextoBD = $_mensajes_recibidos['Mensaje_RecibidoTexto'];
                        $Mensaje_RecibidoFechaBD = $_mensajes_recibidos['Mensaje_RecibidoFecha'];
                    }
                    if (strcmp($Mensaje_RecibidoId, $Mensaje_RecibidoIdBD) == 0 &&
                            strcmp($UsuarioId, $UsuarioIdBD) == 0 &&
                            strcmp($Mensaje_RecibidoRemitente, $Mensaje_RecibidoRemitenteBD) == 0 &&
                            strcmp($Mensaje_RecibidoTexto, $Mensaje_RecibidoTextoBD) == 0 &&
                            strcmp($Mensaje_RecibidoFecha, $Mensaje_RecibidoFechaBD) == 0) {
                        header('Location: /mensajes_recibidos/ver-todos.php');
                    } else {
                        $parametrosActualizar = array('Mensaje_RecibidoId' => $Mensaje_RecibidoId, 'UsuarioId' => $UsuarioId, 'Mensaje_RecibidoRemitente' => $Mensaje_RecibidoRemitente, 'Mensaje_RecibidoTexto' => $Mensaje_RecibidoTexto, 'Mensaje_RecibidoFecha' => $Mensaje_RecibidoFecha);
                        $this->obtenerModelo()->actualizar($parametrosActualizar);
                    }
                }
            }
        }
        $titulo = "Editar mensajes_recibidos";
        $Vista = "mensajes_recibidos/Vista_Actualizar_mensajes_recibidos.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function obtenerPorId() {
        $Mensaje_RecibidoId = filter_input(INPUT_POST, "Mensaje_RecibidoId");
        $mensajes_recibidos = preg_split('/\n/', $this->Modelo_API->consultar_mensajes_recibidos_id($Mensaje_RecibidoId));
        $this->setmensajes_recibidos($mensajes_recibidos);
        $titulo = "Ver mensajes_recibidos";
        $Vista = "mensajes_recibidos/Vista_Ver_mensajes_recibidos.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function borrar() {
        $Mensaje_RecibidoId = filter_input(INPUT_POST, "Mensaje_RecibidoId");
        if (isset($Mensaje_RecibidoId)) {
            $parametros = array('Mensaje_RecibidoId' => $Mensaje_RecibidoId);
            $Borrarmensajes_recibidos = $this->obtenerModelo()->borrar($parametros);
            if ($Borrarmensajes_recibidos === false) {
                header('Location: mensajes_recibidos/ver-todos.php');
            }
        }
    }

    public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'mensajes_recibidos/Vista_Ver_Todos.php';
        $mensajes_recibidos = preg_split('/\n/', $this->Modelo_API->consultar_mensajes_recibidos());
        $this->setTodos($mensajes_recibidos);
        $this->cargarPagina($titulo, $Vista);
    }
}
