<?php
namespace controlador;

require_once("Controlador.php");
class Controlador_creditos extends \controlador\Controlador {

private $todos;
private $creditos;

public function __construct() {
        parent::__construct();
        $this->Modelocreditos = new \modelo\Modelo_creditos();
}

public function obtenerModelo() {
        return $this->Modelocreditos;
    }

public function getTodos() {
    return $this->todos;
}

public function setTodos($todos) {
    $this->todos = $todos->fetchAll();
    return $this;
}
public function getcreditos() {
    return $this->creditos;
}

public function setcreditos($creditos) {
    $this->creditos = $creditos->fetchAll();
    return $this;
}

public function crear() {
        $titulo = "Crear creditos";
        $Vista = 'creditos/Vista_Crear_creditos.php';
        $CreditoId = filter_input(INPUT_POST, "CreditoId");
        $CreditoCantidad = filter_input(INPUT_POST, "CreditoCantidad");
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
        if (filter_input(INPUT_POST, "Crearcreditos") == 'Crearcreditos') {
            if (isset($CreditoId) && isset($CreditoCantidad) && isset($UsuarioId)) {
                $parametros = array('CreditoId' => $CreditoId,'CreditoCantidad' => $CreditoCantidad,'UsuarioId' => $UsuarioId);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

public function actualizar() {
        $CreditoId = filter_input(INPUT_POST, "CreditoId");
        if (isset($CreditoId)) {
            $parametros = array('CreditoId' => $CreditoId);
            $this->setcreditos($this->obtenerModelo()->obtenerPorId($parametros));
            if (filter_input(INPUT_POST, "Actualizarcreditos") == 'Actualizarcreditos') {
        $CreditoId = filter_input(INPUT_POST, "CreditoId");
        $CreditoCantidad = filter_input(INPUT_POST, "CreditoCantidad");
        $UsuarioId = filter_input(INPUT_POST, "UsuarioId");
                if (isset($CreditoId) && isset($CreditoCantidad) && isset($UsuarioId)) {
                    foreach ($this->creditos as $_creditos) {
           $CreditoIdBD = $_creditos['CreditoId'];
           $CreditoCantidadBD = $_creditos['CreditoCantidad'];
           $UsuarioIdBD = $_creditos['UsuarioId'];
                    }
                        if (strcmp($CreditoId,$CreditoIdBD)== 0 && 
strcmp($CreditoCantidad,$CreditoCantidadBD)== 0 && 
strcmp($UsuarioId,$UsuarioIdBD)== 0) {
                            header('Location: /creditos/ver-todos.php');
                        } else {
                            $parametrosActualizar = array('CreditoId' => $CreditoId,'CreditoCantidad' => $CreditoCantidad,'UsuarioId' => $UsuarioId);
                            $this->obtenerModelo()->actualizar($parametrosActualizar);
                        }
                    }
                }
            }
        $titulo = "Editar creditos";
        $Vista = "creditos/Vista_Actualizar_creditos.php";
        $this->cargarPagina($titulo, $Vista);
    }
public function obtenerPorId() {
        $CreditoId = filter_input(INPUT_POST, "CreditoId");
        if (isset($CreditoId)) {
            $parametros = array('CreditoId' => $CreditoId);
            $this->creditos = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver creditos";
        $Vista = "creditos/Vista_Ver_creditos.php";
        $this->cargarPagina($titulo, $Vista);
    }
public function borrar() {
        $CreditoId = filter_input(INPUT_POST, "CreditoId");
        if (isset($CreditoId)) {
            $parametros = array('CreditoId' => $CreditoId);
        $Borrarcreditos = $this->obtenerModelo()->borrar($parametros);
        if ($Borrarcreditos === false) {
            header('Location: creditos/ver-todos.php');
        }
        }
    }
public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'creditos/Vista_Ver_Todos.php';
        $this->setTodos($this->obtenerModelo()->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

}