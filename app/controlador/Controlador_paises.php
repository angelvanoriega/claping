<?php

namespace controlador;

require_once("Controlador.php");

class Controlador_paises extends \controlador\Controlador {

    private $todos;
    private $paises;

    public function __construct() {
        parent::__construct();
        $this->Modelopaises = new \modelo\Modelo_paises();
    }

    public function obtenerModelo() {
        return $this->Modelopaises;
    }

    public function getTodos() {
        return $this->todos;
    }

    public function setTodos($todos) {
        $this->todos = $todos->fetchAll();
        return $this;
    }

    public function getpaises() {
        return $this->paises;
    }

    public function setpaises($paises) {
        $this->paises = $paises->fetchAll();
        return $this;
    }

    public function crear() {
        $titulo = "Crear paises";
        $Vista = 'paises/Vista_Crear_paises.php';
        $PaisId = filter_input(INPUT_POST, "PaisId");
        $PaisNombre = filter_input(INPUT_POST, "PaisNombre");
        $PaisClaveLada = filter_input(INPUT_POST, "PaisClaveLada");
        if (filter_input(INPUT_POST, "Crearpaises") == 'Crearpaises') {
            if (isset($PaisId) && isset($PaisNombre) && isset($PaisClaveLada)) {
                $parametros = array('PaisId' => $PaisId, 'PaisNombre' => $PaisNombre, 'PaisClaveLada' => $PaisClaveLada);
                $this->obtenerModelo()->crear($parametros);
            }
        }
        $this->cargarPagina($titulo, $Vista);
    }

    public function nombrePais() {
        $PaisId = filter_input(INPUT_POST, "PaisId");
        if (isset($PaisId)) {
            $parametros = array('PaisId' => $PaisId);
            $this->paises = $this->obtenerModelo()->obtenerPorId($parametros);
            if (isset($this->paises)) {
                $Paises = $this->paises->fetch(\PDO::FETCH_ASSOC);
                return utf8_encode($Paises["PaisNombre"]);
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    public function clavePais() {
        $PaisId = filter_input(INPUT_POST, "PaisId");
        if (isset($PaisId)) {
            $parametros = array('PaisId' => $PaisId);
            $this->paises = $this->obtenerModelo()->obtenerPorId($parametros);
            if (isset($this->paises)) {
                $Paises = $this->paises->fetch(\PDO::FETCH_ASSOC);
                return utf8_encode($Paises["PaisClaveLada"]);
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function actualizar() {
        $PaisId = filter_input(INPUT_POST, "PaisId");
        if (isset($PaisId)) {
            $parametros = array('PaisId' => $PaisId);
            $this->setpaises($this->obtenerModelo()->obtenerPorId($parametros));
            if (filter_input(INPUT_POST, "Actualizarpaises") == 'Actualizarpaises') {
                exit();
                $PaisId = filter_input(INPUT_POST, "PaisId");
                $PaisNombre = filter_input(INPUT_POST, "PaisNombre");
                $PaisClaveLada = filter_input(INPUT_POST, "PaisClaveLada");
                if (isset($PaisId) && isset($PaisNombre) && isset($PaisClaveLada)) {
                    foreach ($this->paises as $_paises) {
                        $PaisIdBD = $_paises['PaisId'];
                        $PaisNombreBD = $_paises['PaisNombre'];
                        $PaisClaveLadaBD = $_paises['PaisClaveLada'];
                    }
                    if (strcmp($PaisId, $PaisIdBD) == 0 &&
                            strcmp($PaisNombre, $PaisNombreBD) == 0 &&
                            strcmp($PaisClaveLada, $PaisClaveLadaBD) == 0) {
                        header('Location: /paises/ver-todos.php');
                    } else {
                        $parametrosActualizar = array('PaisId' => $PaisId, 'PaisNombre' => $PaisNombre, 'PaisClaveLada' => $PaisClaveLada);
                        $this->obtenerModelo()->actualizar($parametrosActualizar);
                    }
                }
            }
        }
        $titulo = "Editar paises";
        $Vista = "paises/Vista_Actualizar_paises.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function obtenerPorId() {
        $PaisId = filter_input(INPUT_POST, "PaisId");
        if (isset($PaisId)) {
            $parametros = array('PaisId' => $PaisId);
            $this->paises = $this->obtenerModelo()->obtenerPorId($parametros);
        }
        $titulo = "Ver paises";
        $Vista = "paises/Vista_Ver_paises.php";
        $this->cargarPagina($titulo, $Vista);
    }

    public function borrar() {
        $PaisId = filter_input(INPUT_POST, "PaisId");
        if (isset($PaisId)) {
            $parametros = array('PaisId' => $PaisId);
            $Borrarpaises = $this->obtenerModelo()->borrar($parametros);
            if ($Borrarpaises === false) {
                header('Location: paises/ver-todos.php');
            }
        }
    }

    public function obtenerTodos() {
        $titulo = "Ver todos";
        $Vista = 'paises/Vista_Ver_Todos.php';
        $this->setTodos($this->obtenerModelo()->obtenerTodos());
        $this->cargarPagina($titulo, $Vista);
    }

}
